# SOME DESCRIPTIVE TITLE.
# Copyright (C) Django Software Foundation and contributors
# This file is distributed under the same license as the Django package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: Django 1.7\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2014-10-13 18:33+0300\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#: ../../internals/git.txt:3
# a37035881e6a426e8cecb74a85fed40a
msgid "The Django source code repository"
msgstr ""

#: ../../internals/git.txt:5
# 8041448bf78345d0b5327bf1a038aa28
msgid "When deploying a Django application into a real production environment, you will almost always want to use `an official packaged release of Django`_."
msgstr ""

#: ../../internals/git.txt:8
# 477b57205b544683bc11b27d111f98c4
msgid "However, if you'd like to try out in-development code from an upcoming release or contribute to the development of Django, you'll need to obtain a clone of Django's source code repository."
msgstr ""

#: ../../internals/git.txt:12
# 22cb644fd35849238a0d79561ea3a8cf
msgid "This document covers the way the code repository is laid out and how to work with and find things in it."
msgstr ""

#: ../../internals/git.txt:18
# 5de07987c1be4efcb53cb58e44ba61f4
msgid "High-level overview"
msgstr ""

#: ../../internals/git.txt:20
# 57f3f1760baf4416ab123b50d27154cf
msgid "The Django source code repository uses `Git`_ to track changes to the code over time, so you'll need a copy of the Git client (a program called ``git``) on your computer, and you'll want to familiarize yourself with the basics of how Git works."
msgstr ""

#: ../../internals/git.txt:25
# 6bb213677d0c419c8d055b14ed8c0097
msgid "Git's web site offers downloads for various operating systems. The site also contains vast amounts of `documentation`_."
msgstr ""

#: ../../internals/git.txt:28
# 97a131985c9b4e058cea395c2cd43c14
msgid "The Django Git repository is located online at `github.com/django/django <https://github.com/django/django>`_. It contains the full source code for all Django releases, which you can browse online."
msgstr ""

#: ../../internals/git.txt:32
# d7c2850013474c918824a7033d3332e9
msgid "The Git repository includes several `branches`_:"
msgstr ""

#: ../../internals/git.txt:34
# 46b3f68f3547466b98bd62cb62365d52
msgid "``master`` contains the main in-development code which will become the next packaged release of Django. This is where most development activity is focused."
msgstr ""

#: ../../internals/git.txt:38
# 2494292255b341939fc986e622f4cd6c
msgid "``stable/A.B.x`` are the branches where release preparation work happens. They are also used for support and bugfix releases which occur as necessary after the initial release of a major or minor version."
msgstr ""

#: ../../internals/git.txt:42
# d774fbd3a82d4593bb16338c9748d756
msgid "``soc20XX/<project>`` branches were used by students who worked on Django during the 2009 and 2010 Google Summer of Code programs."
msgstr ""

#: ../../internals/git.txt:45
# d9e2286c924e42578f468ff609fefead
msgid "``attic/<project>`` branches were used to develop major or experimental new features without affecting the rest of Django's code."
msgstr ""

#: ../../internals/git.txt:48
# 1caa1c5027c543148f4ae177ca4bc6fb
msgid "The Git repository also contains `tags`_. These are the exact revisions from which packaged Django releases were produced, since version 1.0."
msgstr ""

#: ../../internals/git.txt:51
# 34619333ec974839b479b59148707837
msgid "The source code for the `Djangoproject.com <https://www.djangoproject.com/>`_ web site can be found at `github.com/django/djangoproject.com <https://github.com/django/djangoproject.com>`_."
msgstr ""

#: ../../internals/git.txt:61
# aaf19b0911f64282b518a9ab0da3e0b7
msgid "The master branch"
msgstr ""

#: ../../internals/git.txt:63
# e9c276e9167e46d0aa17ba04e3c96644
msgid "If you'd like to try out the in-development code for the next release of Django, or if you'd like to contribute to Django by fixing bugs or developing new features, you'll want to get the code from the master branch."
msgstr ""

#: ../../internals/git.txt:67
# ec1116ef838241728294c57a582e2f51
msgid "Note that this will get *all* of Django: in addition to the top-level ``django`` module containing Python code, you'll also get a copy of Django's documentation, test suite, packaging scripts and other miscellaneous bits. Django's code will be present in your clone as a directory named ``django``."
msgstr ""

#: ../../internals/git.txt:73
# 72780e736cf04847977214c8a3cb5c56
msgid "To try out the in-development code with your own applications, simply place the directory containing your clone on your Python import path. Then ``import`` statements which look for Django will find the ``django`` module within your clone."
msgstr ""

#: ../../internals/git.txt:78
# 41a551a4bf454eb7b1276ff766b6bba0
msgid "If you're going to be working on Django's code (say, to fix a bug or develop a new feature), you can probably stop reading here and move over to :doc:`the documentation for contributing to Django </internals/contributing/index>`, which covers things like the preferred coding style and how to generate and submit a patch."
msgstr ""

#: ../../internals/git.txt:85
# 02a826dd986e402795dee51e9ac9fee0
msgid "Other branches"
msgstr ""

#: ../../internals/git.txt:87
# 2e65f83fbaf54a9986eed451632999cb
msgid "Django uses branches to prepare for releases of Django (whether they be :term:`major <Major release>` or :term:`minor <Minor release>`)."
msgstr ""

#: ../../internals/git.txt:90
# 354ba99a5ca6478297753be2ed2f3e19
msgid "In the past when Django was hosted on Subversion, branches were also used for feature development. Now Django is hosted on Git and feature development is done on contributor's forks, but the Subversion feature branches remain in Git for historical reference."
msgstr ""

#: ../../internals/git.txt:96
# aaa4e57ea92349659db9520ade5e7928
msgid "Stable branches"
msgstr ""

#: ../../internals/git.txt:98
# b545c838012c4a41a993486c4d260a2b
msgid "These branches can be found in the repository as ``stable/A.B.x`` branches and will be created right after the first alpha is tagged."
msgstr ""

#: ../../internals/git.txt:101
# 13e001ef509649a8854962fdc9c7e22b
msgid "For example, immediately after *Django 1.5 alpha 1* was tagged, the branch ``stable/1.5.x`` was created and all further work on preparing the code for the final 1.5 release was done there."
msgstr ""

#: ../../internals/git.txt:105
# 5b2c61e410f5453f9e9d5560ef14bb93
msgid "These branches also provide limited bugfix support for the most recent released version of Django and security support for the two most recently-released versions of Django."
msgstr ""

#: ../../internals/git.txt:109
# 0b02df69d24b4de7a6063c149db1a8a6
msgid "For example, after the release of Django 1.5, the branch ``stable/1.5.x`` receives only fixes for security and critical stability bugs, which are eventually released as Django 1.5.1 and so on, ``stable/1.4.x`` receives only security fixes, and ``stable/1.3.x`` no longer receives any updates."
msgstr ""

#: ../../internals/git.txt:114
#: ../../internals/git.txt:134
# 99a1b2ac540d473eb007713bf9018a6d
# 3598bacaf4ea4a7eafc0c5b0ee38b038
msgid "Historical information"
msgstr ""

#: ../../internals/git.txt:116
# ca95855413044eeca378ef8051e6f5b9
msgid "This policy for handling ``stable/A.B.x`` branches was adopted starting with the Django 1.5 release cycle."
msgstr ""

#: ../../internals/git.txt:119
# 952925e4045947d9b93b2e91aadf8d2d
msgid "Previously, these branches weren't created until right after the releases and the stabilization work occurred on the main repository branch. Thus, no new features development work for the next release of Django could be committed until the final release happened."
msgstr ""

#: ../../internals/git.txt:124
# 68939409610847dfb691eb91e821a7ee
msgid "For example, shortly after the release of Django 1.3 the branch ``stable/1.3.x`` was created. Official support for that release has expired, and so it no longer receives direct maintenance from the Django project. However, that and all other similarly named branches continue to exist and interested community members have occasionally used them to provide unofficial support for old Django releases."
msgstr ""

#: ../../internals/git.txt:132
# 5c9390812adc45608f5d97253abae7a2
msgid "Feature-development branches"
msgstr ""

#: ../../internals/git.txt:136
# 3997ffcea4f044b59eba0ac58be947cc
msgid "Since Django moved to Git in 2012, anyone can clone the repository and create their own branches, alleviating the need for official branches in the source code repository."
msgstr ""

#: ../../internals/git.txt:140
# a2ad6c73a7f247819558f21727f95b4f
msgid "The following section is mostly useful if you're exploring the repository's history, for example if you're trying to understand how some features were designed."
msgstr ""

#: ../../internals/git.txt:144
# a5531e276ae54928b6ec144fbc654580
msgid "Feature-development branches tend by their nature to be temporary. Some produce successful features which are merged back into Django's master to become part of an official release, but others do not; in either case there comes a time when the branch is no longer being actively worked on by any developer. At this point the branch is considered closed."
msgstr ""

#: ../../internals/git.txt:150
# d21fa777a911496dab1547d915d7abfb
msgid "Unfortunately, Django used to be maintained with the Subversion revision control system, that has no standard way of indicating this. As a workaround, branches of Django which are closed and no longer maintained were moved into ``attic``."
msgstr ""

#: ../../internals/git.txt:155
# 81bb9c84101841749117983787b91f29
msgid "For reference, the following are branches whose code eventually became part of Django itself, and so are no longer separately maintained:"
msgstr ""

#: ../../internals/git.txt:158
# 86dcac659ecb406f89e20ff38d663c05
msgid "``boulder-oracle-sprint``: Added support for Oracle databases to Django's object-relational mapper. This has been part of Django since the 1.0 release."
msgstr ""

#: ../../internals/git.txt:162
# 75021af7d3484961846d1094c1a4fe8d
msgid "``gis``: Added support for geographic/spatial queries to Django's object-relational mapper. This has been part of Django since the 1.0 release, as the bundled application ``django.contrib.gis``."
msgstr ""

#: ../../internals/git.txt:166
# 1f70f7d9c01e48438fc691c125e286ac
msgid "``i18n``: Added :doc:`internationalization support </topics/i18n/index>` to Django. This has been part of Django since the 0.90 release."
msgstr ""

#: ../../internals/git.txt:169
# 467c242ee03d4fe39e0d042d233fe651
msgid "``magic-removal``: A major refactoring of both the internals and public APIs of Django's object-relational mapper. This has been part of Django since the 0.95 release."
msgstr ""

#: ../../internals/git.txt:173
# 961a98ac5f2f459da7048c7350eb6017
msgid "``multi-auth``: A refactoring of :doc:`Django's bundled authentication framework </topics/auth/index>` which added support for :ref:`authentication backends <authentication-backends>`. This has been part of Django since the 0.95 release."
msgstr ""

#: ../../internals/git.txt:178
# 7d29d33bd3444ac196c74829b21861fb
msgid "``new-admin``: A refactoring of :doc:`Django's bundled administrative application </ref/contrib/admin/index>`. This became part of Django as of the 0.91 release, but was superseded by another refactoring (see next listing) prior to the Django 1.0 release."
msgstr ""

#: ../../internals/git.txt:183
# 5daab98579b04e9798a150904f3de775
msgid "``newforms-admin``: The second refactoring of Django's bundled administrative application. This became part of Django as of the 1.0 release, and is the basis of the current incarnation of ``django.contrib.admin``."
msgstr ""

#: ../../internals/git.txt:188
# 51555b94f30a471780b085766036c902
msgid "``queryset-refactor``: A refactoring of the internals of Django's object-relational mapper. This became part of Django as of the 1.0 release."
msgstr ""

#: ../../internals/git.txt:192
# e5f4738db25a4a348884d426fe5f4686
msgid "``unicode``: A refactoring of Django's internals to consistently use Unicode-based strings in most places within Django and Django applications. This became part of Django as of the 1.0 release."
msgstr ""

#: ../../internals/git.txt:196
# 0c7eb6d2206241e4a507bcb5e6c4e1fb
msgid "When Django moved from SVN to Git, the information about branch merges wasn't preserved in the source code repository. This means that the ``master`` branch of Django doesn't contain merge commits for the above branches."
msgstr ""

#: ../../internals/git.txt:200
# b24b21e825b244b89808344bbf3556bd
msgid "However, this information is `available as a grafts file`_. You can restore it by putting the following lines in ``.git/info/grafts`` in your local clone::"
msgstr ""

#: ../../internals/git.txt:218
# ef1df5cf038542c9aff1abc9a1536cc0
msgid "Additionally, the following branches are closed, but their code was never merged into Django and the features they aimed to implement were never finished:"
msgstr ""

#: ../../internals/git.txt:222
# 3c3e9452ec734b5e8dda031622e5715d
msgid "``full-history``"
msgstr ""

#: ../../internals/git.txt:224
# 2467666729794ded9a9008ede0605bea
msgid "``generic-auth``"
msgstr ""

#: ../../internals/git.txt:226
# 9d6aa7e5be154d50bc631744b5b6820a
msgid "``multiple-db-support``"
msgstr ""

#: ../../internals/git.txt:228
# 13d3364bdfa64233aeab7a6902562e8e
msgid "``per-object-permissions``"
msgstr ""

#: ../../internals/git.txt:230
# 03aca3643995403cb10581cf2a248033
msgid "``schema-evolution``"
msgstr ""

#: ../../internals/git.txt:232
# bb8a32410ac84fbeba96a76937a45a6f
msgid "``schema-evolution-ng``"
msgstr ""

#: ../../internals/git.txt:234
# 2556601f6836423b89746166e1b0f07c
msgid "``search-api``"
msgstr ""

#: ../../internals/git.txt:236
# d6d66e28532c4eef865d3c2080bb5b3d
msgid "``sqlalchemy``"
msgstr ""

#: ../../internals/git.txt:238
# 79e633c0544b4f739b7630c29f7ad4dd
msgid "All of the above-mentioned branches now reside in ``attic``."
msgstr ""

#: ../../internals/git.txt:240
# bee74324a78546ef8f67ca46bed5ffd3
msgid "Finally, the repository contains ``soc2009/xxx`` and ``soc2010/xxx`` feature branches, used for Google Summer of Code projects."
msgstr ""

#: ../../internals/git.txt:244
# 4bd4d69e333642e1b8ea779b3d8e1cc0
msgid "Tags"
msgstr ""

#: ../../internals/git.txt:246
# 33421498128b451c8b2329970400d9af
msgid "Each Django release is tagged and signed by Django's release manager."
msgstr ""

#: ../../internals/git.txt:248
# 6e691bfaf582429891e5022467c5e95a
msgid "The tags can be found on GitHub's `tags`_ page."
msgstr ""

