# SOME DESCRIPTIVE TITLE.
# Copyright (C) Django Software Foundation and contributors
# This file is distributed under the same license as the Django package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: Django 1.7\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2014-10-13 18:33+0300\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#: ../../misc/index.txt:2
# 283bf10883d34849810bf913e5de5bf6
msgid "Meta-documentation and miscellany"
msgstr ""

#: ../../misc/index.txt:4
# 5b1b94d8698d43c3a0cf571763ee640e
msgid "Documentation that we can't find a more organized place for. Like that drawer in your kitchen with the scissors, batteries, duct tape, and other junk."
msgstr ""

