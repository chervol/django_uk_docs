# SOME DESCRIPTIVE TITLE.
# Copyright (C) Django Software Foundation and contributors
# This file is distributed under the same license as the Django package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: Django 1.7\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2014-10-13 18:33+0300\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#: ../../ref/class-based-views/mixins-single-object.txt:3
# 065b654835564831843b788ae2df6161
msgid "Single object mixins"
msgstr ""

#: ../../ref/class-based-views/mixins-single-object.txt:6
# 0a01b54fc84542ffb8d12f8bd6e4be1a
msgid "SingleObjectMixin"
msgstr ""

#: ../../ref/class-based-views/mixins-single-object.txt:10
# e43edd7cfbc14db388ed56b7d60bb06e
msgid "Provides a mechanism for looking up an object associated with the current HTTP request."
msgstr ""

#: ../../ref/class-based-views/mixins-single-object.txt:13
#: ../../ref/class-based-views/mixins-single-object.txt:115
# a656b9a116154041b9730418785466a2
# 9353cef245c34d39b3be90a1a8d44e67
msgid "**Methods and Attributes**"
msgstr ""

#: ../../ref/class-based-views/mixins-single-object.txt:17
# 202f6dd95ae8455b8648a1bd8829ea5f
msgid "The model that this view will display data for. Specifying ``model = Foo`` is effectively the same as specifying ``queryset = Foo.objects.all()``."
msgstr ""

#: ../../ref/class-based-views/mixins-single-object.txt:23
# de162f5241e544dab5ee892f135a735c
msgid "A ``QuerySet`` that represents the objects. If provided, the value of ``queryset`` supersedes the value provided for :attr:`model`."
msgstr ""

#: ../../ref/class-based-views/mixins-single-object.txt:28
# 353fb42668f14daabcdf09711c98b330
msgid "``queryset`` is a class attribute with a *mutable* value so care must be taken when using it directly. Before using it, either call its :meth:`~django.db.models.query.QuerySet.all` method or retrieve it with :meth:`get_queryset` which takes care of the cloning behind the scenes."
msgstr ""

#: ../../ref/class-based-views/mixins-single-object.txt:36
# 1495dbb81dbf47f094f6efa307b6d049
msgid "The name of the field on the model that contains the slug. By default, ``slug_field`` is ``'slug'``."
msgstr ""

#: ../../ref/class-based-views/mixins-single-object.txt:41
# fba327a51f784f92ad03004ebb155fd5
msgid "The name of the URLConf keyword argument that contains the slug. By default, ``slug_url_kwarg`` is ``'slug'``."
msgstr ""

#: ../../ref/class-based-views/mixins-single-object.txt:46
# 621bf545f3324e3ba7310687d622d327
msgid "The name of the URLConf keyword argument that contains the primary key. By default, ``pk_url_kwarg`` is ``'pk'``."
msgstr ""

#: ../../ref/class-based-views/mixins-single-object.txt:51
# 2e090e47a2104a619b5a01d2b07d0f67
msgid "Designates the name of the variable to use in the context."
msgstr ""

#: ../../ref/class-based-views/mixins-single-object.txt:55
# 5e3e89bd2ebd40ff92166a11df49320b
msgid "Returns the single object that this view will display. If ``queryset`` is provided, that queryset will be used as the source of objects; otherwise, :meth:`get_queryset` will be used. ``get_object()`` looks for a :attr:`pk_url_kwarg` argument in the arguments to the view; if this argument is found, this method performs a primary-key based lookup using that value. If this argument is not found, it looks for a :attr:`slug_url_kwarg` argument, and performs a slug lookup using the :attr:`slug_field`."
msgstr ""

#: ../../ref/class-based-views/mixins-single-object.txt:66
# a13511d31f8141eda93c86f1639de3b9
msgid "Returns the queryset that will be used to retrieve the object that this view will display. By default, :meth:`get_queryset` returns the value of the :attr:`queryset` attribute if it is set, otherwise it constructs a :class:`~django.db.models.query.QuerySet` by calling the ``all()`` method on the :attr:`model` attribute's default manager."
msgstr ""

#: ../../ref/class-based-views/mixins-single-object.txt:74
# ef1fda2fb6244c6897be8fcc84841b73
msgid "Return the context variable name that will be used to contain the data that this view is manipulating. If :attr:`context_object_name` is not set, the context name will be constructed from the ``model_name`` of the model that the queryset is composed from. For example, the model ``Article`` would have context object named ``'article'``."
msgstr ""

#: ../../ref/class-based-views/mixins-single-object.txt:82
# dde53caadfb54c8dbb1e70ed49252e41
msgid "Returns context data for displaying the list of objects."
msgstr ""

#: ../../ref/class-based-views/mixins-single-object.txt:84
# 57413a2628c94218935ce99a43555534
msgid "The base implementation of this method requires that the ``object`` attribute be set by the view (even if ``None``). Be sure to do this if you are using this mixin without one of the built-in views that does so."
msgstr ""

#: ../../ref/class-based-views/mixins-single-object.txt:90
# 8089eccdd206446da4d957b000466644
msgid "Returns the name of a slug field to be used to look up by slug. By default this simply returns the value of :attr:`slug_field`."
msgstr ""

#: ../../ref/class-based-views/mixins-single-object.txt:93
# 7ffe2c917f704ba78404dd83ed93bc7e
msgid "**Context**"
msgstr ""

#: ../../ref/class-based-views/mixins-single-object.txt:95
# 702a1b8b4e54474a978bcbd6172fd040
msgid "``object``: The object that this view is displaying. If ``context_object_name`` is specified, that variable will also be set in the context, with the same value as ``object``."
msgstr ""

#: ../../ref/class-based-views/mixins-single-object.txt:100
# 00690475113a4201b14609ec3f7aa421
msgid "SingleObjectTemplateResponseMixin"
msgstr ""

#: ../../ref/class-based-views/mixins-single-object.txt:104
# dc410a06da1641ab98ba02d1b3950fd1
msgid "A mixin class that performs template-based response rendering for views that operate upon a single object instance. Requires that the view it is mixed with provides ``self.object``, the object instance that the view is operating on. ``self.object`` will usually be, but is not required to be, an instance of a Django model. It may be ``None`` if the view is in the process of constructing a new instance."
msgstr ""

#: ../../ref/class-based-views/mixins-single-object.txt:111
# 5859ee57bdd24eaab01f44d02c43c263
msgid "**Extends**"
msgstr ""

#: ../../ref/class-based-views/mixins-single-object.txt:113
# 1dacd426f0a446b88c4e4f2bcfb1e63b
msgid ":class:`~django.views.generic.base.TemplateResponseMixin`"
msgstr ""

#: ../../ref/class-based-views/mixins-single-object.txt:119
# 7bf3754ef07646049e27b1d27ba2f88b
msgid "The field on the current object instance that can be used to determine the name of a candidate template. If either ``template_name_field`` itself or the value of the ``template_name_field`` on the current object instance is ``None``, the object will not be used for a candidate template name."
msgstr ""

#: ../../ref/class-based-views/mixins-single-object.txt:127
# 285bec5a018f4f30a8981a9332d888df
msgid "The suffix to append to the auto-generated candidate template name. Default suffix is ``_detail``."
msgstr ""

#: ../../ref/class-based-views/mixins-single-object.txt:132
# f52c73d568434c56af5e26bb457ed880
msgid "Returns a list of candidate template names. Returns the following list:"
msgstr ""

#: ../../ref/class-based-views/mixins-single-object.txt:134
# 75e60865f1c648daaa186580c4559da3
msgid "the value of ``template_name`` on the view (if provided)"
msgstr ""

#: ../../ref/class-based-views/mixins-single-object.txt:135
# b8c73d9f0a70481cb4e854a1144c1fde
msgid "the contents of the ``template_name_field`` field on the object instance that the view is operating upon (if available)"
msgstr ""

#: ../../ref/class-based-views/mixins-single-object.txt:137
# 75aca82a182841a3836ec55014f6f07a
msgid "``<app_label>/<model_name><template_name_suffix>.html``"
msgstr ""

