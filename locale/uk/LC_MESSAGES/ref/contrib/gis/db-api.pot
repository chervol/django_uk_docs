# SOME DESCRIPTIVE TITLE.
# Copyright (C) Django Software Foundation and contributors
# This file is distributed under the same license as the Django package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: Django 1.7\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2014-10-13 18:33+0300\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#: ../../ref/contrib/gis/db-api.txt:5
# 042f1d18230b4e86a24ddf31a423f312
msgid "GeoDjango Database API"
msgstr ""

#: ../../ref/contrib/gis/db-api.txt:10
# cad32ff0931c4eeeb0ecd5d29556d227
msgid "Spatial Backends"
msgstr ""

#: ../../ref/contrib/gis/db-api.txt:15
# 42092be1ddd24698848a063e4d8f9662
msgid "GeoDjango currently provides the following spatial database backends:"
msgstr ""

#: ../../ref/contrib/gis/db-api.txt:17
# f7bf9e67a13140478c22b80611f01581
msgid "``django.contrib.gis.db.backends.postgis``"
msgstr ""

#: ../../ref/contrib/gis/db-api.txt:18
# 3c1dcbef282b4b59a86a1f449c1119b0
msgid "``django.contrib.gis.db.backends.mysql``"
msgstr ""

#: ../../ref/contrib/gis/db-api.txt:19
# 606481a1050644a7a7df7218dc9c3ca4
msgid "``django.contrib.gis.db.backends.oracle``"
msgstr ""

#: ../../ref/contrib/gis/db-api.txt:20
# 16315a35ee64430b89a7ed02ffd1a47b
msgid "``django.contrib.gis.db.backends.spatialite``"
msgstr ""

#: ../../ref/contrib/gis/db-api.txt:28
# 66fb28fd134c450ab90d2ada4db1ffc8
msgid "MySQL Spatial Limitations"
msgstr ""

#: ../../ref/contrib/gis/db-api.txt:30
# b4a1799ec80a45e2b454fd8010e6b146
msgid "MySQL's spatial extensions only support bounding box operations (what MySQL calls minimum bounding rectangles, or MBR).  Specifically, `MySQL does not conform to the OGC standard <http://dev.mysql.com/doc/refman/5.6/en/spatial-relation-functions.html>`_:"
msgstr ""

#: ../../ref/contrib/gis/db-api.txt:35
# d30b37393582446190bd46aa7554e60a
msgid "Currently, MySQL does not implement these functions [``Contains``, ``Crosses``, ``Disjoint``, ``Intersects``, ``Overlaps``, ``Touches``, ``Within``] according to the specification.  Those that are implemented return the same result as the corresponding MBR-based functions."
msgstr ""

#: ../../ref/contrib/gis/db-api.txt:41
# 0719f78a922d4f12966071a8876afc50
msgid "In other words, while spatial lookups such as :lookup:`contains <gis-contains>` are available in GeoDjango when using MySQL, the results returned are really equivalent to what would be returned when using :lookup:`bbcontains` on a different spatial backend."
msgstr ""

#: ../../ref/contrib/gis/db-api.txt:48
# 942634679af14887891c33a491bb113c
msgid "True spatial indexes (R-trees) are only supported with MyISAM tables on MySQL. [#fnmysqlidx]_ In other words, when using MySQL spatial extensions you have to choose between fast spatial lookups and the integrity of your data -- MyISAM tables do not support transactions or foreign key constraints."
msgstr ""

#: ../../ref/contrib/gis/db-api.txt:55
# 830849870bb5464c92fbd95e373c7102
msgid "Creating and Saving Geographic Models"
msgstr ""

#: ../../ref/contrib/gis/db-api.txt:57
# 5a58f50411e1496692f26acaa2bb32f8
msgid "Here is an example of how to create a geometry object (assuming the ``Zipcode`` model)::"
msgstr ""

#: ../../ref/contrib/gis/db-api.txt:64
# e0c9b5427c3049148b9f6236bc4c16af
msgid ":class:`~django.contrib.gis.geos.GEOSGeometry` objects may also be used to save geometric models::"
msgstr ""

#: ../../ref/contrib/gis/db-api.txt:71
# 8b906eed92f54918a66709269828f84f
msgid "Moreover, if the ``GEOSGeometry`` is in a different coordinate system (has a different SRID value) than that of the field, then it will be implicitly transformed into the SRID of the model's field, using the spatial database's transform procedure::"
msgstr ""

#: ../../ref/contrib/gis/db-api.txt:83
# 68af8b76eb4240dba3674ce4f21d345b
msgid "Thus, geometry parameters may be passed in using the ``GEOSGeometry`` object, WKT (Well Known Text [#fnwkt]_), HEXEWKB (PostGIS specific -- a WKB geometry in hexadecimal [#fnewkb]_), and GeoJSON [#fngeojson]_ (requires GDAL). Essentially, if the input is not a ``GEOSGeometry`` object, the geometry field will attempt to create a ``GEOSGeometry`` instance from the input."
msgstr ""

#: ../../ref/contrib/gis/db-api.txt:89
# 5cb6fd1a28c64b3eb970146cd04ca76a
msgid "For more information creating :class:`~django.contrib.gis.geos.GEOSGeometry` objects, refer to the :ref:`GEOS tutorial <geos-tutorial>`."
msgstr ""

#: ../../ref/contrib/gis/db-api.txt:95
#: ../../ref/contrib/gis/db-api.txt:229
# c6ff62a5e85c4294a23c0a2ad7edfa79
# 9ef3615e141b498381b33f6f9db77602
msgid "Spatial Lookups"
msgstr ""

#: ../../ref/contrib/gis/db-api.txt:97
# 7e3c5cc02dd04e7a8f6431f6aad71206
msgid "GeoDjango's lookup types may be used with any manager method like ``filter()``, ``exclude()``, etc.  However, the lookup types unique to GeoDjango are only available on geometry fields. Filters on 'normal' fields (e.g. :class:`~django.db.models.CharField`) may be chained with those on geographic fields.  Thus, geographic queries take the following general form (assuming  the ``Zipcode`` model used in the :ref:`ref-gis-model-api`)::"
msgstr ""

#: ../../ref/contrib/gis/db-api.txt:108
# 9132827a07a34266996b9aebbdd130a5
msgid "For example::"
msgstr ""

#: ../../ref/contrib/gis/db-api.txt:112
# a67fdeedfa424a9d8aa49a48882e8fd4
msgid "In this case, ``poly`` is the geographic field, :lookup:`contains <gis-contains>` is the spatial lookup type, and ``pnt`` is the parameter (which may be a :class:`~django.contrib.gis.geos.GEOSGeometry` object or a string of GeoJSON , WKT, or HEXEWKB)."
msgstr ""

#: ../../ref/contrib/gis/db-api.txt:117
# 99bd58e73bd040b3a46a8747b0e81ce8
msgid "A complete reference can be found in the :ref:`spatial lookup reference <spatial-lookups>`."
msgstr ""

#: ../../ref/contrib/gis/db-api.txt:122
# c4bb1b059917465eb771b3aadabcd2b6
msgid "GeoDjango constructs spatial SQL with the :class:`GeoQuerySet`, a subclass of :class:`~django.db.models.query.QuerySet`.  The :class:`GeoManager` instance attached to your model is what enables use of :class:`GeoQuerySet`."
msgstr ""

#: ../../ref/contrib/gis/db-api.txt:130
# 109198da3abc4c68983da0a64167fd8a
msgid "Distance Queries"
msgstr ""

#: ../../ref/contrib/gis/db-api.txt:133
# 311193f3979f4372905f20639fb56a37
msgid "Introduction"
msgstr ""

#: ../../ref/contrib/gis/db-api.txt:134
# 0d105ebf4d504d87b536d188dc73fb17
msgid "Distance calculations with spatial data is tricky because, unfortunately, the Earth is not flat.  Some distance queries with fields in a geographic coordinate system may have to be expressed differently because of limitations in PostGIS.  Please see the :ref:`selecting-an-srid` section in the :ref:`ref-gis-model-api` documentation for more details."
msgstr ""

#: ../../ref/contrib/gis/db-api.txt:143
# 2ba153285793401eba19bc0c0be51766
msgid "Distance Lookups"
msgstr ""

#: ../../ref/contrib/gis/db-api.txt:144
# 06731d7386b949abaec4231ff8ed7b21
msgid "*Availability*: PostGIS, Oracle, SpatiaLite"
msgstr ""

#: ../../ref/contrib/gis/db-api.txt:146
# f6185d9654f04c51ac441f13f6216dcb
msgid "The following distance lookups are available:"
msgstr ""

#: ../../ref/contrib/gis/db-api.txt:148
#: ../../ref/contrib/gis/db-api.txt:248
# 73353e52fd744fb194d74dddc340683b
# b59d054ba3b24136bb1e315224b59455
msgid ":lookup:`distance_lt`"
msgstr ""

#: ../../ref/contrib/gis/db-api.txt:149
#: ../../ref/contrib/gis/db-api.txt:249
# 610e83e437654ed6b591f919f582cf09
# b425d2810f284ef4b8c203cdcf7c60ab
msgid ":lookup:`distance_lte`"
msgstr ""

#: ../../ref/contrib/gis/db-api.txt:150
#: ../../ref/contrib/gis/db-api.txt:246
# 6b9ba2c6792549d9b53dc3b514fa0b48
# 42a033b7c9e4438c809afd8a4055244a
msgid ":lookup:`distance_gt`"
msgstr ""

#: ../../ref/contrib/gis/db-api.txt:151
#: ../../ref/contrib/gis/db-api.txt:247
# 53563a0291ae4046a2b57ab3f95c29cb
# a10ba121ffb9409899f8ca3d84a9276a
msgid ":lookup:`distance_gte`"
msgstr ""

#: ../../ref/contrib/gis/db-api.txt:152
#: ../../ref/contrib/gis/db-api.txt:250
# f39a83ecf3e34b7f8d940e2e4942ac33
# a81adfa111204dbab3496c0153da92d6
msgid ":lookup:`dwithin`"
msgstr ""

#: ../../ref/contrib/gis/db-api.txt:156
# 5d2b968eba2a4a5cbaa899eb14290375
msgid "For *measuring*, rather than querying on distances, use the :meth:`GeoQuerySet.distance` method."
msgstr ""

#: ../../ref/contrib/gis/db-api.txt:159
# 36ac08b237c341f4b89ac8d5742fbaef
msgid "Distance lookups take a tuple parameter comprising:"
msgstr ""

#: ../../ref/contrib/gis/db-api.txt:161
# a617a2a4c75a4d77964005eff4b9dbcb
msgid "A geometry to base calculations from; and"
msgstr ""

#: ../../ref/contrib/gis/db-api.txt:162
# f691dcd5c2884b288a0d06038a09f15b
msgid "A number or :class:`~django.contrib.gis.measure.Distance` object containing the distance."
msgstr ""

#: ../../ref/contrib/gis/db-api.txt:164
# a640395db0464c2b99b1b31950338fe3
msgid "If a :class:`~django.contrib.gis.measure.Distance` object is used, it may be expressed in any units (the SQL generated will use units converted to those of the field); otherwise, numeric parameters are assumed to be in the units of the field."
msgstr ""

#: ../../ref/contrib/gis/db-api.txt:171
# d4ef61e439884ebc8e771ba6da8169fe
msgid "For users of PostGIS 1.4 and below, the routine ``ST_Distance_Sphere`` is used by default for calculating distances on geographic coordinate systems (e.g., WGS84) -- which may only be called with point geometries [#fndistsphere14]_. Thus, geographic distance lookups on traditional PostGIS geometry columns are only allowed on :class:`PointField` model fields using a point for the geometry parameter."
msgstr ""

#: ../../ref/contrib/gis/db-api.txt:180
# b340b3304114478b8a5e8b1358fab535
msgid "In PostGIS 1.5, ``ST_Distance_Sphere`` does *not* limit the geometry types geographic distance queries are performed with. [#fndistsphere15]_  However, these queries may take a long time, as great-circle distances must be calculated on the fly for *every* row in the query.  This is because the spatial index on traditional geometry fields cannot be used."
msgstr ""

#: ../../ref/contrib/gis/db-api.txt:186
# 2677cf2d71c94aeaa32e9828d288399f
msgid "For much better performance on WGS84 distance queries, consider using :ref:`geography columns <geography-type>` in your database instead because they are able to use their spatial index in distance queries. You can tell GeoDjango to use a geography column by setting ``geography=True`` in your field definition."
msgstr ""

#: ../../ref/contrib/gis/db-api.txt:192
# 6b37544440a74471a39d1aa7c75b771a
msgid "For example, let's say we have a ``SouthTexasCity`` model (from the `GeoDjango distance tests`__ ) on a *projected* coordinate system valid for cities in southern Texas::"
msgstr ""

#: ../../ref/contrib/gis/db-api.txt:205
# eb11f2c78fb94fe7a468437f67d5d2e5
msgid "Then distance queries may be performed as follows::"
msgstr ""

#: ../../ref/contrib/gis/db-api.txt:224
# 559bf5b6ca0d484dacc42a99291aec4f
msgid "Compatibility Tables"
msgstr ""

#: ../../ref/contrib/gis/db-api.txt:231
# db9b17f0383c45acb168c48525f27614
msgid "The following table provides a summary of what spatial lookups are available for each spatial database backend."
msgstr ""

#: ../../ref/contrib/gis/db-api.txt:235
# 9e174ef093854423a55c21305e3ca1f3
msgid "Lookup Type"
msgstr ""

#: ../../ref/contrib/gis/db-api.txt:235
#: ../../ref/contrib/gis/db-api.txt:278
# d7a6f6f31f154009be0e634676828130
# c95cb29509834d03b881b5d92fb08f6f
msgid "PostGIS"
msgstr ""

#: ../../ref/contrib/gis/db-api.txt:235
#: ../../ref/contrib/gis/db-api.txt:278
# 370d407b4e9e46e8b9e42b74d14bf625
# 46d776ee268e4f74af29a5ab4046e72f
msgid "Oracle"
msgstr ""

#: ../../ref/contrib/gis/db-api.txt:235
# 89b53074fa5541248e8e66d6e05ea72a
msgid "MySQL [#]_"
msgstr ""

#: ../../ref/contrib/gis/db-api.txt:235
#: ../../ref/contrib/gis/db-api.txt:278
# be91088a5a7a4c62a7413b3d0beb6c11
# 0b334f66085342fa8abaf1934f86011a
msgid "SpatiaLite"
msgstr ""

#: ../../ref/contrib/gis/db-api.txt:237
# 936a010c953d4b8e8a6ead06f0baa5ce
msgid ":lookup:`bbcontains`"
msgstr ""

#: ../../ref/contrib/gis/db-api.txt:237
#: ../../ref/contrib/gis/db-api.txt:237
#: ../../ref/contrib/gis/db-api.txt:237
#: ../../ref/contrib/gis/db-api.txt:238
#: ../../ref/contrib/gis/db-api.txt:238
#: ../../ref/contrib/gis/db-api.txt:238
#: ../../ref/contrib/gis/db-api.txt:239
#: ../../ref/contrib/gis/db-api.txt:239
#: ../../ref/contrib/gis/db-api.txt:239
#: ../../ref/contrib/gis/db-api.txt:240
#: ../../ref/contrib/gis/db-api.txt:240
#: ../../ref/contrib/gis/db-api.txt:240
#: ../../ref/contrib/gis/db-api.txt:240
#: ../../ref/contrib/gis/db-api.txt:241
#: ../../ref/contrib/gis/db-api.txt:242
#: ../../ref/contrib/gis/db-api.txt:242
#: ../../ref/contrib/gis/db-api.txt:243
#: ../../ref/contrib/gis/db-api.txt:243
#: ../../ref/contrib/gis/db-api.txt:244
#: ../../ref/contrib/gis/db-api.txt:244
#: ../../ref/contrib/gis/db-api.txt:245
#: ../../ref/contrib/gis/db-api.txt:245
#: ../../ref/contrib/gis/db-api.txt:245
#: ../../ref/contrib/gis/db-api.txt:245
#: ../../ref/contrib/gis/db-api.txt:246
#: ../../ref/contrib/gis/db-api.txt:246
#: ../../ref/contrib/gis/db-api.txt:246
#: ../../ref/contrib/gis/db-api.txt:247
#: ../../ref/contrib/gis/db-api.txt:247
#: ../../ref/contrib/gis/db-api.txt:247
#: ../../ref/contrib/gis/db-api.txt:248
#: ../../ref/contrib/gis/db-api.txt:248
#: ../../ref/contrib/gis/db-api.txt:248
#: ../../ref/contrib/gis/db-api.txt:249
#: ../../ref/contrib/gis/db-api.txt:249
#: ../../ref/contrib/gis/db-api.txt:249
#: ../../ref/contrib/gis/db-api.txt:250
#: ../../ref/contrib/gis/db-api.txt:250
#: ../../ref/contrib/gis/db-api.txt:251
#: ../../ref/contrib/gis/db-api.txt:251
#: ../../ref/contrib/gis/db-api.txt:251
#: ../../ref/contrib/gis/db-api.txt:251
#: ../../ref/contrib/gis/db-api.txt:252
#: ../../ref/contrib/gis/db-api.txt:252
#: ../../ref/contrib/gis/db-api.txt:252
#: ../../ref/contrib/gis/db-api.txt:252
#: ../../ref/contrib/gis/db-api.txt:253
#: ../../ref/contrib/gis/db-api.txt:253
#: ../../ref/contrib/gis/db-api.txt:253
#: ../../ref/contrib/gis/db-api.txt:253
#: ../../ref/contrib/gis/db-api.txt:254
#: ../../ref/contrib/gis/db-api.txt:254
#: ../../ref/contrib/gis/db-api.txt:254
#: ../../ref/contrib/gis/db-api.txt:254
#: ../../ref/contrib/gis/db-api.txt:255
#: ../../ref/contrib/gis/db-api.txt:255
#: ../../ref/contrib/gis/db-api.txt:255
#: ../../ref/contrib/gis/db-api.txt:256
#: ../../ref/contrib/gis/db-api.txt:256
#: ../../ref/contrib/gis/db-api.txt:256
#: ../../ref/contrib/gis/db-api.txt:256
#: ../../ref/contrib/gis/db-api.txt:257
#: ../../ref/contrib/gis/db-api.txt:257
#: ../../ref/contrib/gis/db-api.txt:257
#: ../../ref/contrib/gis/db-api.txt:257
#: ../../ref/contrib/gis/db-api.txt:258
#: ../../ref/contrib/gis/db-api.txt:258
#: ../../ref/contrib/gis/db-api.txt:258
#: ../../ref/contrib/gis/db-api.txt:258
#: ../../ref/contrib/gis/db-api.txt:259
#: ../../ref/contrib/gis/db-api.txt:260
#: ../../ref/contrib/gis/db-api.txt:261
#: ../../ref/contrib/gis/db-api.txt:262
#: ../../ref/contrib/gis/db-api.txt:263
#: ../../ref/contrib/gis/db-api.txt:264
#: ../../ref/contrib/gis/db-api.txt:265
#: ../../ref/contrib/gis/db-api.txt:266
#: ../../ref/contrib/gis/db-api.txt:280
#: ../../ref/contrib/gis/db-api.txt:280
#: ../../ref/contrib/gis/db-api.txt:280
#: ../../ref/contrib/gis/db-api.txt:281
#: ../../ref/contrib/gis/db-api.txt:281
#: ../../ref/contrib/gis/db-api.txt:281
#: ../../ref/contrib/gis/db-api.txt:282
#: ../../ref/contrib/gis/db-api.txt:283
#: ../../ref/contrib/gis/db-api.txt:283
#: ../../ref/contrib/gis/db-api.txt:283
#: ../../ref/contrib/gis/db-api.txt:284
#: ../../ref/contrib/gis/db-api.txt:284
#: ../../ref/contrib/gis/db-api.txt:284
#: ../../ref/contrib/gis/db-api.txt:285
#: ../../ref/contrib/gis/db-api.txt:285
#: ../../ref/contrib/gis/db-api.txt:286
#: ../../ref/contrib/gis/db-api.txt:286
#: ../../ref/contrib/gis/db-api.txt:287
#: ../../ref/contrib/gis/db-api.txt:288
#: ../../ref/contrib/gis/db-api.txt:289
#: ../../ref/contrib/gis/db-api.txt:290
#: ../../ref/contrib/gis/db-api.txt:290
#: ../../ref/contrib/gis/db-api.txt:291
#: ../../ref/contrib/gis/db-api.txt:291
#: ../../ref/contrib/gis/db-api.txt:291
#: ../../ref/contrib/gis/db-api.txt:292
#: ../../ref/contrib/gis/db-api.txt:292
#: ../../ref/contrib/gis/db-api.txt:292
#: ../../ref/contrib/gis/db-api.txt:293
#: ../../ref/contrib/gis/db-api.txt:293
#: ../../ref/contrib/gis/db-api.txt:294
#: ../../ref/contrib/gis/db-api.txt:294
#: ../../ref/contrib/gis/db-api.txt:294
#: ../../ref/contrib/gis/db-api.txt:295
#: ../../ref/contrib/gis/db-api.txt:296
#: ../../ref/contrib/gis/db-api.txt:297
#: ../../ref/contrib/gis/db-api.txt:297
#: ../../ref/contrib/gis/db-api.txt:297
#: ../../ref/contrib/gis/db-api.txt:298
#: ../../ref/contrib/gis/db-api.txt:298
#: ../../ref/contrib/gis/db-api.txt:298
#: ../../ref/contrib/gis/db-api.txt:299
#: ../../ref/contrib/gis/db-api.txt:299
#: ../../ref/contrib/gis/db-api.txt:300
#: ../../ref/contrib/gis/db-api.txt:300
#: ../../ref/contrib/gis/db-api.txt:300
#: ../../ref/contrib/gis/db-api.txt:301
#: ../../ref/contrib/gis/db-api.txt:301
#: ../../ref/contrib/gis/db-api.txt:302
#: ../../ref/contrib/gis/db-api.txt:302
#: ../../ref/contrib/gis/db-api.txt:303
#: ../../ref/contrib/gis/db-api.txt:304
#: ../../ref/contrib/gis/db-api.txt:304
#: ../../ref/contrib/gis/db-api.txt:305
#: ../../ref/contrib/gis/db-api.txt:305
#: ../../ref/contrib/gis/db-api.txt:305
#: ../../ref/contrib/gis/db-api.txt:306
#: ../../ref/contrib/gis/db-api.txt:306
#: ../../ref/contrib/gis/db-api.txt:306
#: ../../ref/contrib/gis/db-api.txt:307
#: ../../ref/contrib/gis/db-api.txt:307
#: ../../ref/contrib/gis/db-api.txt:308
#: ../../ref/contrib/gis/db-api.txt:308
#: ../../ref/contrib/gis/db-api.txt:308
#: ../../ref/contrib/gis/db-api.txt:309
#: ../../ref/contrib/gis/db-api.txt:309
#: ../../ref/contrib/gis/db-api.txt:309
# 059d8b2c15364b40ba1e688214c68d4c
# aa3d3473aaae4f02ae5a88ab41e16d60
# 1340c6edd90a4e5585ad0cb07072683c
# c679ce5ef4f84c5e83074f432ebb5fe6
# 0345a27b05e24d2887aa1dad9203bcc6
# 0a5c8f8755c942e381fb388fe85cb00e
# 8c6f9052f452441e94eb23b808fe6532
# 11b0da07b9c64ac0b1c61e1a4ad657fa
# 6ff6a9b1c55d4610b09c445c458aca86
# d63e945b21474c63b86569a3313621d3
# e321fbbea66b40f2a89c22715706b01f
# d8527830334e4c938871236396726065
# e0db3631edc944eda98bed8ad3bda8d5
# e6aefbd1fc314b0d8100ff30f687cb6f
# 2051867f2aa44ad4b0143029378700a2
# 1c16346ad9244b4ab9994b02b76ebfda
# a8d65133a701409d8e64b7d575aae9dd
# 99472b55ed1b4b669f3a836ffd964b15
# ec9ad6bdb43f4516bf3e00bc856270ea
# eead99fe0db0402da65e6ef2993d9f83
# a52111100f7e41aea85e5c0dbdb7c8de
# 9f5178b59c9a48c2a6d290acf5729152
# 55167ee07f9b4465b867246df6391691
# c804e5a4901041258d04994d9a83cc25
# e5fac601b2c24dc8af18c3ad1d8478ab
# 6cd61595dfbc448881c37212b67f2545
# 8cff619e1b68404884a5c344cd806267
# ad42f78ccbb443b18b3b9e84589dcc2d
# 453974f0e0634b77847e013c9da2c4de
# b9dcf69d2b604ba4a03c161204ef5b96
# 21a693d3201844449dd7e617615c7aad
# 69b0aa1057c54eba9a982163dabb8f98
# 3dc5f24386914d58b4fb920cdf68ccf8
# c0e8309b6f954f95bf99c3dcb075c9ed
# dd1ef9395f2a4648b306b83acf256ade
# c686a2e62d614b51957f8790a9286826
# 2d2a5989aa8b4e03a3baf31b4908aae8
# 0d2de64494af4f6e8b2967c13d8a8a13
# afd878d6b96641f294bfd149b13ab074
# 1d1a2d3ce693424398d69909f8e209fc
# fe9fb27a118a434c8f2c799281962bee
# 3ed8ee5729bc42bfa3a4b0a40607c195
# a8e1e35079fa437780b1f109a3041eed
# f8aedb53206c4f66a7076d323c2089fd
# 0ff39ab38e85406681afd75c75601f76
# 5b9e94dd287943b18787cf5128acd971
# 768663ab72fc43b6b676a7c7c3a4e913
# 4db00f7836cd4d28bbdb8a1313fb94f9
# eecaa7335b424193901a8f8703124583
# e39c9fd0c709433eb3d8378a043c7211
# 1752cfaff5af44dcb43008bacdff3694
# f60943b624e54c82b575f93a143d0ff8
# 23f798f4dc834856b6d49bafbae853a5
# f0aadd295f8c4351b214ace00f409143
# e5f5c16a2fcd46e29f00031609e25824
# b648615870604313858e1cbf670c9726
# adf5e56fc1cb412d9678b2d9faf6bc6e
# 436affc5ec3b4ed6a37ef16edaa157b2
# 49ac06275ae1481a9d9dca4d0475c1d3
# e14d1fc04ff0419e99bc607c29e3b0b1
# 44479ea3bc6549a09bf87e3ec5073d12
# ce7e87e09336456bbef09e572ff4522a
# ed920e26383d4ad2887296a00295c70f
# 0ed88192c92f4c4da4964c96b6ea935a
# 9903291650ac418cb742bcb43704e46f
# f39e56d4c8f64fc68160bfde361feec9
# b9331749458c447ab168de793b6f5e97
# 63ffd08fd4eb4e4bbd6d40bdb256c3e1
# f91fb4aeebb64dbabc276bfcea5e58a5
# 261cd1c973ab4894a8de64a7572bb18f
# d0e74d5cdba4478a8252303e61eccb28
# 385aef094cf34341add8beef53319807
# 8918bca9c0db4a03bb0b6459c5e1a8aa
# b662fa7b1fee4efe84a46ab02100b501
# a6f03941665b4067abbf58734a109dd7
# bfd97b09fbd3492db6d5644d8b598008
# f5eb0ab86b784a04a5288b0f3f608967
# 3277399eaca447c2acdbbab5346e523f
# 156e17677613482aa823ce6d59b2fb17
# 02d31d1493694df0bd7b58fa8b07d633
# ea16d5b7c9b741e2ad24ff2601ca9791
# 0ea79249a76b4fd0919b3befa66981c7
# 4ff78577cdcd477ab1e57f82e554b2e2
# f0f3d57d8c98495f95ab7d7f3a0041f6
# 756e4c2c18bb4123add4f6d0cbb5c165
# a49c9942813f46e39af123dee300d2aa
# 63a295a4548e4a7ca46d9218f7e55d4e
# 331418d3976147188edd7b9d8da69689
# 2dbe4c521dee4c9db43addb13ece0295
# 9afb8b1c14b04dd18e75a5e39ee80ba4
# 10d02f13197144fab3a5055904fc713d
# d71a2b0b3e6e47d7b658ce2a7f68f64d
# 4291eabc06b94897b8a89a340c3291d1
# fdc890778e514d7d947709062e62b58f
# 3cfeced29b1443f8a50e85492f15c6e6
# 0e9e0e7db2c249159c551c8e2910e26d
# fdda512cd557422b8afd3f63dc966c86
# cb0677d86598477f961e61b65d23c671
# d2292c55997340e8bdc40d27417f8101
# 489c7d0c245b4b4db1eb8c6dbce30fe7
# 5c9ead41376b442b92ce88a0e31c55f3
# 03a96b12ba3540c1ac5cac162e8be094
# 46d563eeb0624d5b94d84e82ae016ae5
# ad0ef66413334cf9bc422be80f70e50a
# 9d27bad4f3fd4ff58b47b2be446e79b8
# e71b4b641dc143159ea7c53e78728faf
# 745d0da70bae4613b57d58cb691a7c5b
# d1078f2ba5c0498d8a6d83566d8bf334
# ed49a4f8801a4e8fa833314ab2e82e4d
# 53a99712fe834b90bb32e13295a796b9
# a9136e2ad7d4445ca14840661d86f435
# 512ec7e8892c49cba2f0d40c23f1eeb3
# 50672e1047734349b0474e149b3474d7
# 58268da165b94362850c296331a73aad
# ed3537794ef44b238d4e4a27b2c893bd
# 1dcbb5e4501b4b06804d417d20bb622b
# 9f8656dc22944debb19da633aabfe532
# aae7226727a84bb0b3cc966f1c192510
# 78df15ac2c374a658863ea5591d876b3
# be133bfbc5a54426bc3b10b1f92a4e80
# 2ba53708dcba4690b85ab69228b8b051
# d525b7d501e445a3a130189905ceed9f
# ca3dc8e4ec5141259ba5d5f64da7d394
# 2b827b1f20ed470a8d74a825c81e84d8
# 955c9f2cc7c74dfc944aab331ed6aab7
# 332bdcc5824e481d8ac6468032d7c2ba
# eaf0e676682644aabd9def99eee24ef5
# 02b0e00243924aa88a7ca807aa08b4b7
# 6a82d8732ca645bda979ddfa98f8e9f1
# 52e010d185fd44548b2ab04bd0c7307c
# 2d4cc98c08db48a498329127eb0bdefb
# 66f5d053a7ad401ea21cbbf824f1f756
# 2ab9137d90954cb8bb21dec82dacbdf7
# e8c786fff13f48e2be47772170046a50
# 310a2fc6e9bc4d7e9b8374a366cd5bd9
# d2065350927a4995bbdb9368a734cb72
# 0e0e4fc0dd194786a778904d2661555a
# a2ed86c5ecbf4db0b8937fb077a42e65
# 821a955b45704571b4f0d41cb3158a43
# a29e01c53cb14c43ad0d9d2cb9d2cecc
# c8b57698ef5642dbbe7c340a0f384151
# 428ef34b9e1f4cd6913e790c45b604f4
# 72911fa73ab34eb7ba5a4edeb2cfab9c
# b1920e8aed9f453bb4f61e801312ab08
msgid "X"
msgstr ""

#: ../../ref/contrib/gis/db-api.txt:238
# 873cfb5d80cc47cd8dd10f0828d5ab7c
msgid ":lookup:`bboverlaps`"
msgstr ""

#: ../../ref/contrib/gis/db-api.txt:239
# fee1df41b0d840ce9a9ec8a1c3856096
msgid ":lookup:`contained`"
msgstr ""

#: ../../ref/contrib/gis/db-api.txt:240
# 21eaa76c65d341cea538c8ad443e0548
msgid ":lookup:`contains <gis-contains>`"
msgstr ""

#: ../../ref/contrib/gis/db-api.txt:241
# 2796441b4cd34ab89cc60df1e39140b6
msgid ":lookup:`contains_properly`"
msgstr ""

#: ../../ref/contrib/gis/db-api.txt:242
# c64b1c53e8264aaab72b31115739ba51
msgid ":lookup:`coveredby`"
msgstr ""

#: ../../ref/contrib/gis/db-api.txt:243
# bfe73fb24f954eba85dc75d091c5ab36
msgid ":lookup:`covers`"
msgstr ""

#: ../../ref/contrib/gis/db-api.txt:244
# b596b668339343c9b97fd1201d0275e0
msgid ":lookup:`crosses`"
msgstr ""

#: ../../ref/contrib/gis/db-api.txt:245
# 707510dcbd7b45198ecb35936295524f
msgid ":lookup:`disjoint`"
msgstr ""

#: ../../ref/contrib/gis/db-api.txt:251
# 30875170c1574805bedad20ae921562b
msgid ":lookup:`equals`"
msgstr ""

#: ../../ref/contrib/gis/db-api.txt:252
# 66813213d2b049b0b1bafd12716b575e
msgid ":lookup:`exact`"
msgstr ""

#: ../../ref/contrib/gis/db-api.txt:253
# 23cb8903aa724d68b17f386834221fb8
msgid ":lookup:`intersects`"
msgstr ""

#: ../../ref/contrib/gis/db-api.txt:254
# 357821ff0c3243eebdef01f1202df6c0
msgid ":lookup:`overlaps`"
msgstr ""

#: ../../ref/contrib/gis/db-api.txt:255
# 66b6156fc7bc471aa120ff88fa6a600d
msgid ":lookup:`relate`"
msgstr ""

#: ../../ref/contrib/gis/db-api.txt:256
# cade7d2adfff42a5b5c49fd9fb169978
msgid ":lookup:`same_as`"
msgstr ""

#: ../../ref/contrib/gis/db-api.txt:257
# 7348548650be43318829539b9cd3f31e
msgid ":lookup:`touches`"
msgstr ""

#: ../../ref/contrib/gis/db-api.txt:258
# 46dd8121a7f14a2685b905ab8be307f1
msgid ":lookup:`within`"
msgstr ""

#: ../../ref/contrib/gis/db-api.txt:259
# 55ca18a83c6549848278e9f861b66294
msgid ":lookup:`left`"
msgstr ""

#: ../../ref/contrib/gis/db-api.txt:260
# b244d0ac11504a80bd39974557008224
msgid ":lookup:`right`"
msgstr ""

#: ../../ref/contrib/gis/db-api.txt:261
# bb61b0b4f6a54f7f8f18e58e3519c39b
msgid ":lookup:`overlaps_left`"
msgstr ""

#: ../../ref/contrib/gis/db-api.txt:262
# d8a2d914435d4f72af71845f88cc44a4
msgid ":lookup:`overlaps_right`"
msgstr ""

#: ../../ref/contrib/gis/db-api.txt:263
# 601c92f7665f46bb9b307eaed096802a
msgid ":lookup:`overlaps_above`"
msgstr ""

#: ../../ref/contrib/gis/db-api.txt:264
# 37dd742bf0b946928e8554df2aec21ab
msgid ":lookup:`overlaps_below`"
msgstr ""

#: ../../ref/contrib/gis/db-api.txt:265
# 8fabc0359d694ae3a3f30faa52b477c0
msgid ":lookup:`strictly_above`"
msgstr ""

#: ../../ref/contrib/gis/db-api.txt:266
# 7252efc7686946daaaf9a830389bac67
msgid ":lookup:`strictly_below`"
msgstr ""

#: ../../ref/contrib/gis/db-api.txt:272
# e8c50907e6ca4e16947e1a31042e178d
msgid "``GeoQuerySet`` Methods"
msgstr ""

#: ../../ref/contrib/gis/db-api.txt:273
# fd5eb0652c8f4424a853bace459b094e
msgid "The following table provides a summary of what :class:`GeoQuerySet` methods are available on each spatial backend.  Please note that MySQL does not support any of these methods, and is thus excluded from the table."
msgstr ""

#: ../../ref/contrib/gis/db-api.txt:278
# 108c5e79980e4b97a37b4a2a17c2eddb
msgid "Method"
msgstr ""

#: ../../ref/contrib/gis/db-api.txt:280
# 194bfebb92fa4364aad3dfbfea7984ee
msgid ":meth:`GeoQuerySet.area`"
msgstr ""

#: ../../ref/contrib/gis/db-api.txt:281
# 944cdecb0a974e3fa70dc7ba03d8b22f
msgid ":meth:`GeoQuerySet.centroid`"
msgstr ""

#: ../../ref/contrib/gis/db-api.txt:282
# 0cd187475f6e4a399b4d8acd62cb2fa9
msgid ":meth:`GeoQuerySet.collect`"
msgstr ""

#: ../../ref/contrib/gis/db-api.txt:283
# bc003a8357b144a9af0f8da4dbdf7904
msgid ":meth:`GeoQuerySet.difference`"
msgstr ""

#: ../../ref/contrib/gis/db-api.txt:284
# c174fff22fab45d796a61829c8771d89
msgid ":meth:`GeoQuerySet.distance`"
msgstr ""

#: ../../ref/contrib/gis/db-api.txt:285
# 6c39b057a7364665ae905ff6c652e838
msgid ":meth:`GeoQuerySet.envelope`"
msgstr ""

#: ../../ref/contrib/gis/db-api.txt:286
# a61654e35e48495482c412861e15604c
msgid ":meth:`GeoQuerySet.extent`"
msgstr ""

#: ../../ref/contrib/gis/db-api.txt:287
# cb9eaef35c094f2698abd401483b7c00
msgid ":meth:`GeoQuerySet.extent3d`"
msgstr ""

#: ../../ref/contrib/gis/db-api.txt:288
# 9230ecc057f549d6b22df2bad8ccd4c5
msgid ":meth:`GeoQuerySet.force_rhr`"
msgstr ""

#: ../../ref/contrib/gis/db-api.txt:289
# 3feb27275b7b494bbd5b0ae5c3f88e17
msgid ":meth:`GeoQuerySet.geohash`"
msgstr ""

#: ../../ref/contrib/gis/db-api.txt:290
# bbed546faffe4dfbbc946758cd134146
msgid ":meth:`GeoQuerySet.geojson`"
msgstr ""

#: ../../ref/contrib/gis/db-api.txt:291
# c4853e1c827a4be2bb274edb77deb51c
msgid ":meth:`GeoQuerySet.gml`"
msgstr ""

#: ../../ref/contrib/gis/db-api.txt:292
# 5b21359496064a51a1a540a3e83ac377
msgid ":meth:`GeoQuerySet.intersection`"
msgstr ""

#: ../../ref/contrib/gis/db-api.txt:293
# 36aac134e4d14edd97403f65dd864330
msgid ":meth:`GeoQuerySet.kml`"
msgstr ""

#: ../../ref/contrib/gis/db-api.txt:294
# baae8da62f3140fd858956baf7c2b015
msgid ":meth:`GeoQuerySet.length`"
msgstr ""

#: ../../ref/contrib/gis/db-api.txt:295
# d9dec468fab844ac9d199e27df2037fc
msgid ":meth:`GeoQuerySet.make_line`"
msgstr ""

#: ../../ref/contrib/gis/db-api.txt:296
# 64cfd66cae7347898b939fa04b71eee2
msgid ":meth:`GeoQuerySet.mem_size`"
msgstr ""

#: ../../ref/contrib/gis/db-api.txt:297
# a3d63918129b4151a1d7930d1165f2b8
msgid ":meth:`GeoQuerySet.num_geom`"
msgstr ""

#: ../../ref/contrib/gis/db-api.txt:298
# d63e16c9cd16482d80b97d5a179d755d
msgid ":meth:`GeoQuerySet.num_points`"
msgstr ""

#: ../../ref/contrib/gis/db-api.txt:299
# b14ac48ce3884203b25a630db84673dc
msgid ":meth:`GeoQuerySet.perimeter`"
msgstr ""

#: ../../ref/contrib/gis/db-api.txt:300
# d075816a034f4de8ba6cac2b45e36334
msgid ":meth:`GeoQuerySet.point_on_surface`"
msgstr ""

#: ../../ref/contrib/gis/db-api.txt:301
# 04f909fd909e4721800a87b71e67cf6a
msgid ":meth:`GeoQuerySet.reverse_geom`"
msgstr ""

#: ../../ref/contrib/gis/db-api.txt:302
# 1ece1b662ee4457d90bf7407c2d68d0a
msgid ":meth:`GeoQuerySet.scale`"
msgstr ""

#: ../../ref/contrib/gis/db-api.txt:303
# e1de830294724b0fb3a6b2a5d271a8dc
msgid ":meth:`GeoQuerySet.snap_to_grid`"
msgstr ""

#: ../../ref/contrib/gis/db-api.txt:304
# df1f355fc22f470195182e0e27ef9b75
msgid ":meth:`GeoQuerySet.svg`"
msgstr ""

#: ../../ref/contrib/gis/db-api.txt:305
# dda9a0d55c5445f592205b733cb31498
msgid ":meth:`GeoQuerySet.sym_difference`"
msgstr ""

#: ../../ref/contrib/gis/db-api.txt:306
# d8d0130608f240b0a7882ccf58ca7d9a
msgid ":meth:`GeoQuerySet.transform`"
msgstr ""

#: ../../ref/contrib/gis/db-api.txt:307
# 9afa5aea14c648c2a5c5ed8095f40781
msgid ":meth:`GeoQuerySet.translate`"
msgstr ""

#: ../../ref/contrib/gis/db-api.txt:308
# 9e2608f9eac045618629ae8e13236c04
msgid ":meth:`GeoQuerySet.union`"
msgstr ""

#: ../../ref/contrib/gis/db-api.txt:309
# 4deb19440fd04afbb9658da874ec617a
msgid ":meth:`GeoQuerySet.unionagg`"
msgstr ""

#: ../../ref/contrib/gis/db-api.txt:312
# 0ac1a248e9d640d4ab164fd21a28118b
msgid "Footnotes"
msgstr ""

#: ../../ref/contrib/gis/db-api.txt:313
# 99234d8c4bf94b70bacff1d2f1cd2668
msgid "*See* Open Geospatial Consortium, Inc., `OpenGIS Simple Feature Specification For SQL <http://www.opengis.org/docs/99-049.pdf>`_, Document 99-049 (May 5, 1999), at  Ch. 3.2.5, p. 3-11 (SQL Textual Representation of Geometry)."
msgstr ""

#: ../../ref/contrib/gis/db-api.txt:314
# b3e3e76c23d5448db9e7e3f81a21cb5b
msgid "*See* `PostGIS EWKB, EWKT and Canonical Forms <http://postgis.refractions.net/documentation/manual-1.5/ch04.html#EWKB_EWKT>`_, PostGIS documentation at Ch. 4.1.2."
msgstr ""

#: ../../ref/contrib/gis/db-api.txt:315
# 55b3e9584b7b46c2a7066ea5988df2b8
msgid "*See* Howard Butler, Martin Daly, Allan Doyle, Tim Schaub, & Christopher Schmidt, `The GeoJSON Format Specification <http://geojson.org/geojson-spec.html>`_, Revision 1.0 (June 16, 2008)."
msgstr ""

#: ../../ref/contrib/gis/db-api.txt:316
# 59f0fad204c14b13bf418e7b110c9f18
msgid "*See* `PostGIS 1.4 documentation <http://postgis.refractions.net/documentation/manual-1.4/ST_Distance_Sphere.html>`_ on ``ST_distance_sphere``."
msgstr ""

#: ../../ref/contrib/gis/db-api.txt:317
# fd076a6ac5ce4806a75d32a09a6c550e
msgid "*See* `PostGIS 1.5 documentation <http://postgis.refractions.net/documentation/manual-1.5/ST_Distance_Sphere.html>`_ on ``ST_distance_sphere``."
msgstr ""

#: ../../ref/contrib/gis/db-api.txt:318
# f59ca49b5e294020beac2982dbe0a316
msgid "*See* `Creating Spatial Indexes <http://dev.mysql.com/doc/refman/5.6/en/creating-spatial-indexes.html>`_ in the MySQL Reference Manual:"
msgstr ""

#: ../../ref/contrib/gis/db-api.txt:321
# 41f66fa0661342c18f05118bc0749978
msgid "For MyISAM tables, ``SPATIAL INDEX`` creates an R-tree index. For storage engines that support nonspatial indexing of spatial columns, the engine creates a B-tree index. A B-tree index on spatial values will be useful for exact-value lookups, but not for range scans."
msgstr ""

#: ../../ref/contrib/gis/db-api.txt:326
# 5a34819912374db19b053cceb73c1856
msgid "Refer :ref:`mysql-spatial-limitations` section for more details."
msgstr ""

