# SOME DESCRIPTIVE TITLE.
# Copyright (C) Django Software Foundation and contributors
# This file is distributed under the same license as the Django package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: Django 1.7\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2014-10-13 18:33+0300\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#: ../../ref/contrib/syndication.txt:3
# 0695bef8d3a3496585a114ea6f759313
msgid "The syndication feed framework"
msgstr ""

#: ../../ref/contrib/syndication.txt:9
# ed035441810d4e048af307f0fdc62a07
msgid "Django comes with a high-level syndication-feed-generating framework that makes creating RSS_ and Atom_ feeds easy."
msgstr ""

#: ../../ref/contrib/syndication.txt:12
# 5aef6b75ab6b4256b70f9d2b60c2ee48
msgid "To create any syndication feed, all you have to do is write a short Python class. You can create as many feeds as you want."
msgstr ""

#: ../../ref/contrib/syndication.txt:15
# b31b786472534a44b7b854ad56364936
msgid "Django also comes with a lower-level feed-generating API. Use this if you want to generate feeds outside of a Web context, or in some other lower-level way."
msgstr ""

#: ../../ref/contrib/syndication.txt:23
# ff396a0f2c4a4215928ea9b175353500
msgid "The high-level framework"
msgstr ""

#: ../../ref/contrib/syndication.txt:26
# 5a6f24fbf35f4d1caa8d605a0d460871
msgid "Overview"
msgstr ""

#: ../../ref/contrib/syndication.txt:28
# e3c7a3e504ac4e0d9a43832ef0239091
msgid "The high-level feed-generating framework is supplied by the :class:`~django.contrib.syndication.views.Feed` class. To create a feed, write a :class:`~django.contrib.syndication.views.Feed` class and point to an instance of it in your :doc:`URLconf </topics/http/urls>`."
msgstr ""

#: ../../ref/contrib/syndication.txt:35
# a7238f2913b24ef39531c9a6f1261d76
msgid "Feed classes"
msgstr ""

#: ../../ref/contrib/syndication.txt:37
# a489efdc794748628dc765cc36d95cb3
msgid "A :class:`~django.contrib.syndication.views.Feed` class is a Python class that represents a syndication feed. A feed can be simple (e.g., a \"site news\" feed, or a basic feed displaying the latest entries of a blog) or more complex (e.g., a feed displaying all the blog entries in a particular category, where the category is variable)."
msgstr ""

#: ../../ref/contrib/syndication.txt:43
# 7cc1996789c54a57b84bbf8f6d81f5fd
msgid "Feed classes subclass :class:`django.contrib.syndication.views.Feed`. They can live anywhere in your codebase."
msgstr ""

#: ../../ref/contrib/syndication.txt:46
# 039cc0c3562d4f4397aa96868185dca1
msgid "Instances of :class:`~django.contrib.syndication.views.Feed` classes are views which can be used in your :doc:`URLconf </topics/http/urls>`."
msgstr ""

#: ../../ref/contrib/syndication.txt:50
# 9278c4ff56f04edda19b9bd91fc992d9
msgid "A simple example"
msgstr ""

#: ../../ref/contrib/syndication.txt:52
# 01fdc8d6c4ba4097b295273ff3d5425a
msgid "This simple example, taken from a hypothetical police beat news site describes a feed of the latest five news items::"
msgstr ""

#: ../../ref/contrib/syndication.txt:77
# d12878d226e34381a4cbf28347843257
msgid "To connect a URL to this feed, put an instance of the Feed object in your :doc:`URLconf </topics/http/urls>`. For example::"
msgstr ""

#: ../../ref/contrib/syndication.txt:89
# 7f1c17cbedba4b9d96a256fff78e601b
msgid "Note:"
msgstr ""

#: ../../ref/contrib/syndication.txt:91
# 8a8a369b82b8472688fc276a16667169
msgid "The Feed class subclasses :class:`django.contrib.syndication.views.Feed`."
msgstr ""

#: ../../ref/contrib/syndication.txt:93
# f7190e83693b494a8bdccb491a6a9e7c
msgid "``title``, ``link`` and ``description`` correspond to the standard RSS ``<title>``, ``<link>`` and ``<description>`` elements, respectively."
msgstr ""

#: ../../ref/contrib/syndication.txt:97
# c4a0e427a5054d7f8dada035146c93e1
msgid "``items()`` is, simply, a method that returns a list of objects that should be included in the feed as ``<item>`` elements. Although this example returns ``NewsItem`` objects using Django's :doc:`object-relational mapper </ref/models/querysets>`, ``items()`` doesn't have to return model instances. Although you get a few bits of functionality \"for free\" by using Django models, ``items()`` can return any type of object you want."
msgstr ""

#: ../../ref/contrib/syndication.txt:105
# a2901e6a442547658cdad08f9156cf27
msgid "If you're creating an Atom feed, rather than an RSS feed, set the ``subtitle`` attribute instead of the ``description`` attribute. See `Publishing Atom and RSS feeds in tandem`_, later, for an example."
msgstr ""

#: ../../ref/contrib/syndication.txt:109
# dcfcef8b2cab452a9a7ab8cdea2688c9
msgid "One thing is left to do. In an RSS feed, each ``<item>`` has a ``<title>``, ``<link>`` and ``<description>``. We need to tell the framework what data to put into those elements."
msgstr ""

#: ../../ref/contrib/syndication.txt:113
# 3c4914259f784c0eae2f9162443d8077
msgid "For the contents of ``<title>`` and ``<description>``, Django tries calling the methods ``item_title()`` and ``item_description()`` on the :class:`~django.contrib.syndication.views.Feed` class. They are passed a single parameter, ``item``, which is the object itself. These are optional; by default, the unicode representation of the object is used for both."
msgstr ""

#: ../../ref/contrib/syndication.txt:120
# 01af47bf49bf487db1234a7d4d27e030
msgid "If you want to do any special formatting for either the title or description, :doc:`Django templates </topics/templates>` can be used instead. Their paths can be specified with the ``title_template`` and ``description_template`` attributes on the :class:`~django.contrib.syndication.views.Feed` class. The templates are rendered for each item and are passed two template context variables:"
msgstr ""

#: ../../ref/contrib/syndication.txt:127
# 364e401739d046cda80eb422ba73346f
msgid "``{{ obj }}`` -- The current object (one of whichever objects you returned in ``items()``)."
msgstr ""

#: ../../ref/contrib/syndication.txt:130
# 102c2c7973d54a7bbfb5f1c0790bc688
msgid "``{{ site }}`` -- A :class:`django.contrib.sites.models.Site` object representing the current site. This is useful for ``{{ site.domain }}`` or ``{{ site.name }}``. If you do *not* have the Django sites framework installed, this will be set to a :class:`~django.contrib.sites.requests.RequestSite` object. See the :ref:`RequestSite section of the sites framework documentation <requestsite-objects>` for more."
msgstr ""

#: ../../ref/contrib/syndication.txt:138
# 54e40c3d52b648f59fed40fee01a2a79
msgid "See `a complex example`_ below that uses a description template."
msgstr ""

#: ../../ref/contrib/syndication.txt:144
# e10a5a024c2841e5b87ba8569f8d3020
msgid "There is also a way to pass additional information to title and description templates, if you need to supply more than the two variables mentioned before. You can provide your implementation of ``get_context_data`` method in your ``Feed`` subclass. For example::"
msgstr ""

#: ../../ref/contrib/syndication.txt:164
# b27fc0315c0e4623bc1086d704486d04
msgid "And the template:"
msgstr ""

#: ../../ref/contrib/syndication.txt:170
# 9ba8761985bd461bad2541bb6079043c
msgid "This method will be called once per each item in the list returned by ``items()`` with the following keyword arguments:"
msgstr ""

#: ../../ref/contrib/syndication.txt:173
# 5904e07d22f144719f86a8b1ba899c3b
msgid "``item``: the current item. For backward compatibility reasons, the name of this context variable is ``{{ obj }}``."
msgstr ""

#: ../../ref/contrib/syndication.txt:176
# e3a62f7cf54a4b079bd976ab03d21c72
msgid "``obj``: the object returned by ``get_object()``. By default this is not exposed to the templates to avoid confusion with ``{{ obj }}`` (see above), but you can use it in your implementation of ``get_context_data()``."
msgstr ""

#: ../../ref/contrib/syndication.txt:180
# 9ab3a9f7f3074a3e8301300d034562c7
msgid "``site``: current site as described above."
msgstr ""

#: ../../ref/contrib/syndication.txt:182
# f59f941baad34acaa1576a850535c55d
msgid "``request``: current request."
msgstr ""

#: ../../ref/contrib/syndication.txt:184
# a62924cef0dd436fa88cdefd8096853f
msgid "The behavior of ``get_context_data()`` mimics that of :ref:`generic views <adding-extra-context>` - you're supposed to call ``super()`` to retrieve context data from parent class, add your data and return the modified dictionary."
msgstr ""

#: ../../ref/contrib/syndication.txt:189
# ad63d445d5f54cb9aa63d78606d58c4f
msgid "To specify the contents of ``<link>``, you have two options. For each item in ``items()``, Django first tries calling the ``item_link()`` method on the :class:`~django.contrib.syndication.views.Feed` class. In a similar way to the title and description, it is passed it a single parameter, ``item``. If that method doesn't exist, Django tries executing a ``get_absolute_url()`` method on that object. Both ``get_absolute_url()`` and ``item_link()`` should return the item's URL as a normal Python string. As with ``get_absolute_url()``, the result of ``item_link()`` will be included directly in the URL, so you are responsible for doing all necessary URL quoting and conversion to ASCII inside the method itself."
msgstr ""

#: ../../ref/contrib/syndication.txt:203
# d28ed6c8476840bb8899ee6509c7a8cd
msgid "A complex example"
msgstr ""

#: ../../ref/contrib/syndication.txt:205
# 746bf2c58e6e4ca5a11820a14b4b6d95
msgid "The framework also supports more complex feeds, via arguments."
msgstr ""

#: ../../ref/contrib/syndication.txt:207
# 77b52e0b1c0943fca93fb6cb6b3c8729
msgid "For example, a website could offer an RSS feed of recent crimes for every police beat in a city. It'd be silly to create a separate :class:`~django.contrib.syndication.views.Feed` class for each police beat; that would violate the :ref:`DRY principle <dry>` and would couple data to programming logic. Instead, the syndication framework lets you access the arguments passed from your :doc:`URLconf </topics/http/urls>` so feeds can output items based on information in the feed's URL."
msgstr ""

#: ../../ref/contrib/syndication.txt:215
# cb39ea2be80b4a76be2f7ede6156f858
msgid "The police beat feeds could be accessible via URLs like this:"
msgstr ""

#: ../../ref/contrib/syndication.txt:217
# f91a7febba8247f1823087f969d9f803
msgid ":file:`/beats/613/rss/` -- Returns recent crimes for beat 613."
msgstr ""

#: ../../ref/contrib/syndication.txt:218
# 7edda36fb54a4d3cbb292aa9a4d05544
msgid ":file:`/beats/1424/rss/` -- Returns recent crimes for beat 1424."
msgstr ""

#: ../../ref/contrib/syndication.txt:220
# 7ac89176566c46af9ef1973caa764df3
msgid "These can be matched with a :doc:`URLconf </topics/http/urls>` line such as::"
msgstr ""

#: ../../ref/contrib/syndication.txt:224
# c93bd29700d9427dbe0d8ba0394e5f71
msgid "Like a view, the arguments in the URL are passed to the ``get_object()`` method along with the request object."
msgstr ""

#: ../../ref/contrib/syndication.txt:227
# 2b162b20368f4cbd8806520d2b8fb127
msgid "Here's the code for these beat-specific feeds::"
msgstr ""

#: ../../ref/contrib/syndication.txt:250
# 8d57ff46a7de4712a38ab7545386e510
msgid "To generate the feed's ``<title>``, ``<link>`` and ``<description>``, Django uses the ``title()``, ``link()`` and ``description()`` methods. In the previous example, they were simple string class attributes, but this example illustrates that they can be either strings *or* methods. For each of ``title``, ``link`` and ``description``, Django follows this algorithm:"
msgstr ""

#: ../../ref/contrib/syndication.txt:257
# 762682c909c845ec9758af36b189f411
msgid "First, it tries to call a method, passing the ``obj`` argument, where ``obj`` is the object returned by ``get_object()``."
msgstr ""

#: ../../ref/contrib/syndication.txt:260
# b60d4bd975d9487399fee29ddb8ac90a
msgid "Failing that, it tries to call a method with no arguments."
msgstr ""

#: ../../ref/contrib/syndication.txt:262
# 1bcbf50afc5845ceaf4ee8dbb25c6476
msgid "Failing that, it uses the class attribute."
msgstr ""

#: ../../ref/contrib/syndication.txt:264
# 62339c11f3c945b6bfb159e60f3cdda1
msgid "Also note that ``items()`` also follows the same algorithm -- first, it tries ``items(obj)``, then ``items()``, then finally an ``items`` class attribute (which should be a list)."
msgstr ""

#: ../../ref/contrib/syndication.txt:268
# 93b84199a08640f3821d41cc9622a68d
msgid "We are using a template for the item descriptions. It can be very simple:"
msgstr ""

#: ../../ref/contrib/syndication.txt:274
# 9d39854f724746aca250c12b7bc4a2a8
msgid "However, you are free to add formatting as desired."
msgstr ""

#: ../../ref/contrib/syndication.txt:276
# 1ef47b77b3d94310917c29e15121b3ad
msgid "The ``ExampleFeed`` class below gives full documentation on methods and attributes of :class:`~django.contrib.syndication.views.Feed` classes."
msgstr ""

#: ../../ref/contrib/syndication.txt:280
# dc4354e3924a4f758ba0aca862737599
msgid "Specifying the type of feed"
msgstr ""

#: ../../ref/contrib/syndication.txt:282
# 971791e825d64b6c949e0f1898134cd3
msgid "By default, feeds produced in this framework use RSS 2.0."
msgstr ""

#: ../../ref/contrib/syndication.txt:284
# 4d7ebc4e27544ee0a503dd50200b2f2f
msgid "To change that, add a ``feed_type`` attribute to your :class:`~django.contrib.syndication.views.Feed` class, like so::"
msgstr ""

#: ../../ref/contrib/syndication.txt:292
# 9b47e974bb524dd99ca3c35373009c86
msgid "Note that you set ``feed_type`` to a class object, not an instance."
msgstr ""

#: ../../ref/contrib/syndication.txt:294
# 7cb2eaadd36e4bcf85989be6ba0462e6
msgid "Currently available feed types are:"
msgstr ""

#: ../../ref/contrib/syndication.txt:296
# 1197d15eca074167865e156f7b100e79
msgid ":class:`django.utils.feedgenerator.Rss201rev2Feed` (RSS 2.01. Default.)"
msgstr ""

#: ../../ref/contrib/syndication.txt:297
# c60a05216d994cf2960a76599417a0ec
msgid ":class:`django.utils.feedgenerator.RssUserland091Feed` (RSS 0.91.)"
msgstr ""

#: ../../ref/contrib/syndication.txt:298
# b5b1be8914f34e3c9d1e48ba8d624ad7
msgid ":class:`django.utils.feedgenerator.Atom1Feed` (Atom 1.0.)"
msgstr ""

#: ../../ref/contrib/syndication.txt:301
# cc296aabc6cd434584f24e972b86b8f4
msgid "Enclosures"
msgstr ""

#: ../../ref/contrib/syndication.txt:303
# 6577c87290204cdab05ed36d298772a8
msgid "To specify enclosures, such as those used in creating podcast feeds, use the ``item_enclosure_url``, ``item_enclosure_length`` and ``item_enclosure_mime_type`` hooks. See the ``ExampleFeed`` class below for usage examples."
msgstr ""

#: ../../ref/contrib/syndication.txt:309
# 4ee7d241dff84affbf745cfde4654688
msgid "Language"
msgstr ""

#: ../../ref/contrib/syndication.txt:311
# 6f2dd65ecdd84bc8be14d02cbf31665c
msgid "Feeds created by the syndication framework automatically include the appropriate ``<language>`` tag (RSS 2.0) or ``xml:lang`` attribute (Atom). This comes directly from your :setting:`LANGUAGE_CODE` setting."
msgstr ""

#: ../../ref/contrib/syndication.txt:316
# 1117dcebded0486686820f24aae6c9fb
msgid "URLs"
msgstr ""

#: ../../ref/contrib/syndication.txt:318
# 8d4477b4c8924b2f879f370a280c28f3
msgid "The ``link`` method/attribute can return either an absolute path (e.g. :file:`\"/blog/\"`) or a URL with the fully-qualified domain and protocol (e.g. ``\"http://www.example.com/blog/\"``). If ``link`` doesn't return the domain, the syndication framework will insert the domain of the current site, according to your :setting:`SITE_ID setting <SITE_ID>`."
msgstr ""

#: ../../ref/contrib/syndication.txt:324
# f743f5fa566d4517a88841876eaa9030
msgid "Atom feeds require a ``<link rel=\"self\">`` that defines the feed's current location. The syndication framework populates this automatically, using the domain of the current site according to the :setting:`SITE_ID` setting."
msgstr ""

#: ../../ref/contrib/syndication.txt:329
# 211626ed591945d4a81ace7a8262f335
msgid "Publishing Atom and RSS feeds in tandem"
msgstr ""

#: ../../ref/contrib/syndication.txt:331
# 113cb46ac96240f38547e45088470b82
msgid "Some developers like to make available both Atom *and* RSS versions of their feeds. That's easy to do with Django: Just create a subclass of your :class:`~django.contrib.syndication.views.Feed` class and set the ``feed_type`` to something different. Then update your URLconf to add the extra versions."
msgstr ""

#: ../../ref/contrib/syndication.txt:337
# 5195b5a8ea1c43519da11b78065258a2
msgid "Here's a full example::"
msgstr ""

#: ../../ref/contrib/syndication.txt:356
# da02764cef3d440e84f232f8d634d8f8
msgid "In this example, the RSS feed uses a ``description`` while the Atom feed uses a ``subtitle``. That's because Atom feeds don't provide for a feed-level \"description,\" but they *do* provide for a \"subtitle.\""
msgstr ""

#: ../../ref/contrib/syndication.txt:360
# f907887693174aa4a311bff82cccd400
msgid "If you provide a ``description`` in your :class:`~django.contrib.syndication.views.Feed` class, Django will *not* automatically put that into the ``subtitle`` element, because a subtitle and description are not necessarily the same thing. Instead, you should define a ``subtitle`` attribute."
msgstr ""

#: ../../ref/contrib/syndication.txt:366
# 9509a9cca73449859967115c73b92983
msgid "In the above example, we simply set the Atom feed's ``subtitle`` to the RSS feed's ``description``, because it's quite short already."
msgstr ""

#: ../../ref/contrib/syndication.txt:369
# 2a18bd62a5e540e187ae1dea32d5a1b9
msgid "And the accompanying URLconf::"
msgstr ""

#: ../../ref/contrib/syndication.txt:382
# 8e7cbdb9009d4675b8c6b15c496da93c
msgid "Feed class reference"
msgstr ""

#: ../../ref/contrib/syndication.txt:386
# f26a1f79ed1142a0b4565163f5de420b
msgid "This example illustrates all possible attributes and methods for a :class:`~django.contrib.syndication.views.Feed` class::"
msgstr ""

#: ../../ref/contrib/syndication.txt:871
# 944e6e43186c47b88c32e44031f64d6b
msgid "The low-level framework"
msgstr ""

#: ../../ref/contrib/syndication.txt:873
# e4d220a465d44bf18c6e06bda8844c8b
msgid "Behind the scenes, the high-level RSS framework uses a lower-level framework for generating feeds' XML. This framework lives in a single module: `django/utils/feedgenerator.py`_."
msgstr ""

#: ../../ref/contrib/syndication.txt:877
# 942c370f0faa4d17b0a71190235d61a0
msgid "You use this framework on your own, for lower-level feed generation. You can also create custom feed generator subclasses for use with the ``feed_type`` ``Feed`` option."
msgstr ""

#: ../../ref/contrib/syndication.txt:884
# ad7fc487a43e478c87ebe20bce0dc275
msgid "``SyndicationFeed`` classes"
msgstr ""

#: ../../ref/contrib/syndication.txt:886
# b489a510cb244727bc607bd2d32f39ad
msgid "The :mod:`~django.utils.feedgenerator` module contains a base class:"
msgstr ""

#: ../../ref/contrib/syndication.txt:888
# 6bab6279b3a549bbb7dcbec1a6d3c295
msgid ":class:`django.utils.feedgenerator.SyndicationFeed`"
msgstr ""

#: ../../ref/contrib/syndication.txt:890
# 6c924a43a2dc4acfacecc428dcb309fd
msgid "and several subclasses:"
msgstr ""

#: ../../ref/contrib/syndication.txt:892
# a68ca86f2504465bb7c2e600a0fc4743
msgid ":class:`django.utils.feedgenerator.RssUserland091Feed`"
msgstr ""

#: ../../ref/contrib/syndication.txt:893
# cff20ef0cfe2447c982e750e8a17310c
msgid ":class:`django.utils.feedgenerator.Rss201rev2Feed`"
msgstr ""

#: ../../ref/contrib/syndication.txt:894
# 1a0c914891004a4c9d2beb919ffe21bb
msgid ":class:`django.utils.feedgenerator.Atom1Feed`"
msgstr ""

#: ../../ref/contrib/syndication.txt:896
# 0cf6182933c64cf99980fa02fc59ead2
msgid "Each of these three classes knows how to render a certain type of feed as XML. They share this interface:"
msgstr ""

#: ../../ref/contrib/syndication.txt:924
# ebcd5786bcec4a80bbfc5f4ccfb61a25
msgid ":meth:`.SyndicationFeed.__init__`"
msgstr ""

#: ../../ref/contrib/syndication.txt:900
# eb0e00adcbe4487f80b48cabd741a7aa
msgid "Initialize the feed with the given dictionary of metadata, which applies to the entire feed. Required keyword arguments are:"
msgstr ""

#: ../../ref/contrib/syndication.txt:903
#: ../../ref/contrib/syndication.txt:931
# beb57936fb834e22922869c043e69836
# 94b04192c2e24890918ae713072032e2
msgid "``title``"
msgstr ""

#: ../../ref/contrib/syndication.txt:904
#: ../../ref/contrib/syndication.txt:932
# 07e5976b6f85493f81b798964af66914
# 491688f72d044d96a20b75488ea7a187
msgid "``link``"
msgstr ""

#: ../../ref/contrib/syndication.txt:905
#: ../../ref/contrib/syndication.txt:933
# e95519b55ca5419a92c55c95ae4d898a
# 8c425db175644fa28667c4adf1c1940d
msgid "``description``"
msgstr ""

#: ../../ref/contrib/syndication.txt:907
# 415fc3844b414661bc69401f9570693e
msgid "There's also a bunch of other optional keywords:"
msgstr ""

#: ../../ref/contrib/syndication.txt:909
# e1af9ce89b084b08ac52fcf770c735e0
msgid "``language``"
msgstr ""

#: ../../ref/contrib/syndication.txt:910
#: ../../ref/contrib/syndication.txt:937
# 8cf37f4fc16842c7a18f1e524efd9c03
# 7fc87f5ff22046e0a3747370d3bcd037
msgid "``author_email``"
msgstr ""

#: ../../ref/contrib/syndication.txt:911
#: ../../ref/contrib/syndication.txt:938
# feb80b5964c54123a22a8a72a5ae66aa
# dd279dfc08014ae8b85116eb6edc8973
msgid "``author_name``"
msgstr ""

#: ../../ref/contrib/syndication.txt:912
#: ../../ref/contrib/syndication.txt:939
# 1c74ad7e6fac485e93f4f86e682744fc
# 8c5d9b7288ac4570ba0988bd5b017b39
msgid "``author_link``"
msgstr ""

#: ../../ref/contrib/syndication.txt:913
# b7ef0d4bc3744b4f9e8959adae276e43
msgid "``subtitle``"
msgstr ""

#: ../../ref/contrib/syndication.txt:914
#: ../../ref/contrib/syndication.txt:944
# 6bc097366dfb4161be6201731ae4da99
# c85e096111a445bca59b5cfcc59a8bbc
msgid "``categories``"
msgstr ""

#: ../../ref/contrib/syndication.txt:915
# 3e31a0cb1bb04264a511edf9a51b3ef4
msgid "``feed_url``"
msgstr ""

#: ../../ref/contrib/syndication.txt:916
# 43f3ca6798804775831fee0c9ee1dc50
msgid "``feed_copyright``"
msgstr ""

#: ../../ref/contrib/syndication.txt:917
# f8944dcfcc5b40e6990305630344b190
msgid "``feed_guid``"
msgstr ""

#: ../../ref/contrib/syndication.txt:918
#: ../../ref/contrib/syndication.txt:946
# 69019edbb0534a07a904d65f2871621d
# 63c067718c4f429a9345c2757efa67c7
msgid "``ttl``"
msgstr ""

#: ../../ref/contrib/syndication.txt:920
# 05d8581bbe984769974618bb1a4a14e9
msgid "Any extra keyword arguments you pass to ``__init__`` will be stored in ``self.feed`` for use with `custom feed generators`_."
msgstr ""

#: ../../ref/contrib/syndication.txt:923
# b396d46edbb84d6292663904548cba7e
msgid "All parameters should be Unicode objects, except ``categories``, which should be a sequence of Unicode objects."
msgstr ""

#: ../../ref/contrib/syndication.txt:961
# 93a1d08939714b438480262bf787bbf5
msgid ":meth:`.SyndicationFeed.add_item`"
msgstr ""

#: ../../ref/contrib/syndication.txt:927
# 76b3b904103c4adebd713fd7aaa719f6
msgid "Add an item to the feed with the given parameters."
msgstr ""

#: ../../ref/contrib/syndication.txt:929
# 3171754b943d400e92768023e689ad22
msgid "Required keyword arguments are:"
msgstr ""

#: ../../ref/contrib/syndication.txt:935
# 789be451985c4769bd4fc68f9b0f94fa
msgid "Optional keyword arguments are:"
msgstr ""

#: ../../ref/contrib/syndication.txt:940
# 10d87b96977b425ba1eb93bbd88fdbb1
msgid "``pubdate``"
msgstr ""

#: ../../ref/contrib/syndication.txt:941
# 3cea3e4ec3d54e03ba215c898151cb47
msgid "``comments``"
msgstr ""

#: ../../ref/contrib/syndication.txt:942
# 85dce00adeb64de898015f64fa787cd0
msgid "``unique_id``"
msgstr ""

#: ../../ref/contrib/syndication.txt:943
# 4d58d162d4fb458ca19b3b636e2a8067
msgid "``enclosure``"
msgstr ""

#: ../../ref/contrib/syndication.txt:945
# 40453191305c488aa2ee057bdeb57f2f
msgid "``item_copyright``"
msgstr ""

#: ../../ref/contrib/syndication.txt:947
# e92f5cecb88a4748880520d844218ea7
msgid "``updateddate``"
msgstr ""

#: ../../ref/contrib/syndication.txt:949
# db941d5e88ed4487b7173d7d428e3f17
msgid "Extra keyword arguments will be stored for `custom feed generators`_."
msgstr ""

#: ../../ref/contrib/syndication.txt:951
# 88e7522b6fe8472a9d62d93c052527c9
msgid "All parameters, if given, should be Unicode objects, except:"
msgstr ""

#: ../../ref/contrib/syndication.txt:953
# ad90bbe2c0dd4fe09ba9a098162354e8
msgid "``pubdate`` should be a Python  :class:`~datetime.datetime` object."
msgstr ""

#: ../../ref/contrib/syndication.txt:954
# 1df14b7d13e6425b8f7be138a6c00cb5
msgid "``updateddate`` should be a Python  :class:`~datetime.datetime` object."
msgstr ""

#: ../../ref/contrib/syndication.txt:955
# 1f7f43dca547446ca0f76b0a481c4570
msgid "``enclosure`` should be an instance of :class:`django.utils.feedgenerator.Enclosure`."
msgstr ""

#: ../../ref/contrib/syndication.txt:957
# 0bf16afb093b4138920115817bb3b2f5
msgid "``categories`` should be a sequence of Unicode objects."
msgstr ""

#: ../../ref/contrib/syndication.txt:961
# 00fa1df1ca6a4d5b889c00f14220b584
msgid "The optional ``updateddate`` argument was added."
msgstr ""

#: ../../ref/contrib/syndication.txt:964
# 6f728c94d209422fbdb882caf3f74bbe
msgid ":meth:`.SyndicationFeed.write`"
msgstr ""

#: ../../ref/contrib/syndication.txt:964
# 08ceb6889ae94d88988e8f84cf265153
msgid "Outputs the feed in the given encoding to outfile, which is a file-like object."
msgstr ""

#: ../../ref/contrib/syndication.txt:967
# 584979839c044b818a57c3f9e14b6dc1
msgid ":meth:`.SyndicationFeed.writeString`"
msgstr ""

#: ../../ref/contrib/syndication.txt:967
# 1fd6da6ee86b4912a0bc0a3c13fb9349
msgid "Returns the feed as a string in the given encoding."
msgstr ""

#: ../../ref/contrib/syndication.txt:969
# 2cb563292f8f4b5b9cabdaac4394d3c1
msgid "For example, to create an Atom 1.0 feed and print it to standard output::"
msgstr ""

#: ../../ref/contrib/syndication.txt:995
# 5e913301000440ec9c9e3c9840ea7d2a
msgid "Custom feed generators"
msgstr ""

#: ../../ref/contrib/syndication.txt:997
# bc9c06994ff84deeba2d79297d27a4bd
msgid "If you need to produce a custom feed format, you've got a couple of options."
msgstr ""

#: ../../ref/contrib/syndication.txt:999
# b5b243f2c1954266a0e2e8471dee5933
msgid "If the feed format is totally custom, you'll want to subclass ``SyndicationFeed`` and completely replace the ``write()`` and ``writeString()`` methods."
msgstr ""

#: ../../ref/contrib/syndication.txt:1003
# c4e47e8d4cc44c0b8f576ed8de60dc93
msgid "However, if the feed format is a spin-off of RSS or Atom (i.e. GeoRSS_, Apple's `iTunes podcast format`_, etc.), you've got a better choice. These types of feeds typically add extra elements and/or attributes to the underlying format, and there are a set of methods that ``SyndicationFeed`` calls to get these extra attributes. Thus, you can subclass the appropriate feed generator class (``Atom1Feed`` or ``Rss201rev2Feed``) and extend these callbacks. They are:"
msgstr ""

#: ../../ref/contrib/syndication.txt:1015
# e8640f148886426d83349f5646cc089c
msgid "``SyndicationFeed.root_attributes(self, )``"
msgstr ""

#: ../../ref/contrib/syndication.txt:1014
# ccb186d627be428e8099950e43271515
msgid "Return a ``dict`` of attributes to add to the root feed element (``feed``/``channel``)."
msgstr ""

#: ../../ref/contrib/syndication.txt:1021
# 166d350e3bda4504a9ae6c7f388d3198
msgid "``SyndicationFeed.add_root_elements(self, handler)``"
msgstr ""

#: ../../ref/contrib/syndication.txt:1018
# a0c3e0cb76c841b2ab14d49609c1d70e
msgid "Callback to add elements inside the root feed element (``feed``/``channel``). ``handler`` is an :class:`~xml.sax.saxutils.XMLGenerator` from Python's built-in SAX library; you'll call methods on it to add to the XML document in process."
msgstr ""

#: ../../ref/contrib/syndication.txt:1026
# c3393fbfa6f14766b7742a5896ae798f
msgid "``SyndicationFeed.item_attributes(self, item)``"
msgstr ""

#: ../../ref/contrib/syndication.txt:1024
# 3a83e02dd28c4f8a86490f82fd5a96d0
msgid "Return a ``dict`` of attributes to add to each item (``item``/``entry``) element. The argument, ``item``, is a dictionary of all the data passed to ``SyndicationFeed.add_item()``."
msgstr ""

#: ../../ref/contrib/syndication.txt:1030
# cf3452114d994db7996c64b97e393cec
msgid "``SyndicationFeed.add_item_elements(self, handler, item)``"
msgstr ""

#: ../../ref/contrib/syndication.txt:1029
# 73caca80cbb64f1888f547b86a9d175e
msgid "Callback to add elements to each item (``item``/``entry``) element. ``handler`` and ``item`` are as above."
msgstr ""

#: ../../ref/contrib/syndication.txt:1034
# ff8792902fc543538db8ce33f402df05
msgid "If you override any of these methods, be sure to call the superclass methods since they add the required elements for each feed format."
msgstr ""

#: ../../ref/contrib/syndication.txt:1037
# b3b65f3e5b024a03b27803d6ec6de873
msgid "For example, you might start implementing an iTunes RSS feed generator like so::"
msgstr ""

#: ../../ref/contrib/syndication.txt:1049
# e83873af08154069b5fbb112bedc983e
msgid "Obviously there's a lot more work to be done for a complete custom feed class, but the above example should demonstrate the basic idea."
msgstr ""

