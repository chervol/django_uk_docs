#
#
msgid ""
msgstr ""
"Project-Id-Version: Django 1.7\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2014-10-13 18:33+0300\n"
"Last-Translator: Volodymyr Cherepanyak <chervol@gmail.com>\n"
"Language-Team: Quintagroup <info@quintagroup.com>\n"
"Content-Transfer-Encoding: 8bit\n"
"Content-Type: text/plain; charset=UTF-8\n"
"MIME-Version: 1.0\n"
"PO-Revision-Date: 2015-03-05 13:06+0200\n"
"Language: uk\n"
"Plural-Forms: nplurals=3; plural=(n%10==1 && n%100!=11 ? 0 : n%10>=2 && n%10<"
"=4 && (n%100<10 || n%100>=20) ? 1 : 2);\n"

# ae8a3bada53e46ca82e6e3f2cf43b50b
#: ../../ref/contrib/csrf.txt:3
msgid "Cross Site Request Forgery protection"
msgstr "Підробка міжсайтового запиту (CSRF)"

# 8a8db88a22ad4b2f9ba0770e25245308
#: ../../ref/contrib/csrf.txt:8
msgid ""
"The CSRF middleware and template tag provides easy-to-use protection against"
" `Cross Site Request Forgeries`_.  This type of attack occurs when a "
"malicious Web site contains a link, a form button or some javascript that is"
" intended to perform some action on your Web site, using the credentials of "
"a logged-in user who visits the malicious site in their browser.  A related "
"type of attack, 'login CSRF', where an attacking site tricks a user's "
"browser into logging into a site with someone else's credentials, is also "
"covered."
msgstr ""
"Проміжний шар CSRF і шаблонний тег надають легкий у використанні захист "
"проти підробки міжсайтового запиту (`Cross Site Request Forgeries`_). Цей тип "
"атак трапляється, коли зловмисний вебсайт містить посилання, кнопку форми або "
"деякий javascript, який призначений для виконання деяких дій на вашому "
"вебсайті, "
"використовуючи облікові дані авторизованого користувача, який відвідував "
"зловмисний сайт у своєму браузері."

# 34c91e52dd204a6c85cfe96df1572ec2
#: ../../ref/contrib/csrf.txt:16
msgid ""
"The first defense against CSRF attacks is to ensure that GET requests (and "
"other 'safe' methods, as defined by 9.1.1 Safe Methods, HTTP 1.1, "
":rfc:`2616#section-9.1.1`) are side-effect free. Requests via 'unsafe' "
"methods, such as POST, PUT and DELETE, can then be protected by following "
"the steps below."
msgstr ""
"Перший захист проти CSRF атак - це гарантування того, що GET запити (та інші"
" 'безпечні ' методи, визначені в 9.1.1 Safe Methods, HTTP 1.1, "
":rfc:`2616#section-9.1.1`) "
"вільні від побічних ефектів. Запити через 'небезпечні' методи, такі як POST, "
"PUT і DELETE "
"можуть бути захищені за допомогою кроків, описаних нижче."

# 6a86700d65244d48ab909a51e3b004d5
#: ../../ref/contrib/csrf.txt:27
msgid "How to use it"
msgstr "Як це використовувати"

# 418c46988659422292a8dec666e63d10
#: ../../ref/contrib/csrf.txt:29
msgid "To enable CSRF protection for your views, follow these steps:"
msgstr ""
"Для того, щоб включити CSRF захист для ваших представлень, виконайте наступні "
"кроки:"

# ac950880591d4308bf1b5bee247e73f2
#: ../../ref/contrib/csrf.txt:31
msgid ""
"Add the middleware ``'django.middleware.csrf.CsrfViewMiddleware'`` to your "
"list of middleware classes, :setting:`MIDDLEWARE_CLASSES`.  (It should come "
"before any view middleware that assume that CSRF attacks have been dealt "
"with.)"
msgstr ""
"Додайте функціональний шар ``'django.middleware.csrf.CsrfViewMiddleware'`` в "
"ваш"
" список класів функціональних шарів :setting:`MIDDLEWARE_CLASSES`. (Він "
"повинен "
"бути доданим перед будь-яким з функціональних шарів представлень, що дасть "
"можливість запобігти CSRF атаці)."

# c27abc7f759a40e3822e785553b3cbb1
#: ../../ref/contrib/csrf.txt:37
msgid ""
"Alternatively, you can use the decorator "
":func:`~django.views.decorators.csrf.csrf_protect` on particular views you "
"want to protect (see below)."
msgstr ""
"Як варіант, ви можете використовувати декоратор "
":func:`~django.views.decorators.csrf.csrf_protect` в частині представлень, "
"які ви"
" хочете захистити (дивиться нижче)."

# 7db32c71c2204cef83e7ae34e8d4f2a6
#: ../../ref/contrib/csrf.txt:41
msgid ""
"In any template that uses a POST form, use the :ttag:`csrf_token` tag inside"
" the ``<form>`` element if the form is for an internal URL, e.g.::"
msgstr ""
"У будь-якому шаблоні, який використовує POST форму, використовуйте "
"тег :ttag:`csrf_token` всередині елемента ``<form>``, якщо форма для "
"внутрішнього URL, тобто::"

# b3c3639443a143de90c19a005016b3ac
#: ../../ref/contrib/csrf.txt:46
msgid ""
"This should not be done for POST forms that target external URLs, since that"
" would cause the CSRF token to be leaked, leading to a vulnerability."
msgstr ""
"Це не повинно робитися для POST форм, які посилаються на зовнішні URL-адреси, "
"оскільки це може викликати витік CSRF токена, що призводить до вразливості."

# f7ef47ecb64d4e9b97f06631ff7d3b06
#: ../../ref/contrib/csrf.txt:49
msgid ""
"In the corresponding view functions, ensure that the "
"``'django.core.context_processors.csrf'`` context processor is being used. "
"Usually, this can be done in one of two ways:"
msgstr ""
"У відповідних функціях представлення, переконайтеся, що "
"``'django.core.context_processors.csrf'`` контекстний процесор "
"використовується. Зазвичай, це може бути зроблено одним із двох способів:"

# 993288e3b83a4571996932138128ca0a
#: ../../ref/contrib/csrf.txt:53
msgid ""
"Use RequestContext, which always uses "
"``'django.core.context_processors.csrf'`` (no matter what your "
"TEMPLATE_CONTEXT_PROCESSORS setting).  If you are using generic views or "
"contrib apps, you are covered already, since these apps use RequestContext "
"throughout."
msgstr ""
"Використовуйте RequestContext, який завжди використовує "
"``'django.core.context_processors.csrf'`` (незалежно від параметра "
"TEMPLATE_CONTEXT_PROCESSORS). Якщо ви використовуєте загальні представлення "
"або "
"contrib додатки, ви вже застраховані, оскільки ці додатки використовують "
"RequestContext всюди."

# 350e4f5c0b8d477399380deeefb4541e
#: ../../ref/contrib/csrf.txt:59
msgid ""
"Manually import and use the processor to generate the CSRF token and add it "
"to the template context. e.g.::"
msgstr ""
"Вручну імпортуйте і використовуйте процесор для генерації CSRF токена і "
"додайте в шаблон контексту. Наприклад,::"

# 26b09e24d72045fbbab6db2fbd90652b
#: ../../ref/contrib/csrf.txt:71
msgid ""
"You may want to write your own "
":func:`~django.shortcuts.render_to_response()` wrapper that takes care of "
"this step for you."
msgstr ""
"Ви можете захотіти написати власну "
":func:`~django.shortcuts.render_to_response()` обгортку, яка подбає про "
"цей крок для вас."

# 0f0a4bcd4fbf4534b8b935979b0eeee9
#: ../../ref/contrib/csrf.txt:75
msgid ""
"The utility script ``extras/csrf_migration_helper.py`` (located in the "
"Django distribution, but not installed) can help to automate the finding of "
"code and templates that may need these steps. It contains full help on how "
"to use it."
msgstr ""
"Допоміжний скрипт ``extras/csrf_migration_helper.py`` (розташований в "
"дистрибутиві Django, але не встановлений) може допомогти автоматизувати "
"пошук коду і шаблонів, які можуть вимагати виконання цих кроків із "
"забезпечення безпеки. Скрипт містить повний опис свого використання."

# c45143e6801a4f42bef92537664b44b6
#: ../../ref/contrib/csrf.txt:82
msgid "AJAX"
msgstr "AJAX"

# 3925a0bf36724d3ebab6a85e8d00eea5
#: ../../ref/contrib/csrf.txt:84
msgid ""
"While the above method can be used for AJAX POST requests, it has some "
"inconveniences: you have to remember to pass the CSRF token in as POST data "
"with every POST request. For this reason, there is an alternative method: on"
" each XMLHttpRequest, set a custom ``X-CSRFToken`` header to the value of "
"the CSRF token. This is often easier, because many javascript frameworks "
"provide hooks that allow headers to be set on every request."
msgstr ""
"Хоча вищеописаний метод може бути використаний для AJAX POST запитів, він "
"має деякі незручності: ви повинні не забувати передавати CSRF токен як POST "
"дані з кожним POST запитом. З цієї причини є альтернативний метод: для "
"кожного XMLHttpRequest можна встановлювати користувацький заголовок "
"``X-CSRFToken`` в значення CSRF токена."

# 318b61503bd147b396fef2bdb7ece74d
#: ../../ref/contrib/csrf.txt:91
msgid ""
"As a first step, you must get the CSRF token itself. The recommended source "
"for the token is the ``csrftoken`` cookie, which will be set if you've "
"enabled CSRF protection for your views as outlined above."
msgstr ""
"Як перший крок, ви повинні отримати CSRF токен самостійно. "
"Рекомендоване джерело для токена - це файл cookie ``csrftoken``, який буде "
"встановлений, якщо ви включили CSRF захист для ваших представлень як описано "
"вище."

# f75e496a8a524e49b26c1bd19b665911
#: ../../ref/contrib/csrf.txt:97
msgid ""
"The CSRF token cookie is named ``csrftoken`` by default, but you can control"
" the cookie name via the :setting:`CSRF_COOKIE_NAME` setting."
msgstr ""
"Файл cookie CSRF токена називається ``csrftoken`` за замовчуванням, але ви "
"можете "
"керувати ім'ям цього файла за допомогою параметра :setting:`CSRF_COOKIE_NAME`."

# c131baffa0084a1495e3e841ca8a5d42
#: ../../ref/contrib/csrf.txt:100
msgid "Acquiring the token is straightforward:"
msgstr "Отримання токена є простим:"

# a8d8fe968ea44a4f80fe76e66396f91c
#: ../../ref/contrib/csrf.txt:122
msgid ""
"The above code could be simplified by using the `jQuery cookie plugin "
"<http://plugins.jquery.com/cookie/>`_ to replace ``getCookie``:"
msgstr ""
"Вищенаведений код може бути спрощений за допомогою `плагіна jQuery "
"<http://plugins.jquery.com/cookie/>`_ для заміни ``getCookie``:"

# 0f119567e6a64daa93560c57ca97efa2
#: ../../ref/contrib/csrf.txt:131
msgid ""
"The CSRF token is also present in the DOM, but only if explicitly included "
"using :ttag:`csrf_token` in a template. The cookie contains the canonical "
"token; the ``CsrfViewMiddleware`` will prefer the cookie to the token in the"
" DOM. Regardless, you're guaranteed to have the cookie if the token is "
"present in the DOM, so you should use the cookie!"
msgstr ""
"CSRF токен також представлений в DOM, але тільки якщо він явно включений, "
"використовуючи :ttag:`csrf_token` в шаблоні. Файл cookie містить канонічний "
"токен; "
"``CsrfViewMiddleware`` буде віддавати перевагу цьому файлу, а не токену в "
"DOM. Не "
"дивлячись на це, ви гарантовано маєте файл cookie, якщо токен представлений в "
"DOM, "
"так що ви повинні використовувати файл cookie."

# bd1fb63bfd7d4608a702d43818302c52
#: ../../ref/contrib/csrf.txt:139
msgid ""
"If your view is not rendering a template containing the :ttag:`csrf_token` "
"template tag, Django might not set the CSRF token cookie. This is common in "
"cases where forms are dynamically added to the page. To address this case, "
"Django provides a view decorator which forces setting of the cookie: "
":func:`~django.views.decorators.csrf.ensure_csrf_cookie`."
msgstr ""
"Якщо ваше представлення не обробляє шаблон, що містить шаблонний "
"тег :ttag:`csrf_token`, Django може не встановити файл cookie з CSRF токеном. "
"Це поширено в тих випадках, коли форми динамічно додаються на сторінку. Для "
"вирішення цієї проблеми, Django надає декоратор представлення, який "
"встановлює налаштування файлу cookie: :func:`~django.views.decorators.csrf.ens"
"ure_csrf_cookie`."

# 5741ab34c507497fbb094fecefa3bcca
#: ../../ref/contrib/csrf.txt:145
msgid ""
"Finally, you'll have to actually set the header on your AJAX request, while "
"protecting the CSRF token from being sent to other domains."
msgstr ""
"Зрештою, вам доведеться фактично встановити заголовок у вашому AJAX "
"запиті, одночасно захищаючи CSRF токен від відправки на інші домени."

# 16e9dcece3034c1b99d360a59d3db8fd
#: ../../ref/contrib/csrf.txt:180
msgid ""
"Due to a bug introduced in jQuery 1.5, the example above will not work "
"correctly on that version. Make sure you are running at least jQuery 1.5.1."
msgstr ""
"Через помилки, допущені в jQuery 1.5, приклад вище не працюватиме правильно"
" в цій версії. Переконайтеся, що ви використовуєте принаймні jQuery 1.5.1."

# 359d49d5fdbe474cbaed4a833483f460
#: ../../ref/contrib/csrf.txt:183
msgid ""
"You can use `settings.crossDomain <http://api.jquery.com/jQuery.ajax>`_ in "
"jQuery 1.5 and newer in order to replace the ``sameOrigin`` logic above:"
msgstr ""
"Ви можете використовувати `settings.crossDomain <"
"http://api.jquery.com/jQuery.ajax>`_ "
"в jQuery 1.5 і новіше для того, щоб замінити логіку ``sameOrigin``, описану "
"вище:"

# 33873bea736340c68263bd9f69b9ab25
#: ../../ref/contrib/csrf.txt:202
msgid ""
"In a `security release blogpost`_, a simpler \"same origin test\" example "
"was provided which only checked for a relative URL. The ``sameOrigin`` test "
"above supersedes that example—it works for edge cases like scheme-relative "
"or absolute URLs for the same domain."
msgstr ""
"В `security release blogpost`_ був наданий простий \"same origin test\" "
"приклад, який перевіряє тільки відносний URL. ``sameOrigin`` тест вище"
" замінює цей приклад - це працює для крайніх випадків, наприклад, для "
"залежних "
"від схем або абсолютних URL-адрес для того ж домена."

# c37b4428dd3f46d394c38ab2343080c4
#: ../../ref/contrib/csrf.txt:210
msgid "Other template engines"
msgstr "Інші шаблонізатор"

# baec80648919484d9857cb648d88913f
#: ../../ref/contrib/csrf.txt:212
msgid ""
"When using a different template engine than Django's built-in engine, you "
"can set the token in your forms manually after making sure it's available in"
" the template context."
msgstr ""
"Якщо використовуються шаблонізатор відмінні від вбудованого в Django, ви "
"повинні встановити токен у ваших формах вручну, після того як переконалися, "
"що він доступний в контексті шаблону."

# f971a1474c32499eb31c2c0de30a7f88
#: ../../ref/contrib/csrf.txt:216
msgid ""
"For example, in the Cheetah template language, your form could contain the "
"following:"
msgstr "Наприклад, в шаблонізаторі Cheetah, ваша форма може містити наступне:"

# 7be1408e448f4f98b4e24ebd21be662a
#: ../../ref/contrib/csrf.txt:225
msgid ""
"You can use JavaScript similar to the :ref:`AJAX code <csrf-ajax>` above to "
"get the value of the CSRF token."
msgstr ""
"Ви можете використовувати JavaScript подібний до "
":ref:`AJAX code <csrf-ajax>` для отримання значення CSRF токена."

# 1ec02baedad44ec4aee4dad9a6087d9b
#: ../../ref/contrib/csrf.txt:229
msgid "The decorator method"
msgstr "Підхід з використанням декоратора"

# 355684ea506643c9adbe9f2eee28211f
#: ../../ref/contrib/csrf.txt:233
msgid ""
"Rather than adding ``CsrfViewMiddleware`` as a blanket protection, you can "
"use the ``csrf_protect`` decorator, which has exactly the same "
"functionality, on particular views that need the protection. It must be used"
" **both** on views that insert the CSRF token in the output, and on those "
"that accept the POST form data. (These are often the same view function, but"
" not always)."
msgstr ""
"Замість додавання ``CsrfViewMiddleware`` для загального захисту, ви можете "
"використовувати декоратор ``csrf_protect``, який володіє точно такою ж "
"функціональністю, для конкретних представлень, які потрібно захистити. Це "
"повинно"
" бути використано і в представленнях, які вставляють CSRF токен у вихідний "
"вміст, і в тих,"
" які приймають POST дані форм."

# e65f8c9376344ffa87c520fda013ce55
#: ../../ref/contrib/csrf.txt:239
msgid ""
"Use of the decorator by itself is **not recommended**, since if you forget "
"to use it, you will have a security hole. The 'belt and braces' strategy of "
"using both is fine, and will incur minimal overhead."
msgstr ""
"Використання тільки декоратора **не рекомендується**, оскільки, якщо ви "
"забудете використати його, ви отримаєте дірку в безпеці. Стратегія "
"'ременя і підтяжок' ('belt and braces'), в сенсі використання обох методів, "
"виглядає краще, і "
"призводить до мінімальних накладних витрат."

# 260516215abd4d5ea7da8ebc7b682d78
#: ../../ref/contrib/csrf.txt:245
msgid ""
"Decorator that provides the protection of ``CsrfViewMiddleware`` to a view."
msgstr ""
"Декоратор, який забезпечує захист ``CsrfViewMiddleware`` для представлення."

# 2efa42f62b54425591bdbb9837521f5f
#: ../../ref/contrib/csrf.txt:247
msgid "Usage::"
msgstr "Використання::"

# 24a48aecebb94f519dc0bfb662aded0c
#: ../../ref/contrib/csrf.txt:259
msgid "Rejected requests"
msgstr "Відхилені запити"

# 2d992ecfded245e389dfed3fbc332bfe
#: ../../ref/contrib/csrf.txt:261
msgid ""
"By default, a '403 Forbidden' response is sent to the user if an incoming "
"request fails the checks performed by ``CsrfViewMiddleware``.  This should "
"usually only be seen when there is a genuine Cross Site Request Forgery, or "
"when, due to a programming error, the CSRF token has not been included with "
"a POST form."
msgstr ""
"За замовчуванням, відповідь ' 403 Forbidden ' надсилається користувачеві, "
"якщо "
"вхідний запит не пройшов перевірок, виконуваних ``CsrfViewMiddleware``. "
"Зазвичай це відбуватиметься, якщо це дійсно CSRF, або через помилку в коді, "
"якщо CSRF токен не був включений у POST форму."

# 2d0bb8dccd9a433f82fe5433175e169a
#: ../../ref/contrib/csrf.txt:267
msgid ""
"The error page, however, is not very friendly, so you may want to provide "
"your own view for handling this condition.  To do this, simply set the "
":setting:`CSRF_FAILURE_VIEW` setting."
msgstr ""
"Сторінка помилки не дуже дружелюбна, тому ви можете надавати власне "
"представлення для обробки цього стану. Щоб зробити це, просто встановіть "
"параметр"
" :setting:`CSRF_FAILURE_VIEW`."

# 32e4c0318c74480ba5371deb93e37d76
#: ../../ref/contrib/csrf.txt:274
msgid "How it works"
msgstr "Як це працює"

# 338f505eb2a5469582fdcff0143695f4
#: ../../ref/contrib/csrf.txt:276
msgid "The CSRF protection is based on the following things:"
msgstr "CSRF базується на наступних речах:"

# 5159af6c18aa482a95b9e36def570449
#: ../../ref/contrib/csrf.txt:278
msgid ""
"A CSRF cookie that is set to a random value (a session independent nonce, as"
" it is called), which other sites will not have access to."
msgstr ""
"CSRF файл cookie, який встановлюється як випадкове число (сесія незалежного "
"випадку, як це ще іноді називають), до якого інші сайти не "
"матимуть доступу."

# c950960f752e4bb68e98c43265589837
#: ../../ref/contrib/csrf.txt:281
msgid ""
"This cookie is set by ``CsrfViewMiddleware``.  It is meant to be permanent, "
"but since there is no way to set a cookie that never expires, it is sent "
"with every response that has called ``django.middleware.csrf.get_token()`` "
"(the function used internally to retrieve the CSRF token)."
msgstr ""
"Цей файл cookie встановлюється за допомогою ``CsrfViewMiddleware``. Він має "
"бути "
"постійним, але так як немає способу встановити файли cookies, у яких ніколи "
"не "
"закінчується час життя, то він надсилається з кожною відповіддю, яка викликає "
"``django.middleware.csrf.get_token()`` (функція використовувалася внутрішньо "
"для отримання CSRF токена)."

# 3f9e807789144555bab5f69db996a1ba
#: ../../ref/contrib/csrf.txt:286
msgid ""
"A hidden form field with the name 'csrfmiddlewaretoken' present in all "
"outgoing POST forms.  The value of this field is the value of the CSRF "
"cookie."
msgstr ""
"Всі POST форми містять приховане поле 'csrfmiddlewaretoken'. Значення поля"
" це значення CSRF файла cookie."

# e79a6ebd24864902ab7dcfb4386c5476
#: ../../ref/contrib/csrf.txt:290
msgid "This part is done by the template tag."
msgstr "Ця частина виконується шаблонним тегом."

# 01c1df4d391b4acbbe5cd311f388605b
#: ../../ref/contrib/csrf.txt:292
msgid ""
"For all incoming requests that are not using HTTP GET, HEAD, OPTIONS or "
"TRACE, a CSRF cookie must be present, and the 'csrfmiddlewaretoken' field "
"must be present and correct. If it isn't, the user will get a 403 error."
msgstr ""
"Всі HTTP запити, крім GET, HEAD, OPTIONS або TRACE, повинні містити CSRF "
"файл cookie і поле 'csrfmiddlewaretoken' з правильним значенням. Інакше "
"користувач отримає 403 помилку."

# 0c37613c8da842f48ab11688fac5c85b
#: ../../ref/contrib/csrf.txt:296
msgid "This check is done by ``CsrfViewMiddleware``."
msgstr "Ця перевірка виконується в ``CsrfViewMiddleware``."

# 26521fd10bc84544a0b206e9f83b2dfd
#: ../../ref/contrib/csrf.txt:298
msgid ""
"In addition, for HTTPS requests, strict referer checking is done by "
"``CsrfViewMiddleware``.  This is necessary to address a Man-In-The-Middle "
"attack that is possible under HTTPS when using a session independent nonce, "
"due to the fact that HTTP 'Set-Cookie' headers are (unfortunately) accepted "
"by clients that are talking to a site under HTTPS.  (Referer checking is not"
" done for HTTP requests because the presence of the Referer header is not "
"reliable enough under HTTP.)"
msgstr ""
"На додаток, для HTTPS запитів в ``CsrfViewMiddleware`` перевіряється "
"\"referer\" (джерело запиту). Це необхідно для запобігання MITM-атак "
"(Man-In-The-Middle), які можлива при використанні HTTPS і токена, який не "
"прив'язаний до сесії, тому що заголовки HTTP 'Set-Cookie' приймаються "
"клієнтами "
"сайтів під HTTPS. (Для HTTP запитів не перевіряється джерело запитів, тому що "
"заголовок \"referer\" не достатньо надійний у HTTP.)"

# 44fe714cf7104512b2a94d76680bd4b3
#: ../../ref/contrib/csrf.txt:306
msgid ""
"This ensures that only forms that have originated from your Web site can be "
"used to POST data back."
msgstr ""
"Такий підхід гарантує, що тільки форми, відправлені через ваш сайт, можуть "
"передавати POST дані."

# 4f62853e853f47ec9d39a19a05223541
#: ../../ref/contrib/csrf.txt:309
msgid ""
"It deliberately ignores GET requests (and other requests that are defined as"
" 'safe' by :rfc:`2616`). These requests ought never to have any potentially "
"dangerous side effects , and so a CSRF attack with a GET request ought to be"
" harmless. :rfc:`2616` defines POST, PUT and DELETE as 'unsafe', and all "
"other methods are assumed to be unsafe, for maximum protection."
msgstr ""
"GET запити ігноруються свідомо (і всі інші запити, які вважаються "
"\"безпечними\" "
"відповідно до :rfc:`2616`). Ці запити ніколи не повинні мати ніякого "
"потенційно небезпечного впливу, і CSRF атаки через GET запит повиненні бути "
"нешкідливими. :rfc:`2616` визначає POST, PUT і DELETE та всі інші запити як "
"\"небезпечні\" "
"для забезпечення максимального захисту."

# 5f962433479a4c43a40fd5541633be45
#: ../../ref/contrib/csrf.txt:316
msgid "Caching"
msgstr "Кешування"

# 6aaaa625133a40b9964e3145cf15c5b8
#: ../../ref/contrib/csrf.txt:318
msgid ""
"If the :ttag:`csrf_token` template tag is used by a template (or the "
"``get_token`` function is called some other way), ``CsrfViewMiddleware`` "
"will add a cookie and a ``Vary: Cookie`` header to the response. This means "
"that the middleware will play well with the cache middleware if it is used "
"as instructed (``UpdateCacheMiddleware`` goes before all other middleware)."
msgstr ""
"При використанні шаблонного тега :ttag:`csrf_token` (або виклику функції "
"``get_token`` ) ``CsrfViewMiddleware`` додасть файл cookie і заголовок "
"``Vary: Cookie`` у відповідь."

# 0b5e2b052d37473d9ea7abd15f8c2ef4
#: ../../ref/contrib/csrf.txt:324
msgid ""
"However, if you use cache decorators on individual views, the CSRF "
"middleware will not yet have been able to set the Vary header or the CSRF "
"cookie, and the response will be cached without either one. In this case, on"
" any views that will require a CSRF token to be inserted you should use the "
":func:`django.views.decorators.csrf.csrf_protect` decorator first::"
msgstr ""
"Однак, якщо ви використовує декоратор кешування для конкретного "
"представлення, "
"функціональний шар CSRF ще не встановив \"Vary\" заголовок або файл cookie "
"CSRF, і "
"відповідь буде закешована без них. У такому випадку для всіх таких "
"представлень, які вимагають CSRF токен, використовуйте спочатку декоратор "
":func:`django.views.decorators.csrf.csrf_protect`::"

# e3a4bf3c389f41f68a74d344cc6ee00a
#: ../../ref/contrib/csrf.txt:340
msgid "Testing"
msgstr "Тестування"

# 06f6fba35f5f477db6bdccadae795059
#: ../../ref/contrib/csrf.txt:342
msgid ""
"The ``CsrfViewMiddleware`` will usually be a big hindrance to testing view "
"functions, due to the need for the CSRF token which must be sent with every "
"POST request.  For this reason, Django's HTTP client for tests has been "
"modified to set a flag on requests which relaxes the middleware and the "
"``csrf_protect`` decorator so that they no longer rejects requests.  In "
"every other respect (e.g. sending cookies etc.), they behave the same."
msgstr ""
"``CsrfViewMiddleware`` - зазвичай велика перешкода при тестуванні "
"представлень, "
"оскільки кожен POST запит повинен містити CSRF токен. Тому тестовий клієнт "
"Django для кожного запиту встановлює спеціальний прапорець, який враховується "
"декоратором ``csrf_protect`` і не повертає помилку."

# dc87828dc1c0427b9f65d9541af0cc8a
#: ../../ref/contrib/csrf.txt:349
msgid ""
"If, for some reason, you *want* the test client to perform CSRF checks, you "
"can create an instance of the test client that enforces CSRF checks::"
msgstr ""
"Якщо, з якоїсь причини, ви *хочете*, щоб тестовий клієнт виконував перевірку"
" CSRF, ви можете створити спеціальний примірник тестового клієнта::"

# c3e9670b2ff5498cbe9a491e4697274e
#: ../../ref/contrib/csrf.txt:359
msgid "Limitations"
msgstr "Обмеження"

# 550b8dc89fe5493cb3e98807772615e9
#: ../../ref/contrib/csrf.txt:361
msgid ""
"Subdomains within a site will be able to set cookies on the client for the "
"whole domain.  By setting the cookie and using a corresponding token, "
"subdomains will be able to circumvent the CSRF protection.  The only way to "
"avoid this is to ensure that subdomains are controlled by trusted users (or,"
" are at least unable to set cookies).  Note that even without CSRF, there "
"are other vulnerabilities, such as session fixation, that make giving "
"subdomains to untrusted parties a bad idea, and these vulnerabilities cannot"
" easily be fixed with current browsers."
msgstr ""
"Піддомени сайту можуть встановлювати файли cookies на клієнті для всього "
"домену. "
"Встановивши відповідний токен в файлі cookie, піддомени можуть обійти CSRF "
"захист. "
"Єдиний спосіб підвищити безпеку - бути впевненим, що піддомени контролюються "
"довіреними користувачами (або хоча б не можуть встановлювати файли cookies)."

# 62fd5873d54145039c0f7448fcbf8a7a
#: ../../ref/contrib/csrf.txt:370
msgid "Edge cases"
msgstr "Особливі випадки"

# 862d27a83c1f41099c8ef94b0d8b6fe9
#: ../../ref/contrib/csrf.txt:372
msgid ""
"Certain views can have unusual requirements that mean they don't fit the "
"normal pattern envisaged here. A number of utilities can be useful in these "
"situations. The scenarios they might be needed in are described in the "
"following section."
msgstr ""
"Деякі представлення можуть вимагати особливого процесу обробки запиту, який "
"не "
"вписується в описаний вище шаблон. Нижче наведено ряд функцій, які можуть "
"стати в нагоді в такому випадку."

# e1da9bd092494d3a9a4924d47c626e07
#: ../../ref/contrib/csrf.txt:378
msgid "Utilities"
msgstr "Утиліти"

# ed0d84599acc48ecbb2749304ff0876e
#: ../../ref/contrib/csrf.txt:382
msgid ""
"This decorator marks a view as being exempt from the protection ensured by "
"the middleware. Example::"
msgstr ""
"Цей декоратор дозволяє виключити представлення з процесу перевірки CSRF. "
"Наприклад::"

# a08ef260565c4be883b6d276526f07d1
#: ../../ref/contrib/csrf.txt:394
msgid ""
"Normally the :ttag:`csrf_token` template tag will not work if "
"``CsrfViewMiddleware.process_view`` or an equivalent like ``csrf_protect`` "
"has not run. The view decorator ``requires_csrf_token`` can be used to "
"ensure the template tag does work. This decorator works similarly to "
"``csrf_protect``, but never rejects an incoming request."
msgstr ""
"Зазвичай шаблонний тег :ttag:`csrf_token` нічого не робить, якщо "
"``CsrfViewMiddleware.process_view``, або його аналог ``csrf_protect``, не "
"був виконаний. Декоратор ``requires_csrf_token`` можна використовувати, щоб "
"переконатись, що шаблонний тег спрацював. Цей декоратор працює як і "
"``csrf_protect``, але не повертає відповідь з помилкою."

# ea94ed94f82045a1a99bb29e3e6c2ae9
#: ../../ref/contrib/csrf.txt:400
msgid "Example::"
msgstr "Приклади::"

# 43e7b4fec5b147ee98aba9b94c09c5f1
#: ../../ref/contrib/csrf.txt:413
msgid "This decorator forces a view to send the CSRF cookie."
msgstr "Цей декоратор змушує представлення послати CSRF файл cookie."

# 88233430f2634740b8f700667e8e9d14
#: ../../ref/contrib/csrf.txt:416
msgid "Scenarios"
msgstr "Сценарії"

# 129339f01b074325adc28f1d48fe90cb
#: ../../ref/contrib/csrf.txt:419
msgid "CSRF protection should be disabled for just a few views"
msgstr "Необхідно відключити CSRF захист для деяких представлень"

# 6b9ec3a996ca4bd09beeab0ff863ef39
#: ../../ref/contrib/csrf.txt:421
msgid "Most views requires CSRF protection, but a few do not."
msgstr ""
"Для більшості представлень необхідний CSRF захист, але для деяких необхідно "
"його відключити."

# 59b01b05e37c497096b640f104955a56
#: ../../ref/contrib/csrf.txt:423
msgid ""
"Solution: rather than disabling the middleware and applying ``csrf_protect``"
" to all the views that need it, enable the middleware and use "
":func:`~django.views.decorators.csrf.csrf_exempt`."
msgstr ""
"Замість того, щоб відключити функціональний шар захисту і додати декоратор "
"``csrf_protect`` до всіх представлень, яким необхідна перевірка CRSF, включіть"
" функціональний шар і використовуйте :func:`~django.views.decorators.csrf.csrf"
"_exempt`."

# 9221ff60514b415e88c12d66452ef105
#: ../../ref/contrib/csrf.txt:428
msgid "CsrfViewMiddleware.process_view not used"
msgstr "CsrfViewMiddleware.process_view не використовується"

# 1b9c3c8cb81842269c54afd109eaa454
#: ../../ref/contrib/csrf.txt:430
msgid ""
"There are cases when ``CsrfViewMiddleware.process_view`` may not have run "
"before your view is run - 404 and 500 handlers, for example - but you still "
"need the CSRF token in a form."
msgstr ""
"У деяких випадках ``CsrfViewMiddleware.process_view`` може не виконуватися "
"перед вашим представленням, наприклад, для обробників 404 і 500 відповідей. "
"Але "
"вам все ще необхідний CSRF токен для форми."

# dd3277bf78c848279ec13d351b15cdd8
#: ../../ref/contrib/csrf.txt:434
msgid "Solution: use :func:`~django.views.decorators.csrf.requires_csrf_token`"
msgstr ""
"Рішення: "
"використовуйте :func:`~django.views.decorators.csrf.requires_csrf_token`"

# 7974f7efc2f3457084b240966632c04c
#: ../../ref/contrib/csrf.txt:437
msgid "Unprotected view needs the CSRF token"
msgstr "Для незахищеного представлення необхідний CSRF токен"

# b836d4c501cc478f871e07e8916996fe
#: ../../ref/contrib/csrf.txt:439
msgid ""
"There may be some views that are unprotected and have been exempted by "
"``csrf_exempt``, but still need to include the CSRF token."
msgstr ""
"Деякі представлення можуть бути виключені з процесу перевірки CRSF за "
"допомогою "
"декоратора ``csrf_exempt``, але їм необхідний CSRF токен."

# 5002bba3dec94cffaa8b403109f0c3dd
#: ../../ref/contrib/csrf.txt:442
msgid ""
"Solution: use :func:`~django.views.decorators.csrf.csrf_exempt` followed by "
":func:`~django.views.decorators.csrf.requires_csrf_token`. (i.e. "
"``requires_csrf_token`` should be the innermost decorator)."
msgstr ""
"Рішення: "
"використовуйте :func:`~django.views.decorators.csrf.requires_csrf_token` "
"після :func:`~django.views.decorators.csrf.csrf_exempt`. (Тобто "
"``requires_csrf_token`` повинен бути \"найбільш внутрішнім\" декоратором)."

# 4dc9dc49413a416e9f98d2c83d262cf8
#: ../../ref/contrib/csrf.txt:447
msgid "View needs protection for one path"
msgstr "Представленням необхідна перевірка CSRF тільки для певного шляху"

# 94fa894400a7451f9fde64c585274857
#: ../../ref/contrib/csrf.txt:449
msgid ""
"A view needs CSRF protection under one set of conditions only, and mustn't "
"have it for the rest of the time."
msgstr ""
"Представленням необхідна перевірка CSRF тільки за певних умов, і не потрібна "
"для "
"всіх інших випадків."

# c81b772312c64861b37b80841075eb43
#: ../../ref/contrib/csrf.txt:452
msgid ""
"Solution: use :func:`~django.views.decorators.csrf.csrf_exempt` for the "
"whole view function, and :func:`~django.views.decorators.csrf.csrf_protect` "
"for the path within it that needs protection. Example::"
msgstr ""
"Рішення: використовуйте :func:`~django.views.decorators.csrf.csrf_exempt` для"
" всієї функції представлення, і "
":func:`~django.views.decorators.csrf.csrf_protect` при певній умові "
"всередині функції. Наприклад::"

# d3dd82a798c14efc9f872a42c977bda8
#: ../../ref/contrib/csrf.txt:471
msgid "Page uses AJAX without any HTML form"
msgstr "Сторінка використовує AJAX, не маючи жодної HTML форми"

# 7b75215ccf194843b8276f9485d428ae
#: ../../ref/contrib/csrf.txt:473
msgid ""
"A page makes a POST request via AJAX, and the page does not have an HTML "
"form with a :ttag:`csrf_token` that would cause the required CSRF cookie to "
"be sent."
msgstr ""
"Сторінка виконує POST запит через AJAX, на сторінці немає HTML форми з тегом"
" :ttag:`csrf_token`, який викликає відправку CSRF файла cookie."

# 0625536e899c487eb9f1a5b5724ce9b7
#: ../../ref/contrib/csrf.txt:476
msgid ""
"Solution: use :func:`~django.views.decorators.csrf.ensure_csrf_cookie` on "
"the view that sends the page."
msgstr ""
"Рішення: "
"використовуйте :func:`~django.views.decorators.csrf.ensure_csrf_cookie` в "
"представленні, яке відсилає сторінку."

# 5299b1b1b3504a57b5e8cac3fdf63799
#: ../../ref/contrib/csrf.txt:480
msgid "Contrib and reusable apps"
msgstr "Вбудовані і поширювані додатки"

# 9d327a128751484f81c54ba461ddf1c5
#: ../../ref/contrib/csrf.txt:482
msgid ""
"Because it is possible for the developer to turn off the "
"``CsrfViewMiddleware``, all relevant views in contrib apps use the "
"``csrf_protect`` decorator to ensure the security of these applications "
"against CSRF.  It is recommended that the developers of other reusable apps "
"that want the same guarantees also use the ``csrf_protect`` decorator on "
"their views."
msgstr ""
"Оскільки розробник може відключити ``CsrfViewMiddleware``, всі відповідні "
"представлення у вбудованих додатках використовують декоратор "
"``csrf_protect``, щоб"
" переконатися, що ці представлення захищені від CSRF."

# 3e1dfed04ced4dffbaeeb80eb12fce75
#: ../../ref/contrib/csrf.txt:489
msgid "Settings"
msgstr "Параметри"

# 00efffa3ef6c49608ddaada4fde446c9
#: ../../ref/contrib/csrf.txt:491
msgid "A number of settings can be used to control Django's CSRF behavior:"
msgstr ""
"Параметри, які можуть бути використані для управління поведінкою CSRF в "
"Django:"

# 2d5d8ac95c764e5a9b484e9327158ccd
#: ../../ref/contrib/csrf.txt:493
msgid ":setting:`CSRF_COOKIE_AGE`"
msgstr ":setting:`CSRF_COOKIE_AGE`"

# 234fd54724fb4c7eb2d55d2a46ee85fa
#: ../../ref/contrib/csrf.txt:494
msgid ":setting:`CSRF_COOKIE_DOMAIN`"
msgstr ":setting:`CSRF_COOKIE_DOMAIN`"

# 25c9b28e79b94fc592ce89f56b223e57
#: ../../ref/contrib/csrf.txt:495
msgid ":setting:`CSRF_COOKIE_HTTPONLY`"
msgstr ":setting:`CSRF_COOKIE_HTTPONLY`"

# b1ba4073313c48619c8c6e9b6f4f43df
#: ../../ref/contrib/csrf.txt:496
msgid ":setting:`CSRF_COOKIE_NAME`"
msgstr ":setting:`CSRF_COOKIE_NAME`"

# e058902fb33d47d180aea519b3ba91cb
#: ../../ref/contrib/csrf.txt:497
msgid ":setting:`CSRF_COOKIE_PATH`"
msgstr ":setting:`CSRF_COOKIE_PATH`"

# 49b85e19ec284ea59fc53b4a3ffe4660
#: ../../ref/contrib/csrf.txt:498
msgid ":setting:`CSRF_COOKIE_SECURE`"
msgstr ":setting:`CSRF_COOKIE_SECURE`"

# 613924a5c59448d4bd449dd823cc0971
#: ../../ref/contrib/csrf.txt:499
msgid ":setting:`CSRF_FAILURE_VIEW`"
msgstr ":setting:`CSRF_FAILURE_VIEW`"


