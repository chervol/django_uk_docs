# 
msgid ""
msgstr ""
"Project-Id-Version: Django 1.7\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2014-10-13 18:33+0300\n"
"Last-Translator: Volodymyr Cherepanyak <chervol@gmail.com>\n"
"Language-Team: Quintagroup <info@quintagroup.com>\n"
"Content-Transfer-Encoding: 8bit\n"" Content-Type: text/plain; charset=UTF-8\n"
"MIME-Version: 1.0\n"
"Language: uk\n"

# 4042cc43257b43b98f9631f610584281
#: ../../ref/contrib/admin/actions.txt:3
msgid "Admin actions"
msgstr ""

# c57ed3cbd3154763b2c7813717fde1e9
#: ../../ref/contrib/admin/actions.txt:7
msgid ""
"The basic workflow of Django's admin is, in a nutshell, \"select an object, "
"then change it.\" This works well for a majority of use cases. However, if "
"you need to make the same change to many objects at once, this workflow can "
"be quite tedious."
msgstr ""

# ab78730e6d554cbeb62c8cf5834dd65b
#: ../../ref/contrib/admin/actions.txt:12
msgid ""
"In these cases, Django's admin lets you write and register \"actions\" -- "
"simple functions that get called with a list of objects selected on the "
"change list page."
msgstr ""

# a2f49793fdab41e886eb6b47c81455c3
#: ../../ref/contrib/admin/actions.txt:16
msgid ""
"If you look at any change list in the admin, you'll see this feature in "
"action; Django ships with a \"delete selected objects\" action available to "
"all models. For example, here's the user module from Django's built-in "
":mod:`django.contrib.auth` app:"
msgstr ""

# 2f5d635e8b1d4495870a28c6a5044cbe
#: ../../ref/contrib/admin/actions.txt:25
msgid ""
"The \"delete selected objects\" action uses :meth:`QuerySet.delete() "
"<django.db.models.query.QuerySet.delete>` for efficiency reasons, which has "
"an important caveat: your model's ``delete()`` method will not be called."
msgstr ""

# a5961f1c955845ee8beeb03d0efa7fc4
#: ../../ref/contrib/admin/actions.txt:30
msgid ""
"If you wish to override this behavior, simply write a custom action which "
"accomplishes deletion in your preferred manner -- for example, by calling "
"``Model.delete()`` for each of the selected items."
msgstr ""

# 5df0c59ab53b4fd2991ae044e8807eaa
#: ../../ref/contrib/admin/actions.txt:34
msgid ""
"For more background on bulk deletion, see the documentation on :ref:`object "
"deletion <topics-db-queries-delete>`."
msgstr ""

# 3aa579c7fa894ba3b92aa8b3b25008bf
#: ../../ref/contrib/admin/actions.txt:37
msgid "Read on to find out how to add your own actions to this list."
msgstr ""

# 848d246f002a492d866ced3e53955db9
#: ../../ref/contrib/admin/actions.txt:40
msgid "Writing actions"
msgstr ""

# abc8503a960c486087133496eddcb2ac
#: ../../ref/contrib/admin/actions.txt:42
msgid "The easiest way to explain actions is by example, so let's dive in."
msgstr ""

# f9d85b49b04549919fbc1ff4c456844d
#: ../../ref/contrib/admin/actions.txt:44
msgid ""
"A common use case for admin actions is the bulk updating of a model. Imagine"
" a simple news application with an ``Article`` model::"
msgstr ""

# 30f5d342fbfd45218fb38e2805856c3f
#: ../../ref/contrib/admin/actions.txt:63
msgid ""
"A common task we might perform with a model like this is to update an "
"article's status from \"draft\" to \"published\". We could easily do this in"
" the admin one article at a time, but if we wanted to bulk-publish a group "
"of articles, it'd be tedious. So, let's write an action that lets us change "
"an article's status to \"published.\""
msgstr ""

# ce633b971a814d749c5b15bbf92e71da
#: ../../ref/contrib/admin/actions.txt:70
msgid "Writing action functions"
msgstr ""

# 7b2c433afae2407a94e2d29471aaf0a2
#: ../../ref/contrib/admin/actions.txt:72
msgid ""
"First, we'll need to write a function that gets called when the action is "
"trigged from the admin. Action functions are just regular functions that "
"take three arguments:"
msgstr ""

# 1ed587057c874d799d255d784e887805
#: ../../ref/contrib/admin/actions.txt:76
msgid "The current :class:`ModelAdmin`"
msgstr ""

# d45f190c467d4fdc894fdf814389ce31
#: ../../ref/contrib/admin/actions.txt:77
msgid "An :class:`~django.http.HttpRequest` representing the current request,"
msgstr ""

# 9350aef44336469fa5df816edbe7861c
#: ../../ref/contrib/admin/actions.txt:78
msgid ""
"A :class:`~django.db.models.query.QuerySet` containing the set of objects "
"selected by the user."
msgstr ""

# 3952a2cf6ca1470db28d668f473fc00b
#: ../../ref/contrib/admin/actions.txt:81
msgid ""
"Our publish-these-articles function won't need the :class:`ModelAdmin` or "
"the request object, but we will use the queryset::"
msgstr ""

# 31dd1aa89d234263be02976a489fead5
#: ../../ref/contrib/admin/actions.txt:89
msgid ""
"For the best performance, we're using the queryset's :ref:`update method "
"<topics-db-queries-update>`. Other types of actions might need to deal with "
"each object individually; in these cases we'd just iterate over the "
"queryset::"
msgstr ""

# 74e08c85dd464325b1006e297e427ec6
#: ../../ref/contrib/admin/actions.txt:97
msgid ""
"That's actually all there is to writing an action! However, we'll take one "
"more optional-but-useful step and give the action a \"nice\" title in the "
"admin. By default, this action would appear in the action list as \"Make "
"published\" -- the function name, with underscores replaced by spaces. "
"That's fine, but we can provide a better, more human-friendly name by giving"
" the ``make_published`` function a ``short_description`` attribute::"
msgstr ""

# ae2240b91ed04b5fb5d2fba794cb3a1b
#: ../../ref/contrib/admin/actions.txt:110
msgid ""
"This might look familiar; the admin's ``list_display`` option uses the same "
"technique to provide human-readable descriptions for callback functions "
"registered there, too."
msgstr ""

# b30a1aef898c45c89f164cc967114d29
#: ../../ref/contrib/admin/actions.txt:115
msgid "Adding actions to the :class:`ModelAdmin`"
msgstr ""

# c10aa65a26f848cbb79b6065c42d5a5f
#: ../../ref/contrib/admin/actions.txt:117
msgid ""
"Next, we'll need to inform our :class:`ModelAdmin` of the action. This works"
" just like any other configuration option. So, the complete ``admin.py`` "
"with the action and its registration would look like::"
msgstr ""

# f3d68aae7de74ada9bf288ec8249b419
#: ../../ref/contrib/admin/actions.txt:135
msgid ""
"That code will give us an admin change list that looks something like this:"
msgstr ""

# 95a4557bc3cf452894095e183056318c
#: ../../ref/contrib/admin/actions.txt:139
msgid ""
"That's really all there is to it! If you're itching to write your own "
"actions, you now know enough to get started. The rest of this document just "
"covers more advanced techniques."
msgstr ""

# ed0868704a9d4141b8ba05a9c014f156
#: ../../ref/contrib/admin/actions.txt:144
msgid "Handling errors in actions"
msgstr ""

# 4a877e8a9759449eb736a1d1c4e8d9cf
#: ../../ref/contrib/admin/actions.txt:146
msgid ""
"If there are foreseeable error conditions that may occur while running your "
"action, you should gracefully inform the user of the problem. This means "
"handling exceptions and using "
":meth:`django.contrib.admin.ModelAdmin.message_user` to display a user "
"friendly description of the problem in the response."
msgstr ""

# d3766e15027247b386903f01304990c5
#: ../../ref/contrib/admin/actions.txt:153
msgid "Advanced action techniques"
msgstr ""

# 5804e00d92a64b67a32622033fcfaa99
#: ../../ref/contrib/admin/actions.txt:155
msgid ""
"There's a couple of extra options and possibilities you can exploit for more"
" advanced options."
msgstr ""

# d0ea1d00c72f434fb03852570c33b254
#: ../../ref/contrib/admin/actions.txt:159
msgid "Actions as :class:`ModelAdmin` methods"
msgstr ""

# a525a05f3b6b41029a8472cc59434c86
#: ../../ref/contrib/admin/actions.txt:161
msgid ""
"The example above shows the ``make_published`` action defined as a simple "
"function. That's perfectly fine, but it's not perfect from a code design "
"point of view: since the action is tightly coupled to the ``Article`` "
"object, it makes sense to hook the action to the ``ArticleAdmin`` object "
"itself."
msgstr ""

# 34c0b03f475e4fd98d1a933485a855e8
#: ../../ref/contrib/admin/actions.txt:166
msgid "That's easy enough to do::"
msgstr ""

# 3b91b9ecb77b4e659091fab69994d755
#: ../../ref/contrib/admin/actions.txt:177
msgid ""
"Notice first that we've moved ``make_published`` into a method and renamed "
"the ``modeladmin`` parameter to ``self``, and second that we've now put the "
"string ``'make_published'`` in ``actions`` instead of a direct function "
"reference. This tells the :class:`ModelAdmin` to look up the action as a "
"method."
msgstr ""

# 82a4fa448f5f4864a9d2c77ff9a5d4ea
#: ../../ref/contrib/admin/actions.txt:182
msgid ""
"Defining actions as methods gives the action more straightforward, idiomatic"
" access to the :class:`ModelAdmin` itself, allowing the action to call any "
"of the methods provided by the admin."
msgstr ""

# 476b608fc61a495cac7125d8a8944688
#: ../../ref/contrib/admin/actions.txt:188
msgid ""
"For example, we can use ``self`` to flash a message to the user informing "
"her that the action was successful::"
msgstr ""

# 9c9ca971e66c4de698af2c3653efc695
#: ../../ref/contrib/admin/actions.txt:202
msgid ""
"This make the action match what the admin itself does after successfully "
"performing an action:"
msgstr ""

# da6c17455d7a4b12ba3c99c8572a8685
#: ../../ref/contrib/admin/actions.txt:208
msgid "Actions that provide intermediate pages"
msgstr ""

# 763dce787a2348119564f8347f15b0a4
#: ../../ref/contrib/admin/actions.txt:210
msgid ""
"By default, after an action is performed the user is simply redirected back "
"to the original change list page. However, some actions, especially more "
"complex ones, will need to return intermediate pages. For example, the "
"built-in delete action asks for confirmation before deleting the selected "
"objects."
msgstr ""

# 9443b3daeb784ffa8b77c8305572d475
#: ../../ref/contrib/admin/actions.txt:216
msgid ""
"To provide an intermediary page, simply return an "
":class:`~django.http.HttpResponse` (or subclass) from your action. For "
"example, you might write a simple export function that uses Django's "
":doc:`serialization functions </topics/serialization>` to dump some selected"
" objects as JSON::"
msgstr ""

# 378e1bf2b39f4adda1bb37e767f06c6d
#: ../../ref/contrib/admin/actions.txt:230
msgid ""
"Generally, something like the above isn't considered a great idea. Most of "
"the time, the best practice will be to return an "
":class:`~django.http.HttpResponseRedirect` and redirect the user to a view "
"you've written, passing the list of selected objects in the GET query "
"string. This allows you to provide complex interaction logic on the "
"intermediary pages. For example, if you wanted to provide a more complete "
"export function, you'd want to let the user choose a format, and possibly a "
"list of fields to include in the export. The best thing to do would be to "
"write a small action that simply redirects to your custom export view::"
msgstr ""

# 27940e64a53347c0add3f7941b6de2e4
#: ../../ref/contrib/admin/actions.txt:249
msgid ""
"As you can see, the action is the simple part; all the complex logic would "
"belong in your export view. This would need to deal with objects of any "
"type, hence the business with the ``ContentType``."
msgstr ""

# 6d7c4ed40501437f85f94f4ff2831abf
#: ../../ref/contrib/admin/actions.txt:253
msgid "Writing this view is left as an exercise to the reader."
msgstr ""

# 24887f4e1a564ce1a685a66f00d855a9
#: ../../ref/contrib/admin/actions.txt:258
msgid "Making actions available site-wide"
msgstr ""

# 42efcbf402884fa2998af0ec2acffddf
#: ../../ref/contrib/admin/actions.txt:262
msgid ""
"Some actions are best if they're made available to *any* object in the admin"
" site -- the export action defined above would be a good candidate. You can "
"make an action globally available using :meth:`AdminSite.add_action()`. For "
"example::"
msgstr ""

# 8783a19b6da546b2a70b92ccf71c443d
#: ../../ref/contrib/admin/actions.txt:271
msgid ""
"This makes the ``export_selected_objects`` action globally available as an "
"action named \"export_selected_objects\". You can explicitly give the action"
" a name -- good if you later want to programmatically :ref:`remove the "
"action <disabling-admin-actions>` -- by passing a second argument to "
":meth:`AdminSite.add_action()`::"
msgstr ""

# 2efb75de7b994177809dd5863d4cf5de
#: ../../ref/contrib/admin/actions.txt:282
msgid "Disabling actions"
msgstr ""

# 260b5974b3724040878effba05e6d1a5
#: ../../ref/contrib/admin/actions.txt:284
msgid ""
"Sometimes you need to disable certain actions -- especially those "
":ref:`registered site-wide <adminsite-actions>` -- for particular objects. "
"There's a few ways you can disable actions:"
msgstr ""

# c33214267a7b46fb9f6291b44ebf53a5
#: ../../ref/contrib/admin/actions.txt:289
msgid "Disabling a site-wide action"
msgstr ""

# d8004e2e199240fa92dfddf9f2ba5cc9
#: ../../ref/contrib/admin/actions.txt:293
msgid ""
"If you need to disable a :ref:`site-wide action <adminsite-actions>` you can"
" call :meth:`AdminSite.disable_action()`."
msgstr ""

# 936a132a03494c7394f22854ab3468ac
#: ../../ref/contrib/admin/actions.txt:296
msgid ""
"For example, you can use this method to remove the built-in \"delete "
"selected objects\" action::"
msgstr ""

# f32ea162ea894d609207e4778ad85cfb
#: ../../ref/contrib/admin/actions.txt:301
msgid ""
"Once you've done the above, that action will no longer be available site-"
"wide."
msgstr ""

# 365496e245534768a3a77ca4537fc93e
#: ../../ref/contrib/admin/actions.txt:304
msgid ""
"If, however, you need to re-enable a globally-disabled action for one "
"particular model, simply list it explicitly in your ``ModelAdmin.actions`` "
"list::"
msgstr ""

# 36b7ce20627c42ddacec48f90eb2dc07
#: ../../ref/contrib/admin/actions.txt:323
msgid "Disabling all actions for a particular :class:`ModelAdmin`"
msgstr ""

# 673693f9fe9c49deb5e681b611aeb779
#: ../../ref/contrib/admin/actions.txt:325
msgid ""
"If you want *no* bulk actions available for a given :class:`ModelAdmin`, "
"simply set :attr:`ModelAdmin.actions` to ``None``::"
msgstr ""

# a9e47c39b8284a83ba0d690e6c50c854
#: ../../ref/contrib/admin/actions.txt:331
msgid ""
"This tells the :class:`ModelAdmin` to not display or allow any actions, "
"including any :ref:`site-wide actions <adminsite-actions>`."
msgstr ""

# fdea1402a6b14fdeb0467e1eafd922d8
#: ../../ref/contrib/admin/actions.txt:335
msgid "Conditionally enabling or disabling actions"
msgstr ""

# c3e3fb9367f644c5a76a8fb6335c3a23
#: ../../ref/contrib/admin/actions.txt:339
msgid ""
"Finally, you can conditionally enable or disable actions on a per-request "
"(and hence per-user basis) by overriding :meth:`ModelAdmin.get_actions`."
msgstr ""

# 995b6e5934e444caa6e13d73f65c97b7
#: ../../ref/contrib/admin/actions.txt:342
msgid ""
"This returns a dictionary of actions allowed. The keys are action names, and"
" the values are ``(function, name, short_description)`` tuples."
msgstr ""

# e7cdcd1b76a5423c92d120e72a94d2a7
#: ../../ref/contrib/admin/actions.txt:345
msgid ""
"Most of the time you'll use this method to conditionally remove actions from"
" the list gathered by the superclass. For example, if I only wanted users "
"whose names begin with 'J' to be able to delete objects in bulk, I could do "
"the following::"
msgstr ""
