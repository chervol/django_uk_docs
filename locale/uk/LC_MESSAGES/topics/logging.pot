# SOME DESCRIPTIVE TITLE.
# Copyright (C) Django Software Foundation and contributors
# This file is distributed under the same license as the Django package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: Django 1.7\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2014-10-13 18:33+0300\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#: ../../topics/logging.txt:3
# 381dc6323c4e4fc88e78094de9ef6c2c
msgid "Logging"
msgstr ""

#: ../../topics/logging.txt:9
# 8ed794a4fff34a2dbe885350d0e760c1
msgid "A quick logging primer"
msgstr ""

#: ../../topics/logging.txt:11
# fe9f102cf69c4a0c964fba9febda91d3
msgid "Django uses Python's builtin :mod:`logging` module to perform system logging. The usage of this module is discussed in detail in Python's own documentation. However, if you've never used Python's logging framework (or even if you have), here's a quick primer."
msgstr ""

#: ../../topics/logging.txt:17
# cc920bb9fed2456585ba3dabbeb085c3
msgid "The cast of players"
msgstr ""

#: ../../topics/logging.txt:19
# 6c01fd3bfb534d709bb4f379b61c95c2
msgid "A Python logging configuration consists of four parts:"
msgstr ""

#: ../../topics/logging.txt:21
# 7653011f26f148a69dc8ed914f5a04bc
msgid ":ref:`topic-logging-parts-loggers`"
msgstr ""

#: ../../topics/logging.txt:22
# d4e22f56d79048ea992d6193c73d54c8
msgid ":ref:`topic-logging-parts-handlers`"
msgstr ""

#: ../../topics/logging.txt:23
# 35eea919bb9c47e8a3ca4d708a006586
msgid ":ref:`topic-logging-parts-filters`"
msgstr ""

#: ../../topics/logging.txt:24
# a74fb1083cd84f5fb8fc2029dac435eb
msgid ":ref:`topic-logging-parts-formatters`"
msgstr ""

#: ../../topics/logging.txt:29
#: ../../topics/logging.txt:414
# 0478e51500674122be4e01d06e657eab
# 45b6b20d0f694c93a99e82d0f9cad254
msgid "Loggers"
msgstr ""

#: ../../topics/logging.txt:31
# a185e2ac69304468b333a2f24a875230
msgid "A logger is the entry point into the logging system. Each logger is a named bucket to which messages can be written for processing."
msgstr ""

#: ../../topics/logging.txt:34
# 7460bbeaf7e545569581fc52fa602374
msgid "A logger is configured to have a *log level*. This log level describes the severity of the messages that the logger will handle. Python defines the following log levels:"
msgstr ""

#: ../../topics/logging.txt:38
# 925eb01bf4df4da1b703eedb2c2ae458
msgid "``DEBUG``: Low level system information for debugging purposes"
msgstr ""

#: ../../topics/logging.txt:40
# 69a4242aba7d458b9cd33c1a4993bd24
msgid "``INFO``: General system information"
msgstr ""

#: ../../topics/logging.txt:42
# bb2e3dd075cf487fa1ad554e9b997f92
msgid "``WARNING``: Information describing a minor problem that has occurred."
msgstr ""

#: ../../topics/logging.txt:45
# 73957bab829c4754ae17a8121f29952b
msgid "``ERROR``: Information describing a major problem that has occurred."
msgstr ""

#: ../../topics/logging.txt:48
# 27b3ce163dbf4e9d8ca90d224b7c5e02
msgid "``CRITICAL``: Information describing a critical problem that has occurred."
msgstr ""

#: ../../topics/logging.txt:51
# 2508a7988f3548b1b0543b9be6c8d85f
msgid "Each message that is written to the logger is a *Log Record*. Each log record also has a *log level* indicating the severity of that specific message. A log record can also contain useful metadata that describes the event that is being logged. This can include details such as a stack trace or an error code."
msgstr ""

#: ../../topics/logging.txt:57
# f2742ff1142a4e1cbebf43ab0ea66826
msgid "When a message is given to the logger, the log level of the message is compared to the log level of the logger. If the log level of the message meets or exceeds the log level of the logger itself, the message will undergo further processing. If it doesn't, the message will be ignored."
msgstr ""

#: ../../topics/logging.txt:63
# 2e03da4bdba742f8b135691bcf41b0fa
msgid "Once a logger has determined that a message needs to be processed, it is passed to a *Handler*."
msgstr ""

#: ../../topics/logging.txt:69
#: ../../topics/logging.txt:505
# 058c5cea614843df944f61f12defd7d6
# f2a31dc88a07420db2d3f5862d586871
msgid "Handlers"
msgstr ""

#: ../../topics/logging.txt:71
# 467742e0c77344a5a30b99e7521bcfd6
msgid "The handler is the engine that determines what happens to each message in a logger. It describes a particular logging behavior, such as writing a message to the screen, to a file, or to a network socket."
msgstr ""

#: ../../topics/logging.txt:75
# 18094277550b47a18d6dc46e82424a3d
msgid "Like loggers, handlers also have a log level. If the log level of a log record doesn't meet or exceed the level of the handler, the handler will ignore the message."
msgstr ""

#: ../../topics/logging.txt:79
# 55dacdcd9a5643fe97089e2bffafddcf
msgid "A logger can have multiple handlers, and each handler can have a different log level. In this way, it is possible to provide different forms of notification depending on the importance of a message. For example, you could install one handler that forwards ``ERROR`` and ``CRITICAL`` messages to a paging service, while a second handler logs all messages (including ``ERROR`` and ``CRITICAL`` messages) to a file for later analysis."
msgstr ""

#: ../../topics/logging.txt:90
#: ../../topics/logging.txt:571
# ca5da531500d4680addfdebf6776567c
# 203a492fdaea4f6e8f8d284fe55df161
msgid "Filters"
msgstr ""

#: ../../topics/logging.txt:92
# 4ba038189c4b40729e340a80810f914d
msgid "A filter is used to provide additional control over which log records are passed from logger to handler."
msgstr ""

#: ../../topics/logging.txt:95
# 503a3d49d0104978a2a267078ac0b857
msgid "By default, any log message that meets log level requirements will be handled. However, by installing a filter, you can place additional criteria on the logging process. For example, you could install a filter that only allows ``ERROR`` messages from a particular source to be emitted."
msgstr ""

#: ../../topics/logging.txt:101
# c6d5254d076d4409b5dbf525cc9da30b
msgid "Filters can also be used to modify the logging record prior to being emitted. For example, you could write a filter that downgrades ``ERROR`` log records to ``WARNING`` records if a particular set of criteria are met."
msgstr ""

#: ../../topics/logging.txt:106
# 61237cd0eb83497eae5279d9471b4265
msgid "Filters can be installed on loggers or on handlers; multiple filters can be used in a chain to perform multiple filtering actions."
msgstr ""

#: ../../topics/logging.txt:112
# f9c2ecc85d7c4d3ca8cdb4b8a8b1f30c
msgid "Formatters"
msgstr ""

#: ../../topics/logging.txt:114
# 1a025cbf2e2a46bcb807f9e0445e87df
msgid "Ultimately, a log record needs to be rendered as text. Formatters describe the exact format of that text. A formatter usually consists of a Python formatting string; however, you can also write custom formatters to implement specific formatting behavior."
msgstr ""

#: ../../topics/logging.txt:120
# 8305c6fc3410451d8206f9407eb4b692
msgid "Using logging"
msgstr ""

#: ../../topics/logging.txt:122
# 2be4c2a1944b44ebbc8cdea580cec98b
msgid "Once you have configured your loggers, handlers, filters and formatters, you need to place logging calls into your code. Using the logging framework is very simple. Here's an example::"
msgstr ""

#: ../../topics/logging.txt:138
# 2e1b8c8c31de4b2e8ec631f3be62162f
msgid "And that's it! Every time the ``bad_mojo`` condition is activated, an error log record will be written."
msgstr ""

#: ../../topics/logging.txt:142
# 90c08ffaa2984362b68407f54b2daaf5
msgid "Naming loggers"
msgstr ""

#: ../../topics/logging.txt:144
# 86c7f2ea776c4e5caec7883d59c9c38e
msgid "The call to :func:`logging.getLogger()` obtains (creating, if necessary) an instance of a logger. The logger instance is identified by a name. This name is used to identify the logger for configuration purposes."
msgstr ""

#: ../../topics/logging.txt:149
# 6e64a7e1877e4d15b6dbcbb8aed8e45f
msgid "By convention, the logger name is usually ``__name__``, the name of the python module that contains the logger. This allows you to filter and handle logging calls on a per-module basis. However, if you have some other way of organizing your logging messages, you can provide any dot-separated name to identify your logger::"
msgstr ""

#: ../../topics/logging.txt:158
# 320a3c6e59fe410ca737608215362870
msgid "The dotted paths of logger names define a hierarchy. The ``project.interesting`` logger is considered to be a parent of the ``project.interesting.stuff`` logger; the ``project`` logger is a parent of the ``project.interesting`` logger."
msgstr ""

#: ../../topics/logging.txt:163
# 655626b712da4e559ad25dd431d41c42
msgid "Why is the hierarchy important? Well, because loggers can be set to *propagate* their logging calls to their parents. In this way, you can define a single set of handlers at the root of a logger tree, and capture all logging calls in the subtree of loggers. A logging handler defined in the ``project`` namespace will catch all logging messages issued on the ``project.interesting`` and ``project.interesting.stuff`` loggers."
msgstr ""

#: ../../topics/logging.txt:171
# 9027f43d09e74f14bc71c6c66b355ca3
msgid "This propagation can be controlled on a per-logger basis. If you don't want a particular logger to propagate to its parents, you can turn off this behavior."
msgstr ""

#: ../../topics/logging.txt:176
# fc64c30417844fc2b26ce1aa4d30c11d
msgid "Making logging calls"
msgstr ""

#: ../../topics/logging.txt:178
# dedc26023dce4d0e8761bc0338486311
msgid "The logger instance contains an entry method for each of the default log levels:"
msgstr ""

#: ../../topics/logging.txt:181
# e3d00ee8d4994bcb9ee6d5eb5f5fc7a4
msgid "``logger.debug()``"
msgstr ""

#: ../../topics/logging.txt:182
# 7034506960254276aa99b5d8a519e5fe
msgid "``logger.info()``"
msgstr ""

#: ../../topics/logging.txt:183
# 30f756b38dfd471e85c3c1a9cd7591d0
msgid "``logger.warning()``"
msgstr ""

#: ../../topics/logging.txt:184
# 9044fa5f35634ac9aec01a48ad782e3b
msgid "``logger.error()``"
msgstr ""

#: ../../topics/logging.txt:185
# 9b2da8b58cad4e1f92437780a1260f64
msgid "``logger.critical()``"
msgstr ""

#: ../../topics/logging.txt:187
# b641f73e7c4a4bf58696f0818fc665f5
msgid "There are two other logging calls available:"
msgstr ""

#: ../../topics/logging.txt:189
# 23307737da574eac844197a4ad52f808
msgid "``logger.log()``: Manually emits a logging message with a specific log level."
msgstr ""

#: ../../topics/logging.txt:192
# e8c40a7369f842ef91205aeb0badbb6a
msgid "``logger.exception()``: Creates an ``ERROR`` level logging message wrapping the current exception stack frame."
msgstr ""

#: ../../topics/logging.txt:198
# 053ed4432f7340e48efc468545753a9a
msgid "Configuring logging"
msgstr ""

#: ../../topics/logging.txt:200
# c24de65d431b45a483393b9720c4a07e
msgid "Of course, it isn't enough to just put logging calls into your code. You also need to configure the loggers, handlers, filters and formatters to ensure that logging output is output in a useful way."
msgstr ""

#: ../../topics/logging.txt:204
# c714a2b758644b35a6a66246810cf326
msgid "Python's logging library provides several techniques to configure logging, ranging from a programmatic interface to configuration files. By default, Django uses the `dictConfig format`_."
msgstr ""

#: ../../topics/logging.txt:208
# f82491000b5f4ce789f2289590e2e9cf
msgid "In order to configure logging, you use :setting:`LOGGING` to define a dictionary of logging settings. These settings describes the loggers, handlers, filters and formatters that you want in your logging setup, and the log levels and other properties that you want those components to have."
msgstr ""

#: ../../topics/logging.txt:214
# 2ea75a810a6e43459108ef1899948dbe
msgid "Prior to Django 1.5, the :setting:`LOGGING` setting always overwrote the :ref:`default Django logging configuration <default-logging-configuration>`. From Django 1.5 forward, it is possible to get the project's logging configuration merged with Django's defaults, hence you can decide if you want to add to, or replace the existing configuration."
msgstr ""

#: ../../topics/logging.txt:220
# 3d319f8d1d4a4ece81ad07a0241ca789
msgid "If the ``disable_existing_loggers`` key in the :setting:`LOGGING` dictConfig is set to ``True`` (which is the default) the default configuration is completely overridden. Alternatively you can redefine some or all of the loggers by setting ``disable_existing_loggers`` to ``False``."
msgstr ""

#: ../../topics/logging.txt:225
# 23cc3742d18c44d68a31e7707e57f14e
msgid "Logging is configured as part of the general Django ``setup()`` function. Therefore, you can be certain that loggers are always ready for use in your project code."
msgstr ""

#: ../../topics/logging.txt:232
# 212423cd89b84bedba2e86d26347e64d
msgid "Examples"
msgstr ""

#: ../../topics/logging.txt:234
# 98e8f9738304450eacb584bfdf87da7b
msgid "The full documentation for `dictConfig format`_ is the best source of information about logging configuration dictionaries. However, to give you a taste of what is possible, here are a couple examples."
msgstr ""

#: ../../topics/logging.txt:238
# 60fa416be4b148a68031d96671e554e6
msgid "First, here's a simple configuration which writes all request logging from the :ref:`django-request-logger` logger to a local file::"
msgstr ""

#: ../../topics/logging.txt:260
# 827fa9e3e55c42e49a33cd5ff29e8ddd
msgid "If you use this example, be sure to change the ``'filename'`` path to a location that's writable by the user that's running the Django application."
msgstr ""

#: ../../topics/logging.txt:263
# 38145cb8b9fe49a8985a3a32cc3fca50
msgid "Second, here's an example of a fairly complex logging setup, configured using :func:`logging.config.dictConfig`::"
msgstr ""

#: ../../topics/logging.txt:318
# 9de155d6d2da4c4fa15749124b2a487b
msgid "This logging configuration does the following things:"
msgstr ""

#: ../../topics/logging.txt:320
# a1e9c46f9d7e4c0e80522e3f6f09a89e
msgid "Identifies the configuration as being in 'dictConfig version 1' format. At present, this is the only dictConfig format version."
msgstr ""

#: ../../topics/logging.txt:323
# fd64a40486204c47af5e36ca3a1e0ffe
msgid "Disables all existing logging configurations."
msgstr ""

#: ../../topics/logging.txt:325
# 5b2cc60ec6264a25b21ead5a0d24f10b
msgid "Defines two formatters:"
msgstr ""

#: ../../topics/logging.txt:327
# 7dad48d0ab8d4e81852dca2a143b555b
msgid "``simple``, that just outputs the log level name (e.g., ``DEBUG``) and the log message."
msgstr ""

#: ../../topics/logging.txt:330
# 71a6cdc4c6bb417cabd791b3bb5f2f7b
msgid "The ``format`` string is a normal Python formatting string describing the details that are to be output on each logging line. The full list of detail that can be output can be found in the `formatter documentation`_."
msgstr ""

#: ../../topics/logging.txt:335
# 1346789c919142f7ba6ec9591e3c644f
msgid "``verbose``, that outputs the log level name, the log message, plus the time, process, thread and module that generate the log message."
msgstr ""

#: ../../topics/logging.txt:339
# 7d74a3d4081c45cf976f92de92118f3e
msgid "Defines one filter -- ``project.logging.SpecialFilter``, using the alias ``special``. If this filter required additional arguments at time of construction, they can be provided as additional keys in the filter configuration dictionary. In this case, the argument ``foo`` will be given a value of ``bar`` when instantiating the ``SpecialFilter``."
msgstr ""

#: ../../topics/logging.txt:346
# 08bace9678fb4faca06fb392d1bdf261
msgid "Defines three handlers:"
msgstr ""

#: ../../topics/logging.txt:348
# c07d0f5afef441e982e0678c5cbf89d7
msgid "``null``, a NullHandler, which will pass any ``DEBUG`` (or higher) message to ``/dev/null``."
msgstr ""

#: ../../topics/logging.txt:351
# 145ada3783f0481e93ffe8a8925880fd
msgid "``console``, a StreamHandler, which will print any ``DEBUG`` (or higher) message to stderr. This handler uses the ``simple`` output format."
msgstr ""

#: ../../topics/logging.txt:355
# 1a8db569ea99422a84cf63088adede61
msgid "``mail_admins``, an AdminEmailHandler, which will email any ``ERROR`` (or higher) message to the site admins. This handler uses the ``special`` filter."
msgstr ""

#: ../../topics/logging.txt:359
# fd6b3b5f08684a3fa10534fc2f014392
msgid "Configures three loggers:"
msgstr ""

#: ../../topics/logging.txt:361
# bdc0f5a287f34f509545fc70cdc4032d
msgid "``django``, which passes all messages at ``INFO`` or higher to the ``null`` handler."
msgstr ""

#: ../../topics/logging.txt:364
# 7a9fb9d06b054dc6a82c0fa3f5758c22
msgid "``django.request``, which passes all ``ERROR`` messages to the ``mail_admins`` handler. In addition, this logger is marked to *not* propagate messages. This means that log messages written to ``django.request`` will not be handled by the ``django`` logger."
msgstr ""

#: ../../topics/logging.txt:370
# 1647df868e3243308ebb01dd6acaea39
msgid "``myproject.custom``, which passes all messages at ``INFO`` or higher that also pass the ``special`` filter to two handlers -- the ``console``, and ``mail_admins``. This means that all ``INFO`` level messages (or higher) will be printed to the console; ``ERROR`` and ``CRITICAL`` messages will also be output via email."
msgstr ""

#: ../../topics/logging.txt:380
# 93d682f3deee4b7fa569b50a3ed67611
msgid "Custom logging configuration"
msgstr ""

#: ../../topics/logging.txt:382
# ea96e957dc6247f1900f241bf5f2a21a
msgid "If you don't want to use Python's dictConfig format to configure your logger, you can specify your own configuration scheme."
msgstr ""

#: ../../topics/logging.txt:385
# 7677aba116624ef980a86c33c08b3da8
msgid "The :setting:`LOGGING_CONFIG` setting defines the callable that will be used to configure Django's loggers. By default, it points at Python's :func:`logging.config.dictConfig()` function. However, if you want to use a different configuration process, you can use any other callable that takes a single argument. The contents of :setting:`LOGGING` will be provided as the value of that argument when logging is configured."
msgstr ""

#: ../../topics/logging.txt:393
# f0f9adac76c047879426e12bce64a376
msgid "Disabling logging configuration"
msgstr ""

#: ../../topics/logging.txt:395
# 027b859e98484f0ca94ff98c6211379a
msgid "If you don't want to configure logging at all (or you want to manually configure logging using your own approach), you can set :setting:`LOGGING_CONFIG` to ``None``. This will disable the configuration process."
msgstr ""

#: ../../topics/logging.txt:401
# 762e1c94712e44bda74853742e0625ab
msgid "Setting :setting:`LOGGING_CONFIG` to ``None`` only means that the configuration process is disabled, not logging itself. If you disable the configuration process, Django will still make logging calls, falling back to whatever default logging behavior is defined."
msgstr ""

#: ../../topics/logging.txt:408
# 1dffdacf39294b169ce3d63a418cb499
msgid "Django's logging extensions"
msgstr ""

#: ../../topics/logging.txt:410
# f9298b224c7f443d9230817b0c35248d
msgid "Django provides a number of utilities to handle the unique requirements of logging in Web server environment."
msgstr ""

#: ../../topics/logging.txt:416
# c9174ac35c8e4234ae59c46ed34f1fde
msgid "Django provides several built-in loggers."
msgstr ""

#: ../../topics/logging.txt:419
# 63ef25f208064080a789f0208275fae3
msgid "``django``"
msgstr ""

#: ../../topics/logging.txt:421
# 2c9b2707fc394b80b303f577e1547844
msgid "``django`` is the catch-all logger. No messages are posted directly to this logger."
msgstr ""

#: ../../topics/logging.txt:427
# 1a7eae171499459e9ec6b7474d661831
msgid "``django.request``"
msgstr ""

#: ../../topics/logging.txt:429
# 9b1336ee9a1d4b6283ae6c9b1446412d
msgid "Log messages related to the handling of requests. 5XX responses are raised as ``ERROR`` messages; 4XX responses are raised as ``WARNING`` messages."
msgstr ""

#: ../../topics/logging.txt:433
#: ../../topics/logging.txt:448
# bf4ab329c451489c854c39e853146c2d
# 20c8e44dfcba4f15b0e9a11e46b88aa0
msgid "Messages to this logger have the following extra context:"
msgstr ""

#: ../../topics/logging.txt:435
# aa091eada09d41bbbdfb4e09164e93b5
msgid "``status_code``: The HTTP response code associated with the request."
msgstr ""

#: ../../topics/logging.txt:438
# 7d96f8e10ae741f3bfa434e2189150f8
msgid "``request``: The request object that generated the logging message."
msgstr ""

#: ../../topics/logging.txt:442
# 470e6849d64a4c3f8c60227bc30c0c57
msgid "``django.db.backends``"
msgstr ""

#: ../../topics/logging.txt:444
# 48fff633457f4947a7c9354ec8b96b6e
msgid "Messages relating to the interaction of code with the database. For example, every application-level SQL statement executed by a request is logged at the ``DEBUG`` level to this logger."
msgstr ""

#: ../../topics/logging.txt:450
# 4139e41e52f14123b713cfc5c3a53db7
msgid "``duration``: The time taken to execute the SQL statement."
msgstr ""

#: ../../topics/logging.txt:451
# 0f71a02578a440caa15bcb0316482581
msgid "``sql``: The SQL statement that was executed."
msgstr ""

#: ../../topics/logging.txt:452
# ef14d54ef1a4418ebfd9852aa03a8b78
msgid "``params``: The parameters that were used in the SQL call."
msgstr ""

#: ../../topics/logging.txt:454
# 25de12f6906a49a48c196766279498f8
msgid "For performance reasons, SQL logging is only enabled when ``settings.DEBUG`` is set to ``True``, regardless of the logging level or handlers that are installed."
msgstr ""

#: ../../topics/logging.txt:458
# 92cf119519294706a3284972e7639d2c
msgid "This logging does not include framework-level initialization (e.g. ``SET TIMEZONE``) or transaction management queries (e.g. ``BEGIN``, ``COMMIT``, and ``ROLLBACK``). Turn on query logging in your database if you wish to view all database queries."
msgstr ""

#: ../../topics/logging.txt:464
# 1553f162e64e442abcdabe1475dd8a99
msgid "``django.security.*``"
msgstr ""

#: ../../topics/logging.txt:466
# 5ee0471a746a472881dca7c6e89d9746
msgid "The security loggers will receive messages on any occurrence of :exc:`~django.core.exceptions.SuspiciousOperation`. There is a sub-logger for each sub-type of SuspiciousOperation. The level of the log event depends on where the exception is handled.  Most occurrences are logged as a warning, while any ``SuspiciousOperation`` that reaches the WSGI handler will be logged as an error. For example, when an HTTP ``Host`` header is included in a request from a client that does not match :setting:`ALLOWED_HOSTS`, Django will return a 400 response, and an error message will be logged to the ``django.security.DisallowedHost`` logger."
msgstr ""

#: ../../topics/logging.txt:476
# 557a75b836214e3cb87f341843ba3e06
msgid "Only the parent ``django.security`` logger is configured by default, and all child loggers will propagate to the parent logger. The ``django.security`` logger is configured the same as the ``django.request`` logger, and any error events will be mailed to admins. Requests resulting in a 400 response due to a ``SuspiciousOperation`` will not be logged to the ``django.request`` logger, but only to the ``django.security`` logger."
msgstr ""

#: ../../topics/logging.txt:483
# 79fb796710f24098a2e6d01bf5fe1052
msgid "To silence a particular type of SuspiciousOperation, you can override that specific logger following this example:"
msgstr ""

#: ../../topics/logging.txt:496
# db981b00a635416fa32fae72ea9c815f
msgid "``django.db.backends.schema``"
msgstr ""

#: ../../topics/logging.txt:500
# 835059c0fc3343fbba51ef684680ce27
msgid "Logs the SQL queries that are executed during schema changes to the database by the :doc:`migrations framework </topics/migrations>`. Note that it won't log the queries executed by :class:`~django.db.migrations.operations.RunPython`."
msgstr ""

#: ../../topics/logging.txt:507
# 29d30863abf8456584bd841257cb5a67
msgid "Django provides one log handler in addition to those provided by the Python logging module."
msgstr ""

#: ../../topics/logging.txt:512
# 628d4c47707044b2a7f610455c642ddb
msgid "This handler sends an email to the site admins for each log message it receives."
msgstr ""

#: ../../topics/logging.txt:515
# c80ab934ba63477594ef06294a5c557e
msgid "If the log record contains a ``request`` attribute, the full details of the request will be included in the email."
msgstr ""

#: ../../topics/logging.txt:518
# 69b727d9728e49d686270927485f8d0c
msgid "If the log record contains stack trace information, that stack trace will be included in the email."
msgstr ""

#: ../../topics/logging.txt:521
# b0cf5a93f4bb4780887b059f82ad2227
msgid "The ``include_html`` argument of ``AdminEmailHandler`` is used to control whether the traceback email includes an HTML attachment containing the full content of the debug Web page that would have been produced if :setting:`DEBUG` were ``True``. To set this value in your configuration, include it in the handler definition for ``django.utils.log.AdminEmailHandler``, like this:"
msgstr ""

#: ../../topics/logging.txt:538
# 561dd131e3ec4c59aab46e54c136a0e2
msgid "Note that this HTML version of the email contains a full traceback, with names and values of local variables at each level of the stack, plus the values of your Django settings. This information is potentially very sensitive, and you may not want to send it over email. Consider using something such as `Sentry`_ to get the best of both worlds -- the rich information of full tracebacks plus the security of *not* sending the information over email. You may also explicitly designate certain sensitive information to be filtered out of error reports -- learn more on :ref:`Filtering error reports<filtering-error-reports>`."
msgstr ""

#: ../../topics/logging.txt:550
# 926d0251fec44149aa0b8ef5af0d67a8
msgid "By setting the ``email_backend`` argument of ``AdminEmailHandler``, the :ref:`email backend <topic-email-backends>` that is being used by the handler can be overridden, like this:"
msgstr ""

#: ../../topics/logging.txt:564
# c2820cec23724b12bc5ace95d6035780
msgid "By default, an instance of the email backend specified in :setting:`EMAIL_BACKEND` will be used."
msgstr ""

#: ../../topics/logging.txt:573
# 947aa2ad91ad4f038175739c64a45e90
msgid "Django provides two log filters in addition to those provided by the Python logging module."
msgstr ""

#: ../../topics/logging.txt:578
# 540a59b11d6e459ea2897ff06260c29d
msgid "This filter accepts a callback function (which should accept a single argument, the record to be logged), and calls it for each record that passes through the filter. Handling of that record will not proceed if the callback returns False."
msgstr ""

#: ../../topics/logging.txt:583
# cf0169757a73424aa7b2e87bf8675432
msgid "For instance, to filter out :exc:`~django.http.UnreadablePostError` (raised when a user cancels an upload) from the admin emails, you would create a filter function::"
msgstr ""

#: ../../topics/logging.txt:596
# fd37f8586a4e4247b0b692136fc005ef
msgid "and then add it to your logging config:"
msgstr ""

#: ../../topics/logging.txt:616
# 7ed1de9adfea4512bf3c472391a76849
msgid "This filter will only pass on records when settings.DEBUG is False."
msgstr ""

#: ../../topics/logging.txt:618
# db14b54737bf442eb717024126c6e152
msgid "This filter is used as follows in the default :setting:`LOGGING` configuration to ensure that the :class:`AdminEmailHandler` only sends error emails to admins when :setting:`DEBUG` is ``False``:"
msgstr ""

#: ../../topics/logging.txt:639
# 2f379d2bf35944d5ac7fae52ef785234
msgid "This filter is similar to :class:`RequireDebugFalse`, except that records are passed only when :setting:`DEBUG` is ``True``."
msgstr ""

#: ../../topics/logging.txt:645
# bbed325ed0894c24b7afd8b04466d06b
msgid "Django's default logging configuration"
msgstr ""

#: ../../topics/logging.txt:647
# 37293d953fe547688e3c7a9c8f2852e9
msgid "By default, Django configures the ``django.request`` logger so that all messages with ``ERROR`` or ``CRITICAL`` level are sent to :class:`AdminEmailHandler`, as long as the :setting:`DEBUG` setting is set to ``False``."
msgstr ""

#: ../../topics/logging.txt:651
# 8a747ddaa1854339a426248408f44b89
msgid "All messages reaching the ``django`` catch-all logger when :setting:`DEBUG` is ``True`` are sent to the console. They are simply discarded (sent to ``NullHandler``) when :setting:`DEBUG` is ``False``."
msgstr ""

#: ../../topics/logging.txt:655
# 3754136375794568a5e28b83e178906b
msgid "See also :ref:`Configuring logging <configuring-logging>` to learn how you can complement or replace this default logging configuration."
msgstr ""

