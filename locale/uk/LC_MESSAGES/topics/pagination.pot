# SOME DESCRIPTIVE TITLE.
# Copyright (C) Django Software Foundation and contributors
# This file is distributed under the same license as the Django package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: Django 1.7\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2014-10-13 18:33+0300\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#: ../../topics/pagination.txt:3
# 3f61eefe77fa4f9c90a62e718ba1a245
msgid "Pagination"
msgstr ""

#: ../../topics/pagination.txt:8
# 34159d7b8cbb4d2ca5757bf3f684679c
msgid "Django provides a few classes that help you manage paginated data -- that is, data that's split across several pages, with \"Previous/Next\" links. These classes live in :file:`django/core/paginator.py`."
msgstr ""

#: ../../topics/pagination.txt:13
# 40c552cf45e145d3b03e5b0e0ac77491
msgid "Example"
msgstr ""

#: ../../topics/pagination.txt:15
# b0fa18c8d0af49d6ae437df650e3691c
msgid "Give :class:`Paginator` a list of objects, plus the number of items you'd like to have on each page, and it gives you methods for accessing the items for each page::"
msgstr ""

#: ../../topics/pagination.txt:67
# 1491fa48256e4333ac50ae8542c75a2e
msgid "Note that you can give ``Paginator`` a list/tuple, a Django ``QuerySet``, or any other object with a ``count()`` or ``__len__()`` method. When determining the number of objects contained in the passed object, ``Paginator`` will first try calling ``count()``, then fallback to using ``len()`` if the passed object has no ``count()`` method. This allows objects such as Django's ``QuerySet`` to use a more efficient ``count()`` method when available."
msgstr ""

#: ../../topics/pagination.txt:77
# 9a9c95e958ee42b39b41fc56d347984e
msgid "Using ``Paginator`` in a view"
msgstr ""

#: ../../topics/pagination.txt:79
# 8dfd8732cb334a7faf3144b951eb30a8
msgid "Here's a slightly more complex example using :class:`Paginator` in a view to paginate a queryset. We give both the view and the accompanying template to show how you can display the results. This example assumes you have a ``Contacts`` model that has already been imported."
msgstr ""

#: ../../topics/pagination.txt:84
# 4ff520b60e3e4a309d4ea88de11ef276
msgid "The view function looks like this::"
msgstr ""

#: ../../topics/pagination.txt:104
# eaed6c6450cc483d8026e7f3fdb8f3f7
msgid "In the template :file:`list.html`, you'll want to include navigation between pages along with any interesting information from the objects themselves::"
msgstr ""

#: ../../topics/pagination.txt:130
# cf789ac33968483797823ef5b986da0b
msgid "``Paginator`` objects"
msgstr ""

#: ../../topics/pagination.txt:132
# 6bf14da04c6e459395ad5cae2b87ddef
msgid "The :class:`Paginator` class has this constructor:"
msgstr ""

#: ../../topics/pagination.txt:137
# f17e4194576d45d796c7f3155bc18fbb
msgid "Required arguments"
msgstr ""

#: ../../topics/pagination.txt:141
# ac1566cd504d4cab99b6aa6965674f2c
msgid "``object_list``"
msgstr ""

#: ../../topics/pagination.txt:140
# 534e86525b474496b11e36474f1ef4a7
msgid "A list, tuple, Django ``QuerySet``, or other sliceable object with a ``count()`` or ``__len__()`` method."
msgstr ""

#: ../../topics/pagination.txt:145
# 9745e74085a848c9970531c53b0c35f1
msgid "``per_page``"
msgstr ""

#: ../../topics/pagination.txt:144
# 589e499ce60847d18844d52259e8d2e0
msgid "The maximum number of items to include on a page, not including orphans (see the ``orphans`` optional argument below)."
msgstr ""

#: ../../topics/pagination.txt:148
# 5bd2b33f5c504dcab597e9a623b9d413
msgid "Optional arguments"
msgstr ""

#: ../../topics/pagination.txt:158
# 477198a7383440a3a955a322ce0c6137
msgid "``orphans``"
msgstr ""

#: ../../topics/pagination.txt:151
# 83d08a786d9f436fbb5b437f8c78b21e
msgid "The minimum number of items allowed on the last page, defaults to zero. Use this when you don't want to have a last page with very few items. If the last page would normally have a number of items less than or equal to ``orphans``, then those items will be added to the previous page (which becomes the last page) instead of leaving the items on a page by themselves. For example, with 23 items, ``per_page=10``, and ``orphans=3``, there will be two pages; the first page with 10 items and the  second (and last) page with 13 items."
msgstr ""

#: ../../topics/pagination.txt:162
# ce4c8024376c4f788d65286e007c7291
msgid "``allow_empty_first_page``"
msgstr ""

#: ../../topics/pagination.txt:161
# 16a6547b45d6493288d4400f3d632a3b
msgid "Whether or not the first page is allowed to be empty.  If ``False`` and ``object_list`` is  empty, then an ``EmptyPage`` error will be raised."
msgstr ""

#: ../../topics/pagination.txt:165
#: ../../topics/pagination.txt:235
# 897701227e6e4f5c9b7972b69286e8be
# 8dbee02a56a443018577f3b0d2c35dd0
msgid "Methods"
msgstr ""

#: ../../topics/pagination.txt:169
# a6bbd295754c4b22832942b9aa5d8cf0
msgid "Returns a :class:`Page` object with the given 1-based index. Raises :exc:`InvalidPage` if the given page number doesn't exist."
msgstr ""

#: ../../topics/pagination.txt:173
#: ../../topics/pagination.txt:274
# cccdea950eac4bb2bb3b4dafaedcff77
# 1901fb9d4eaf4adeac13aca0fd019491
msgid "Attributes"
msgstr ""

#: ../../topics/pagination.txt:177
# 208fff91af4442c0b21dd266ed7124a8
msgid "The total number of objects, across all pages."
msgstr ""

#: ../../topics/pagination.txt:181
# 0aa809e05e3d4776bcc42bbe4977830c
msgid "When determining the number of objects contained in ``object_list``, ``Paginator`` will first try calling ``object_list.count()``. If ``object_list`` has no ``count()`` method, then ``Paginator`` will fallback to using ``len(object_list)``. This allows objects, such as Django's ``QuerySet``, to use a more efficient ``count()`` method when available."
msgstr ""

#: ../../topics/pagination.txt:190
# b8a1fcc56238432c8c9e4f37c7fc2975
msgid "The total number of pages."
msgstr ""

#: ../../topics/pagination.txt:194
# ebb488620973408696326283e6ee4701
msgid "A 1-based range of page numbers, e.g., ``[1, 2, 3, 4]``."
msgstr ""

#: ../../topics/pagination.txt:198
# 99b0f018576344d2b8776b26ac4ac336
msgid "``InvalidPage`` exceptions"
msgstr ""

#: ../../topics/pagination.txt:202
# dfe966e866194376aff252de3030be6f
msgid "A base class for exceptions raised when a paginator is passed an invalid page number."
msgstr ""

#: ../../topics/pagination.txt:205
# 062f59755ae44b45bf7c62e792208944
msgid "The :meth:`Paginator.page` method raises an exception if the requested page is invalid (i.e., not an integer) or contains no objects. Generally, it's enough to trap the ``InvalidPage`` exception, but if you'd like more granularity, you can trap either of the following exceptions:"
msgstr ""

#: ../../topics/pagination.txt:212
# ffd10715d50746f884b632f6a48faef2
msgid "Raised when ``page()`` is given a value that isn't an integer."
msgstr ""

#: ../../topics/pagination.txt:216
# 051aead844d246e0a0dbac0af1fb5b7b
msgid "Raised when ``page()`` is given a valid value but no objects exist on that page."
msgstr ""

#: ../../topics/pagination.txt:219
# c3ca5880123746d181a3a3b9c4bf222f
msgid "Both of the exceptions are subclasses of :exc:`InvalidPage`, so you can handle them both with a simple ``except InvalidPage``."
msgstr ""

#: ../../topics/pagination.txt:224
# 41c12296865747f4bdefcbf81a62744f
msgid "``Page`` objects"
msgstr ""

#: ../../topics/pagination.txt:226
# 43e0221472ff4784b25c289a5da8dea7
msgid "You usually won't construct ``Page`` objects by hand -- you'll get them using :meth:`Paginator.page`."
msgstr ""

#: ../../topics/pagination.txt:231
# 7b47ecc373bc4baa8ceb8bf6050c7b1d
msgid "A page acts like a sequence of :attr:`Page.object_list` when using ``len()`` or iterating it directly."
msgstr ""

#: ../../topics/pagination.txt:239
# d0500e7690b5486a975f01b02ff038d3
msgid "Returns ``True`` if there's a next page."
msgstr ""

#: ../../topics/pagination.txt:243
# 24933028f69e4719a18c4f5409ea9289
msgid "Returns ``True`` if there's a previous page."
msgstr ""

#: ../../topics/pagination.txt:247
# 36fe2e6430184ced882789b6bf642946
msgid "Returns ``True`` if there's a next *or* previous page."
msgstr ""

#: ../../topics/pagination.txt:251
# a7865cb475a746b1803147c0251ac3ee
msgid "Returns the next page number. Raises :exc:`InvalidPage` if next page doesn't exist."
msgstr ""

#: ../../topics/pagination.txt:256
# 64932e7394404c3dbf50d0f44c350908
msgid "Returns the previous page number. Raises :exc:`InvalidPage` if previous page doesn't exist."
msgstr ""

#: ../../topics/pagination.txt:261
# 2b4a49ee2b87492aa27e76b0c7e4d424
msgid "Returns the 1-based index of the first object on the page, relative to all of the objects in the paginator's list. For example, when paginating a list of 5 objects with 2 objects per page, the second page's :meth:`~Page.start_index` would return ``3``."
msgstr ""

#: ../../topics/pagination.txt:268
# c00eb8fe191743019c2fdfd574a5eec1
msgid "Returns the 1-based index of the last object on the page, relative to all of the objects in the paginator's list. For example, when paginating a list of 5 objects with 2 objects per page, the second page's :meth:`~Page.end_index` would return ``4``."
msgstr ""

#: ../../topics/pagination.txt:278
# 6561f392e2834810b1746c716a2f85f8
msgid "The list of objects on this page."
msgstr ""

#: ../../topics/pagination.txt:282
# 819bd7da854d46b8b1b0c95e63b2a426
msgid "The 1-based page number for this page."
msgstr ""

#: ../../topics/pagination.txt:286
# a6bc3509133245448a1971f662af8c27
msgid "The associated :class:`Paginator` object."
msgstr ""

