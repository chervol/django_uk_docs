# SOME DESCRIPTIVE TITLE.
# Copyright (C) Django Software Foundation and contributors
# This file is distributed under the same license as the Django package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: Django 1.7\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2014-10-13 18:33+0300\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#: ../../topics/localflavor.txt:3
# a9332720a4854a8ca0eee590a3c912fa
msgid "The \"local flavor\" add-ons"
msgstr ""

#: ../../topics/localflavor.txt:5
# 396af7d1e6e943e9aa5b95cb540c54e2
msgid "Historically, Django has shipped with ``django.contrib.localflavor`` -- assorted pieces of code that are useful for particular countries or cultures. This code is now distributed separately from Django, for easier maintenance and to trim the size of Django's codebase."
msgstr ""

#: ../../topics/localflavor.txt:10
# b439413562774cd5b971954c943219e7
msgid "The new localflavor package is named ``django-localflavor``, with a main module called ``localflavor`` and many subpackages using an `ISO 3166 country code`_. For example: ``localflavor.us`` is the localflavor package for the U.S.A."
msgstr ""

#: ../../topics/localflavor.txt:15
# bde5d260726d4d0cad7b02ef242eeb48
msgid "Most of these ``localflavor`` add-ons are country-specific fields for the :doc:`forms </topics/forms/index>` framework -- for example, a ``USStateField`` that knows how to validate U.S. state abbreviations and a ``FISocialSecurityNumber`` that knows how to validate Finnish social security numbers."
msgstr ""

#: ../../topics/localflavor.txt:21
# d1a1a7252b904cc89cba6d64557bbd75
msgid "To use one of these localized components, just import the relevant subpackage. For example, here's how you can create a form with a field representing a French telephone number::"
msgstr ""

#: ../../topics/localflavor.txt:31
# 8807612f8f114cf2abbb9321b57c9ec5
msgid "For documentation on a given country's localflavor helpers, see its README file."
msgstr ""

#: ../../topics/localflavor.txt:39
# 203b8c3067da463989b98ea3f8be7c68
msgid "Supported countries"
msgstr ""

#: ../../topics/localflavor.txt:41
# 9a24b69309c14ddd995152aaf87f0f80
msgid "See the official documentation for more information:"
msgstr ""

#: ../../topics/localflavor.txt:43
# f85e66d7f4fa4f0b92da379f20a39998
msgid "https://django-localflavor.readthedocs.org/"
msgstr ""

#: ../../topics/localflavor.txt:46
# 9dddb68343514c1884e629a6694315cd
msgid "Internationalization of localflavors"
msgstr ""

#: ../../topics/localflavor.txt:48
# 7dcd8dd39f9a4518ac4fe2147d0ee4fa
msgid "To activate translations for the ``localflavor`` application, you must include the application's name in the :setting:`INSTALLED_APPS` setting, so the internationalization system can find the catalog, as explained in :ref:`how-django-discovers-translations`."
msgstr ""

#: ../../topics/localflavor.txt:56
# 8dcf603376df4d67b10a344bc50c2a53
msgid "How to migrate"
msgstr ""

#: ../../topics/localflavor.txt:58
# f73da00599944df38a3d44aa00804c90
msgid "If you've used the old ``django.contrib.localflavor`` package or one of the temporary ``django-localflavor-*`` releases, follow these two easy steps to update your code:"
msgstr ""

#: ../../topics/localflavor.txt:62
# c23f621c0a104e1aa9a5dc4fe020d8e9
msgid "Install the third-party ``django-localflavor`` package from PyPI."
msgstr ""

#: ../../topics/localflavor.txt:64
# 8cb1c2d227d349a29bd60d7a3e5ac11a
msgid "Change your app's import statements to reference the new package."
msgstr ""

#: ../../topics/localflavor.txt:66
# 0457c546993841c5808302b25891e5f3
msgid "For example, change this::"
msgstr ""

#: ../../topics/localflavor.txt:70
# 7408b1944452474a94170a8f56bcb992
msgid "...to this::"
msgstr ""

#: ../../topics/localflavor.txt:74
# e7de80f92a4f40de9680834f4ac601cf
msgid "The code in the new package is the same (it was copied directly from Django), so you don't have to worry about backwards compatibility in terms of functionality. Only the imports have changed."
msgstr ""

#: ../../topics/localflavor.txt:81
# 3c5f868936374751aa73819946d5163d
msgid "Deprecation policy"
msgstr ""

#: ../../topics/localflavor.txt:83
# fb7efe3168554800bfb8f6e9f351edfe
msgid "In Django 1.5, importing from ``django.contrib.localflavor`` will result in a ``DeprecationWarning``. This means your code will still work, but you should change it as soon as possible."
msgstr ""

#: ../../topics/localflavor.txt:87
# c401556fc8a541cf84ab9e9a6699f633
msgid "In Django 1.6, importing from ``django.contrib.localflavor`` will no longer work."
msgstr ""

