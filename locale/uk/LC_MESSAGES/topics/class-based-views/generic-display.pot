# SOME DESCRIPTIVE TITLE.
# Copyright (C) Django Software Foundation and contributors
# This file is distributed under the same license as the Django package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: Django 1.7\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2014-10-13 18:33+0300\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#: ../../topics/class-based-views/generic-display.txt:5
# 3eb0ff593c2e482ab3042aaf4027d74e
msgid "Built-in Class-based generic views"
msgstr ""

#: ../../topics/class-based-views/generic-display.txt:7
# cab663d89cf444c0a3593ade4170661c
msgid "Writing Web applications can be monotonous, because we repeat certain patterns again and again. Django tries to take away some of that monotony at the model and template layers, but Web developers also experience this boredom at the view level."
msgstr ""

#: ../../topics/class-based-views/generic-display.txt:12
# 5a1ad730eade4971b76681cb5c6a989f
msgid "Django's *generic views* were developed to ease that pain. They take certain common idioms and patterns found in view development and abstract them so that you can quickly write common views of data without having to write too much code."
msgstr ""

#: ../../topics/class-based-views/generic-display.txt:17
# 8d4aa57dc11742ca93655ed3c38bb5ff
msgid "We can recognize certain common tasks, like displaying a list of objects, and write code that displays a list of *any* object. Then the model in question can be passed as an extra argument to the URLconf."
msgstr ""

#: ../../topics/class-based-views/generic-display.txt:21
# 8011fc2f5ba2408fb7c38c6e190c32e8
msgid "Django ships with generic views to do the following:"
msgstr ""

#: ../../topics/class-based-views/generic-display.txt:23
# 6d8ce5d8ce114b45ba2efa16a0818569
msgid "Display list and detail pages for a single object. If we were creating an application to manage conferences then a ``TalkListView`` and a ``RegisteredUserListView`` would be examples of list views. A single talk page is an example of what we call a \"detail\" view."
msgstr ""

#: ../../topics/class-based-views/generic-display.txt:28
# 350a479c72c34d56a1d4cd5a98ecd2e4
msgid "Present date-based objects in year/month/day archive pages, associated detail, and \"latest\" pages."
msgstr ""

#: ../../topics/class-based-views/generic-display.txt:31
# 1357818e56de4ba2aecc8d6e2d9e5c34
msgid "Allow users to create, update, and delete objects -- with or without authorization."
msgstr ""

#: ../../topics/class-based-views/generic-display.txt:34
# 9c873af3037e47b897a50c43f64a26b3
msgid "Taken together, these views provide easy interfaces to perform the most common tasks developers encounter."
msgstr ""

#: ../../topics/class-based-views/generic-display.txt:39
# 3dcd6fe740914cf0aea4621f989212ba
msgid "Extending generic views"
msgstr ""

#: ../../topics/class-based-views/generic-display.txt:41
# 94e04862fd4f4036991afe3aca6fe35a
msgid "There's no question that using generic views can speed up development substantially. In most projects, however, there comes a moment when the generic views no longer suffice. Indeed, the most common question asked by new Django developers is how to make generic views handle a wider array of situations."
msgstr ""

#: ../../topics/class-based-views/generic-display.txt:47
# cbd229f8e13f49bd9e762fca9508c053
msgid "This is one of the reasons generic views were redesigned for the 1.3 release - previously, they were just view functions with a bewildering array of options; now, rather than passing in a large amount of configuration in the URLconf, the recommended way to extend generic views is to subclass them, and override their attributes or methods."
msgstr ""

#: ../../topics/class-based-views/generic-display.txt:53
# b3f31204c7d44dc9a67e2ebd449148ea
msgid "That said, generic views will have a limit. If you find you're struggling to implement your view as a subclass of a generic view, then you may find it more effective to write just the code you need, using your own class-based or functional views."
msgstr ""

#: ../../topics/class-based-views/generic-display.txt:58
# 3631b47c393e4ffbafa0c18d24daabc3
msgid "More examples of generic views are available in some third party applications, or you could write your own as needed."
msgstr ""

#: ../../topics/class-based-views/generic-display.txt:63
# 924eb19988754f9580866b35cc748c31
msgid "Generic views of objects"
msgstr ""

#: ../../topics/class-based-views/generic-display.txt:65
# 96f0a89658d3446cb1444b131e6be24e
msgid ":class:`~django.views.generic.base.TemplateView` certainly is useful, but Django's generic views really shine when it comes to presenting views of your database content. Because it's such a common task, Django comes with a handful of built-in generic views that make generating list and detail views of objects incredibly easy."
msgstr ""

#: ../../topics/class-based-views/generic-display.txt:71
# 7d8b453027a14910bf68165bf0c4d29d
msgid "Let's start by looking at some examples of showing a list of objects or an individual object."
msgstr ""

#: ../../topics/class-based-views/generic-display.txt:76
# 9f5684c2bccb4241bd9563d5c8c3bb14
msgid "We'll be using these models::"
msgstr ""

#: ../../topics/class-based-views/generic-display.txt:110
# 653b694113c74984ab02d5ee9fc7b41d
msgid "Now we need to define a view::"
msgstr ""

#: ../../topics/class-based-views/generic-display.txt:119
# fa5eb91801024c468d5d781c648be563
msgid "Finally hook that view into your urls::"
msgstr ""

#: ../../topics/class-based-views/generic-display.txt:129
# 86398f28cc934788a1c0e779d925bd58
msgid "That's all the Python code we need to write. We still need to write a template, however. We could explicitly tell the view which template to use by adding a ``template_name`` attribute to the view, but in the absence of an explicit template Django will infer one from the object's name. In this case, the inferred template will be ``\"books/publisher_list.html\"`` -- the \"books\" part comes from the name of the app that defines the model, while the \"publisher\" bit is just the lowercased version of the model's name."
msgstr ""

#: ../../topics/class-based-views/generic-display.txt:139
# 4274a048cfd741e0987b54b35b7f0d49
msgid "Thus, when (for example) the :class:`django.template.loaders.app_directories.Loader` template loader is enabled in :setting:`TEMPLATE_LOADERS`, a template location could be: /path/to/project/books/templates/books/publisher_list.html"
msgstr ""

#: ../../topics/class-based-views/generic-display.txt:144
# 48d74bebecb1440883784b094e53c578
msgid "This template will be rendered against a context containing a variable called ``object_list`` that contains all the publisher objects. A very simple template might look like the following:"
msgstr ""

#: ../../topics/class-based-views/generic-display.txt:161
# 6bd95855aa4e40e785457d99e48b7db6
msgid "That's really all there is to it. All the cool features of generic views come from changing the attributes set on the generic view. The :doc:`generic views reference</ref/class-based-views/index>` documents all the generic views and their options in detail; the rest of this document will consider some of the common ways you might customize and extend generic views."
msgstr ""

#: ../../topics/class-based-views/generic-display.txt:169
# 20fe3b0c39754469a7444c93870d3023
msgid "Making \"friendly\" template contexts"
msgstr ""

#: ../../topics/class-based-views/generic-display.txt:171
# 1a7766e3bc2543078153be6b5653d03e
msgid "You might have noticed that our sample publisher list template stores all the publishers in a variable named ``object_list``. While this works just fine, it isn't all that \"friendly\" to template authors: they have to \"just know\" that they're dealing with publishers here."
msgstr ""

#: ../../topics/class-based-views/generic-display.txt:176
# 9867b465dc614799992821f30137106f
msgid "Well, if you're dealing with a model object, this is already done for you. When you are dealing with an object or queryset, Django is able to populate the context using the lower cased version of the model class' name. This is provided in addition to the default ``object_list`` entry, but contains exactly the same data, i.e. ``publisher_list``."
msgstr ""

#: ../../topics/class-based-views/generic-display.txt:182
# aae49a7a09864e85bf13e6938dc03cf5
msgid "If this still isn't a good match, you can manually set the name of the context variable. The ``context_object_name`` attribute on a generic view specifies the context variable to use::"
msgstr ""

#: ../../topics/class-based-views/generic-display.txt:194
# 3d80b7941ae44d479b87599519cd489d
msgid "Providing a useful ``context_object_name`` is always a good idea. Your coworkers who design templates will thank you."
msgstr ""

#: ../../topics/class-based-views/generic-display.txt:201
# 56dc2b43e53f49678c016fdb1706ebc0
msgid "Adding extra context"
msgstr ""

#: ../../topics/class-based-views/generic-display.txt:203
# 9a20e88d072e4f0789eff64b0af233db
msgid "Often you simply need to present some extra information beyond that provided by the generic view. For example, think of showing a list of all the books on each publisher detail page. The :class:`~django.views.generic.detail.DetailView` generic view provides the publisher to the context, but how do we get additional information in that template?"
msgstr ""

#: ../../topics/class-based-views/generic-display.txt:210
# e60948ab2972431e93cdd7776ebb267a
msgid "The answer is to subclass :class:`~django.views.generic.detail.DetailView` and provide your own implementation of the ``get_context_data`` method. The default implementation simply adds the object being displayed to the template, but you can override it to send more::"
msgstr ""

#: ../../topics/class-based-views/generic-display.txt:231
# d2df3c8bc29041308a459cb67f549649
msgid "Generally, ``get_context_data`` will merge the context data of all parent classes with those of the current class. To preserve this behavior in your own classes where you want to alter the context, you should be sure to call ``get_context_data`` on the super class. When no two classes try to define the same key, this will give the expected results. However if any class attempts to override a key after parent classes have set it (after the call to super), any children of that class will also need to explicitly set it after super if they want to be sure to override all parents. If you're having trouble, review the method resolution order of your view."
msgstr ""

#: ../../topics/class-based-views/generic-display.txt:244
# 8bf96797c88e4b37adbe3c173dd85c11
msgid "Viewing subsets of objects"
msgstr ""

#: ../../topics/class-based-views/generic-display.txt:246
# 97dc5d4126d44551994086a5c1bd61e5
msgid "Now let's take a closer look at the ``model`` argument we've been using all along. The ``model`` argument, which specifies the database model that the view will operate upon, is available on all the generic views that operate on a single object or a collection of objects. However, the ``model`` argument is not the only way to specify the objects that the view will operate upon -- you can also specify the list of objects using the ``queryset`` argument::"
msgstr ""

#: ../../topics/class-based-views/generic-display.txt:262
# c934bf604469482db28759f6b6b9b699
msgid "Specifying ``model = Publisher`` is really just shorthand for saying ``queryset = Publisher.objects.all()``. However, by using ``queryset`` to define a filtered list of objects you can be more specific about the objects that will be visible in the view (see :doc:`/topics/db/queries` for more information about :class:`~django.db.models.query.QuerySet` objects, and see the :doc:`class-based views reference </ref/class-based-views/index>` for the complete details)."
msgstr ""

#: ../../topics/class-based-views/generic-display.txt:270
# ec8560f2a8b94c4cba0f70747ee14122
msgid "To pick a simple example, we might want to order a list of books by publication date, with the most recent first::"
msgstr ""

#: ../../topics/class-based-views/generic-display.txt:280
# dcbf1be3bd6b4ada9e5810b0dcf1c8f1
msgid "That's a pretty simple example, but it illustrates the idea nicely. Of course, you'll usually want to do more than just reorder objects. If you want to present a list of books by a particular publisher, you can use the same technique::"
msgstr ""

#: ../../topics/class-based-views/generic-display.txt:294
# dfb23af1d51f4e45a2618b1bd2829c65
msgid "Notice that along with a filtered ``queryset``, we're also using a custom template name. If we didn't, the generic view would use the same template as the \"vanilla\" object list, which might not be what we want."
msgstr ""

#: ../../topics/class-based-views/generic-display.txt:298
# 795ee077ba2249a0b743590239013077
msgid "Also notice that this isn't a very elegant way of doing publisher-specific books. If we want to add another publisher page, we'd need another handful of lines in the URLconf, and more than a few publishers would get unreasonable. We'll deal with this problem in the next section."
msgstr ""

#: ../../topics/class-based-views/generic-display.txt:305
# 3148c6787f1547ef9b5f0d5d1ae7823f
msgid "If you get a 404 when requesting ``/books/acme/``, check to ensure you actually have a Publisher with the name 'ACME Publishing'.  Generic views have an ``allow_empty`` parameter for this case.  See the :doc:`class-based-views reference</ref/class-based-views/index>` for more details."
msgstr ""

#: ../../topics/class-based-views/generic-display.txt:313
# f253512118bb478fb77eb0cad8ef5508
msgid "Dynamic filtering"
msgstr ""

#: ../../topics/class-based-views/generic-display.txt:315
# 477c076be73145989afa56a5bc9c5500
msgid "Another common need is to filter down the objects given in a list page by some key in the URL. Earlier we hard-coded the publisher's name in the URLconf, but what if we wanted to write a view that displayed all the books by some arbitrary publisher?"
msgstr ""

#: ../../topics/class-based-views/generic-display.txt:320
# 4a3ed84c9b864bfdaa480c1afb6ba114
msgid "Handily, the ``ListView`` has a :meth:`~django.views.generic.list.MultipleObjectMixin.get_queryset` method we can override. Previously, it has just been returning the value of the ``queryset`` attribute, but now we can add more logic."
msgstr ""

#: ../../topics/class-based-views/generic-display.txt:325
# fc4facc4c3b24c41a7a223ead55f2db1
msgid "The key part to making this work is that when class-based views are called, various useful things are stored on ``self``; as well as the request (``self.request``) this includes the positional (``self.args``) and name-based (``self.kwargs``) arguments captured according to the URLconf."
msgstr ""

#: ../../topics/class-based-views/generic-display.txt:330
# b89c5c3706a245a7ab2d647bd2bf63bb
msgid "Here, we have a URLconf with a single captured group::"
msgstr ""

#: ../../topics/class-based-views/generic-display.txt:340
# bcc60fcfc023467ea55c694e876a716c
msgid "Next, we'll write the ``PublisherBookList`` view itself::"
msgstr ""

#: ../../topics/class-based-views/generic-display.txt:355
# 8876ffc4172b4d4a996214ba5493cc58
msgid "As you can see, it's quite easy to add more logic to the queryset selection; if we wanted, we could use ``self.request.user`` to filter using the current user, or other more complex logic."
msgstr ""

#: ../../topics/class-based-views/generic-display.txt:359
# 0fe0e2cbfebf4b34a44dab1fce464a06
msgid "We can also add the publisher into the context at the same time, so we can use it in the template::"
msgstr ""

#: ../../topics/class-based-views/generic-display.txt:374
# 8ecc215d21ed4d6b80f92d268b59e330
msgid "Performing extra work"
msgstr ""

#: ../../topics/class-based-views/generic-display.txt:376
# 6bf65f9d1c74465ab615429bc9d9b2d4
msgid "The last common pattern we'll look at involves doing some extra work before or after calling the generic view."
msgstr ""

#: ../../topics/class-based-views/generic-display.txt:379
# efe1737c320d4e73aff2e654e3e40d79
msgid "Imagine we had a ``last_accessed`` field on our ``Author`` model that we were using to keep track of the last time anybody looked at that author::"
msgstr ""

#: ../../topics/class-based-views/generic-display.txt:392
# 579307ce75f945e3bf9d714afac791ed
msgid "The generic ``DetailView`` class, of course, wouldn't know anything about this field, but once again we could easily write a custom view to keep that field updated."
msgstr ""

#: ../../topics/class-based-views/generic-display.txt:396
# 6e101cae008b40ef8b9a15afaffa195c
msgid "First, we'd need to add an author detail bit in the URLconf to point to a custom view::"
msgstr ""

#: ../../topics/class-based-views/generic-display.txt:407
# 28c7db6b64274f80af3085690a444dcc
msgid "Then we'd write our new view -- ``get_object`` is the method that retrieves the object -- so we simply override it and wrap the call::"
msgstr ""

#: ../../topics/class-based-views/generic-display.txt:429
# e2aafa8f1d0c4a68b3ed224f7e3db0a5
msgid "The URLconf here uses the named group ``pk`` - this name is the default name that ``DetailView`` uses to find the value of the primary key used to filter the queryset."
msgstr ""

#: ../../topics/class-based-views/generic-display.txt:433
# 3d29a3d034c544c1b8e1b887cd5aa7f8
msgid "If you want to call the group something else, you can set ``pk_url_kwarg`` on the view. More details can be found in the reference for :class:`~django.views.generic.detail.DetailView`"
msgstr ""

