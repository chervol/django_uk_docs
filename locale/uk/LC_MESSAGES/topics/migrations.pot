# SOME DESCRIPTIVE TITLE.
# Copyright (C) Django Software Foundation and contributors
# This file is distributed under the same license as the Django package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: Django 1.7\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2014-10-13 18:33+0300\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#: ../../topics/migrations.txt:3
# 2f2f6f6949e34d70ac5dd234cb09c91f
msgid "Migrations"
msgstr ""

#: ../../topics/migrations.txt:10
# a5cb994418fd4a64936e50b8c70d96ab
msgid "Migrations are Django's way of propagating changes you make to your models (adding a field, deleting a model, etc.) into your database schema. They're designed to be mostly automatic, but you'll need to know when to make migrations, when to run them, and the common problems you might run into."
msgstr ""

#: ../../topics/migrations.txt:16
# d26733949da249db99d100e227a235c1
msgid "A Brief History"
msgstr ""

#: ../../topics/migrations.txt:18
# 519e0c46ecfe4a35a9cc40b279e06837
msgid "Prior to version 1.7, Django only supported adding new models to the database; it was not possible to alter or remove existing models via the ``syncdb`` command (the predecessor to :djadmin:`migrate`)."
msgstr ""

#: ../../topics/migrations.txt:22
# be75b9b515af4b03b9bc4e2971442d4c
msgid "Third-party tools, most notably `South <http://south.aeracode.org>`_, provided support for these additional types of change, but it was considered important enough that support was brought into core Django."
msgstr ""

#: ../../topics/migrations.txt:27
# d4e8af500515452ca96f75aa40b9976d
msgid "Two Commands"
msgstr ""

#: ../../topics/migrations.txt:29
# 7a46cda705fc4ed6bfb3f91a83f5cb33
msgid "There are several commands which you will use to interact with migrations and Django's handling of database schema:"
msgstr ""

#: ../../topics/migrations.txt:32
# 99f0b2473ef54771bbbfbd8e36446f3f
msgid ":djadmin:`migrate`, which is responsible for applying migrations, as well as unapplying and listing their status."
msgstr ""

#: ../../topics/migrations.txt:35
# dde02f3566e74cd290296151ef21c09b
msgid ":djadmin:`makemigrations`, which is responsible for creating new migrations based on the changes you have made to your models."
msgstr ""

#: ../../topics/migrations.txt:38
# cda7fb37b8d14ffca261aa01607f27c9
msgid ":djadmin:`sqlmigrate`, which displays the SQL statements for a migration."
msgstr ""

#: ../../topics/migrations.txt:40
# 57a4f9fdedf448d3adc11fc8b6b09986
msgid "It's worth noting that migrations are created and run on a per-app basis. In particular, it's possible to have apps that *do not use migrations* (these are referred to as \"unmigrated\" apps) - these apps will instead mimic the legacy behavior of just adding new models."
msgstr ""

#: ../../topics/migrations.txt:45
# 779ce9a7bb234b4e9d5205f1e91fa355
msgid "You should think of migrations as a version control system for your database schema. ``makemigrations`` is responsible for packaging up your model changes into individual migration files - analogous to commits - and ``migrate`` is responsible for applying those to your database."
msgstr ""

#: ../../topics/migrations.txt:50
# 21200e6d2c3640cca523922cdf0a507d
msgid "The migration files for each app live in a \"migrations\" directory inside of that app, and are designed to be committed to, and distributed as part of, its codebase. You should be making them once on your development machine and then running the same migrations on your colleagues' machines, your staging machines, and eventually your production machines."
msgstr ""

#: ../../topics/migrations.txt:57
# 60c3bd46ace74712bd05159ffad34a91
msgid "It is possible to override the name of the package which contains the migrations on a per-app basis by modifying the :setting:`MIGRATION_MODULES` setting."
msgstr ""

#: ../../topics/migrations.txt:61
# c0b308efd1e44699bc57afe09efc1a88
msgid "Migrations will run the same way on the same dataset and produce consistent results, meaning that what you see in development and staging is, under the same circumstances, exactly what will happen in production."
msgstr ""

#: ../../topics/migrations.txt:65
# f4ac72e1cc96469fb6fd3d511f1d41bd
msgid "Django will make migrations for any change to your models or fields - even options that don't affect the database - as the only way it can reconstruct a field correctly is to have all the changes in the history, and you might need those options in some data migrations later on (for example, if you've set custom validators)."
msgstr ""

#: ../../topics/migrations.txt:72
# 4012f515a3cf4c84a12cdf235d417816
msgid "Backend Support"
msgstr ""

#: ../../topics/migrations.txt:74
# f9c48c6a062041c5a1681c9dd8a657e0
msgid "Migrations are supported on all backends that Django ships with, as well as any third-party backends if they have programmed in support for schema alteration (done via the :doc:`SchemaEditor </ref/schema-editor>` class)."
msgstr ""

#: ../../topics/migrations.txt:78
# db6c5a47d73e461a83e4013bd6a7fbd6
msgid "However, some databases are more capable than others when it comes to schema migrations; some of the caveats are covered below."
msgstr ""

#: ../../topics/migrations.txt:82
# 5e41a6c081a84953b440b69812a4cb29
msgid "PostgreSQL"
msgstr ""

#: ../../topics/migrations.txt:84
# 75835f5f090c4ffba06ede067e6c946e
msgid "PostgreSQL is the most capable of all the databases here in terms of schema support; the only caveat is that adding columns with default values will cause a full rewrite of the table, for a time proportional to its size."
msgstr ""

#: ../../topics/migrations.txt:88
# 21c407e746924257a096408b2bfbc429
msgid "For this reason, it's recommended you always create new columns with ``null=True``, as this way they will be added immediately."
msgstr ""

#: ../../topics/migrations.txt:92
# fb47453b5b53408fa26f7dbbb8783074
msgid "MySQL"
msgstr ""

#: ../../topics/migrations.txt:94
# b62b4973db764ae08558e548c49846ba
msgid "MySQL lacks support for transactions around schema alteration operations, meaning that if a migration fails to apply you will have to manually unpick the changes in order to try again (it's impossible to roll back to an earlier point)."
msgstr ""

#: ../../topics/migrations.txt:99
# 045c6f8c2f9e4c82b8ee647b2ff5f7cf
msgid "In addition, MySQL will fully rewrite tables for almost every schema operation and generally takes a time proportional to the number of rows in the table to add or remove columns. On slower hardware this can be worse than a minute per million rows - adding a few columns to a table with just a few million rows could lock your site up for over ten minutes."
msgstr ""

#: ../../topics/migrations.txt:105
# ac0f7c80486749a0976285f50bfbe635
msgid "Finally, MySQL has reasonably small limits on name lengths for columns, tables and indexes, as well as a limit on the combined size of all columns an index covers. This means that indexes that are possible on other backends will fail to be created under MySQL."
msgstr ""

#: ../../topics/migrations.txt:111
# 1d1380dfc7984bf095e30ac8a9f4a09d
msgid "SQLite"
msgstr ""

#: ../../topics/migrations.txt:113
# 69aa8019a6204124bb28d57718028c76
msgid "SQLite has very little built-in schema alteration support, and so Django attempts to emulate it by:"
msgstr ""

#: ../../topics/migrations.txt:116
# 28ccdc81f980478c93fc006788c24b25
msgid "Creating a new table with the new schema"
msgstr ""

#: ../../topics/migrations.txt:117
# aa5f83c7da1548beae286c1604677be6
msgid "Copying the data across"
msgstr ""

#: ../../topics/migrations.txt:118
# 3fea058fb0894ea9aa92c84722d4a395
msgid "Dropping the old table"
msgstr ""

#: ../../topics/migrations.txt:119
# 2110c0dbbd36468cbc1183f580b00b10
msgid "Renaming the new table to match the original name"
msgstr ""

#: ../../topics/migrations.txt:121
# 03c6cd79035347159e987eb3931346b5
msgid "This process generally works well, but it can be slow and occasionally buggy. It is not recommended that you run and migrate SQLite in a production environment unless you are very aware of the risks and its limitations; the support Django ships with is designed to allow developers to use SQLite on their local machines to develop less complex Django projects without the need for a full database."
msgstr ""

#: ../../topics/migrations.txt:129
# a6e16a8563cd41f8a4daaf21c7da6d40
msgid "Workflow"
msgstr ""

#: ../../topics/migrations.txt:131
# 3b91796ec60c4729ac4bbfae8ca5388b
msgid "Working with migrations is simple. Make changes to your models - say, add a field and remove a model - and then run :djadmin:`makemigrations`::"
msgstr ""

#: ../../topics/migrations.txt:139
# dc709db6d28a481caa49e6351753e1e1
msgid "Your models will be scanned and compared to the versions currently contained in your migration files, and then a new set of migrations will be written out. Make sure to read the output to see what ``makemigrations`` thinks you have changed - it's not perfect, and for complex changes it might not be detecting what you expect."
msgstr ""

#: ../../topics/migrations.txt:145
# c6a17a6e54d04c7fa6e3a10a3e55a7fa
msgid "Once you have your new migration files, you should apply them to your database to make sure they work as expected::"
msgstr ""

#: ../../topics/migrations.txt:160
# 25804912e37d4a9880e39f8b9c44dae8
msgid "The command runs in two stages; first, it synchronizes unmigrated apps (performing the same functionality that ``syncdb`` used to provide), and then it runs any migrations that have not yet been applied."
msgstr ""

#: ../../topics/migrations.txt:164
# 3945b04584b54d7a83cc15b85168f24e
msgid "Once the migration is applied, commit the migration and the models change to your version control system as a single commit - that way, when other developers (or your production servers) check out the code, they'll get both the changes to your models and the accompanying migration at the same time."
msgstr ""

#: ../../topics/migrations.txt:171
# 4e972ecbd2fb4815854bc554ee05ecd2
msgid "Version control"
msgstr ""

#: ../../topics/migrations.txt:173
# e2d217ae65234643ad4ac135b4c2282e
msgid "Because migrations are stored in version control, you'll occasionally come across situations where you and another developer have both committed a migration to the same app at the same time, resulting in two migrations with the same number."
msgstr ""

#: ../../topics/migrations.txt:178
# 1c183e5d63464f00af2a432a0d25291a
msgid "Don't worry - the numbers are just there for developers' reference, Django just cares that each migration has a different name. Migrations specify which other migrations they depend on - including earlier migrations in the same app - in the file, so it's possible to detect when there's two new migrations for the same app that aren't ordered."
msgstr ""

#: ../../topics/migrations.txt:184
# 0c746282be964cd68426a46991fd898c
msgid "When this happens, Django will prompt you and give you some options. If it thinks it's safe enough, it will offer to automatically linearize the two migrations for you. If not, you'll have to go in and modify the migrations yourself - don't worry, this isn't difficult, and is explained more in :ref:`migration-files` below."
msgstr ""

#: ../../topics/migrations.txt:191
# f7a2cee3c8f6437ca054cd15f8051bd5
msgid "Dependencies"
msgstr ""

#: ../../topics/migrations.txt:193
# fdf2920e21fc40f5a6f395d3b7cace85
msgid "While migrations are per-app, the tables and relationships implied by your models are too complex to be created for just one app at a time. When you make a migration that requires something else to run - for example, you add a ``ForeignKey`` in your ``books`` app to your ``authors`` app - the resulting migration will contain a dependency on a migration in ``authors``."
msgstr ""

#: ../../topics/migrations.txt:199
# 70f132d1b2c64f86b66a5ed6d497ca55
msgid "This means that when you run the migrations, the ``authors`` migration runs first and creates the table the ``ForeignKey`` references, and then the migration that makes the ``ForeignKey`` column runs afterwards and creates the constraint. If this didn't happen, the migration would try to create the ``ForeignKey`` column without the table it's referencing existing and your database would throw an error."
msgstr ""

#: ../../topics/migrations.txt:206
# 397a4aabf7db4ca28feeff00469d1ac0
msgid "This dependency behavior affects most migration operations where you restrict to a single app. Restricting to a single app (either in ``makemigrations`` or ``migrate``) is a best-efforts promise, and not a guarantee; any other apps that need to be used to get dependencies correct will be."
msgstr ""

#: ../../topics/migrations.txt:214
# 50279bd85db74bb58880a0f2a47f0e80
msgid "Be aware, however, that unmigrated apps cannot depend on migrated apps, by the very nature of not having migrations. This means that it is not generally possible to have an unmigrated app have a ForeignKey or ManyToManyField to a migrated app; some cases may work, but it will eventually fail."
msgstr ""

#: ../../topics/migrations.txt:219
# a4b2e43bc8b149d2b524daf4d613780e
msgid "This is particularly apparent if you use swappable models (e.g. ``AUTH_USER_MODEL``), as every app that uses swappable models will need to have migrations if you're unlucky. As time goes on, more and more third-party apps will get migrations, but in the meantime you can either give them migrations yourself (using :setting:`MIGRATION_MODULES` to store those modules outside of the app's own module if you wish), or keep the app with your user model unmigrated."
msgstr ""

#: ../../topics/migrations.txt:230
# ca03c4e70ff54e2abdb923456cf21410
msgid "Migration files"
msgstr ""

#: ../../topics/migrations.txt:232
# 598129bbc9554ee0a7c3fa344aa28c11
msgid "Migrations are stored as an on-disk format, referred to here as \"migration files\". These files are actually just normal Python files with an agreed-upon object layout, written in a declarative style."
msgstr ""

#: ../../topics/migrations.txt:236
# 160f81bece354450b31e149f3cc8b2fa
msgid "A basic migration file looks like this::"
msgstr ""

#: ../../topics/migrations.txt:249
# 99b8e901ad624d6dbddc023056279592
msgid "What Django looks for when it loads a migration file (as a Python module) is a subclass of ``django.db.migrations.Migration`` called ``Migration``. It then inspects this object for four attributes, only two of which are used most of the time:"
msgstr ""

#: ../../topics/migrations.txt:254
# 87c24abf0ce140fba28f052c97e7815a
msgid "``dependencies``, a list of migrations this one depends on."
msgstr ""

#: ../../topics/migrations.txt:255
# de76d893e41f42a5a27957d9f48d19ad
msgid "``operations``, a list of ``Operation`` classes that define what this migration does."
msgstr ""

#: ../../topics/migrations.txt:258
# d41313a6b8d34b31b7537dd54a9afb32
msgid "The operations are the key; they are a set of declarative instructions which tell Django what schema changes need to be made. Django scans them and builds an in-memory representation of all of the schema changes to all apps, and uses this to generate the SQL which makes the schema changes."
msgstr ""

#: ../../topics/migrations.txt:263
# 809098dad3a6437ebee19c43757a1ed8
msgid "That in-memory structure is also used to work out what the differences are between your models and the current state of your migrations; Django runs through all the changes, in order, on an in-memory set of models to come up with the state of your models last time you ran ``makemigrations``. It then uses these models to compare against the ones in your ``models.py`` files to work out what you have changed."
msgstr ""

#: ../../topics/migrations.txt:270
# 5fea2145e2d647e28c0ef87b9aad000e
msgid "You should rarely, if ever, need to edit migration files by hand, but it's entirely possible to write them manually if you need to. Some of the more complex operations are not autodetectable and are only available via a hand-written migration, so don't be scared about editing them if you have to."
msgstr ""

#: ../../topics/migrations.txt:276
# a23c02fce05d41fb8f65360dbd2c6da8
msgid "Custom fields"
msgstr ""

#: ../../topics/migrations.txt:278
# 87118839ebfe45e68d09a7d38179ff71
msgid "You can't modify the number of positional arguments in an already migrated custom field without raising a ``TypeError``. The old migration will call the modified ``__init__`` method with the old signature. So if you need a new argument, please create a keyword argument and add something like ``assert kwargs.get('argument_name') is not None`` in the constructor."
msgstr ""

#: ../../topics/migrations.txt:285
# 14b4738494154058bbf4b449d3ffcb03
msgid "Adding migrations to apps"
msgstr ""

#: ../../topics/migrations.txt:287
# 566bbeca3b8b47de8aa3af7d25133574
msgid "Adding migrations to new apps is straightforward - they come preconfigured to accept migrations, and so just run :djadmin:`makemigrations` once you've made some changes."
msgstr ""

#: ../../topics/migrations.txt:291
# 9d0e97686dbc476fb8841df5335511e9
msgid "If your app already has models and database tables, and doesn't have migrations yet (for example, you created it against a previous Django version), you'll need to convert it to use migrations; this is a simple process::"
msgstr ""

#: ../../topics/migrations.txt:297
# 8c5758b744f94a92b0b425f96c7825e4
msgid "This will make a new initial migration for your app. Now, when you run :djadmin:`migrate`, Django will detect that you have an initial migration *and* that the tables it wants to create already exist, and will mark the migration as already applied."
msgstr ""

#: ../../topics/migrations.txt:302
# 767bd58477754194b4bf181ba9ddbe0f
msgid "Note that this only works given two things:"
msgstr ""

#: ../../topics/migrations.txt:304
# 87cfcda192524cad8774d04e3a706ccb
msgid "You have not changed your models since you made their tables. For migrations to work, you must make the initial migration *first* and then make changes, as Django compares changes against migration files, not the database."
msgstr ""

#: ../../topics/migrations.txt:308
# f835fd24592f47a5b541977504d87fac
msgid "You have not manually edited your database - Django won't be able to detect that your database doesn't match your models, you'll just get errors when migrations try to modify those tables."
msgstr ""

#: ../../topics/migrations.txt:315
# ec67a65b35464f87881f6465509f8d80
msgid "Historical models"
msgstr ""

#: ../../topics/migrations.txt:317
# 1676681ab47f47808540a6076aafb319
msgid "When you run migrations, Django is working from historical versions of your models stored in the migration files. If you write Python code using the :class:`~django.db.migrations.operations.RunPython` operation, or if you have ``allow_migrate`` methods on your database routers, you will be exposed to these versions of your models."
msgstr ""

#: ../../topics/migrations.txt:323
# c5752797a63c4e5fbc054ec56a0f734f
msgid "Because it's impossible to serialize arbitrary Python code, these historical models will not have any custom methods or managers that you have defined. They will, however, have the same fields, relationships and ``Meta`` options (also versioned, so they may be different from your current ones)."
msgstr ""

#: ../../topics/migrations.txt:330
# 7324b99d6410409ebf7a94d5f95cf2e9
msgid "This means that you will NOT have custom ``save()`` methods called on objects when you access them in migrations, and you will NOT have any custom constructors or instance methods. Plan appropriately!"
msgstr ""

#: ../../topics/migrations.txt:334
# 39343342a7fa4ad08b1272be4bbe2b14
msgid "References to functions in field options such as ``upload_to`` and ``limit_choices_to`` are serialized in migrations, so the functions will need to be kept around for as long as there is a migration referencing them."
msgstr ""

#: ../../topics/migrations.txt:338
# 4d8afc3a0c4d43d6bd95187ecd232d4f
msgid "In addition, the base classes of the model are just stored as pointers, so you must always keep base classes around for as long as there is a migration that contains a reference to them. On the plus side, methods and managers from these base classes inherit normally, so if you absolutely need access to these you can opt to move them into a superclass."
msgstr ""

#: ../../topics/migrations.txt:347
# fad16f426a11490396d4ae2faab6fbad
msgid "Data Migrations"
msgstr ""

#: ../../topics/migrations.txt:349
# 7360a90173724310aea699f136d1fc94
msgid "As well as changing the database schema, you can also use migrations to change the data in the database itself, in conjunction with the schema if you want."
msgstr ""

#: ../../topics/migrations.txt:352
# 4c4cd7af37564420865abef843a62b58
msgid "Migrations that alter data are usually called \"data migrations\"; they're best written as separate migrations, sitting alongside your schema migrations."
msgstr ""

#: ../../topics/migrations.txt:355
# 71795df94bfc42ef9706a2318d97e485
msgid "Django can't automatically generate data migrations for you, as it does with schema migrations, but it's not very hard to write them. Migration files in Django are made up of :doc:`Operations </ref/migration-operations>`, and the main operation you use for data migrations is :class:`~django.db.migrations.operations.RunPython`."
msgstr ""

#: ../../topics/migrations.txt:361
# edc048feb9bd41d3838a1cf64bf11620
msgid "To start, make an empty migration file you can work from (Django will put the file in the right place, suggest a name, and add dependencies for you)::"
msgstr ""

#: ../../topics/migrations.txt:366
# b66960af0e2f489fafb7bb9a0d7c5986
msgid "Then, open up the file; it should look something like this::"
msgstr ""

#: ../../topics/migrations.txt:380
# 8dd0b74a475d48c4a23867be4c7373d2
msgid "Now, all you need to do is create a new function and have :class:`~django.db.migrations.operations.RunPython` use it. :class:`~django.db.migrations.operations.RunPython` expects a callable as its argument which takes two arguments - the first is an :doc:`app registry </ref/applications/>` that has the historical versions of all your models loaded into it to match where in your history the migration sits, and the second is a :doc:`SchemaEditor </ref/schema-editor>`, which you can use to manually effect database schema changes (but beware, doing this can confuse the migration autodetector!)"
msgstr ""

#: ../../topics/migrations.txt:390
# 8851289513ab4b6cbb54782d2cc71fcf
msgid "Let's write a simple migration that populates our new ``name`` field with the combined values of ``first_name`` and ``last_name`` (we've come to our senses and realized that not everyone has first and last names). All we need to do is use the historical model and iterate over the rows::"
msgstr ""

#: ../../topics/migrations.txt:416
# 726aead45a394132b5a5541806e5cde6
msgid "Once that's done, we can just run ``python manage.py migrate`` as normal and the data migration will run in place alongside other migrations."
msgstr ""

#: ../../topics/migrations.txt:419
# c1e980360f5e41c899af28ebe71aa147
msgid "You can pass a second callable to :class:`~django.db.migrations.operations.RunPython` to run whatever logic you want executed when migrating backwards. If this callable is omitted, migrating backwards will raise an exception."
msgstr ""

#: ../../topics/migrations.txt:424
# 4c3446c9d27346a9b9db8bf81a5bd252
msgid "If you're interested in the more advanced migration operations, or want to be able to write your own, see the :doc:`migration operations reference </ref/migration-operations>`."
msgstr ""

#: ../../topics/migrations.txt:431
# 0b85467f27e546c19ff1b649e25ce065
msgid "Squashing migrations"
msgstr ""

#: ../../topics/migrations.txt:433
# 348c2e20a17c463eafec6ae50c7ce50b
msgid "You are encouraged to make migrations freely and not worry about how many you have; the migration code is optimized to deal with hundreds at a time without much slowdown. However, eventually you will want to move back from having several hundred migrations to just a few, and that's where squashing comes in."
msgstr ""

#: ../../topics/migrations.txt:438
# a425b79b465b403799e9868828e72336
msgid "Squashing is the act of reducing an existing set of many migrations down to one (or sometimes a few) migrations which still represent the same changes."
msgstr ""

#: ../../topics/migrations.txt:441
# e56d95353af84801a71de3347635ed09
msgid "Django does this by taking all of your existing migrations, extracting their ``Operation``\\s and putting them all in sequence, and then running an optimizer over them to try and reduce the length of the list - for example, it knows that :class:`~django.db.migrations.operations.CreateModel` and :class:`~django.db.migrations.operations.DeleteModel` cancel each other out, and it knows that :class:`~django.db.migrations.operations.AddField` can be rolled into :class:`~django.db.migrations.operations.CreateModel`."
msgstr ""

#: ../../topics/migrations.txt:449
# da6bf1b63eab43a38c85680d44101503
msgid "Once the operation sequence has been reduced as much as possible - the amount possible depends on how closely intertwined your models are and if you have any :class:`~django.db.migrations.operations.RunSQL` or :class:`~django.db.migrations.operations.RunPython` operations (which can't be optimized through) - Django will them write it back out into a new set of initial migration files."
msgstr ""

#: ../../topics/migrations.txt:456
# eb6d7e2cecae478697445ba2f8567545
msgid "These files are marked to say they replace the previously-squashed migrations, so they can coexist with the old migration files, and Django will intelligently switch between them depending where you are in the history. If you're still part-way through the set of migrations that you squashed, it will keep using them until it hits the end and then switch to the squashed history, while new installs will just use the new squashed migration and skip all the old ones."
msgstr ""

#: ../../topics/migrations.txt:463
# 152875cf9d4947e7a821f2ff923c2436
msgid "This enables you to squash and not mess up systems currently in production that aren't fully up-to-date yet. The recommended process is to squash, keeping the old files, commit and release, wait until all systems are upgraded with the new release (or if you're a third-party project, just ensure your users upgrade releases in order without skipping any), and then remove the old files, commit and do a second release."
msgstr ""

#: ../../topics/migrations.txt:470
# 3c92f407a6614f579a6bc935393074b1
msgid "The command that backs all this is :djadmin:`squashmigrations` - just pass it the app label and migration name you want to squash up to, and it'll get to work::"
msgstr ""

#: ../../topics/migrations.txt:489
# 1e089efa9fb34370b8ed17312cb3f319
msgid "Note that model interdependencies in Django can get very complex, and squashing may result in migrations that do not run; either mis-optimized (in which case you can try again with ``--no-optimize``, though you should also report an issue), or with a ``CircularDependencyError``, in which case you can manually resolve it."
msgstr ""

#: ../../topics/migrations.txt:494
# 82b2ab13093b4b8b90ecdc24fa31752a
msgid "To manually resolve a ``CircularDependencyError``, break out one of the ForeignKeys in the circular dependency loop into a separate migration, and move the dependency on the other app with it. If you're unsure, see how makemigrations deals with the problem when asked to create brand new migrations from your models. In a future release of Django, squashmigrations will be updated to attempt to resolve these errors itself."
msgstr ""

#: ../../topics/migrations.txt:501
# f80cc23cdc0547f2ba7d69a63c3c315f
msgid "Once you've squashed your migration, you should then commit it alongside the migrations it replaces and distribute this change to all running instances of your application, making sure that they run ``migrate`` to store the change in their database."
msgstr ""

#: ../../topics/migrations.txt:506
# 298170363f0a4d8d8e20f534d357534c
msgid "After this has been done, you must then transition the squashed migration to a normal initial migration, by:"
msgstr ""

#: ../../topics/migrations.txt:509
# 0fc0c96b6652486d902a6f2e1a01ab63
msgid "Deleting all the migration files it replaces"
msgstr ""

#: ../../topics/migrations.txt:510
# e8ea716fdde04673b358fdcf69c0aff8
msgid "Removing the ``replaces`` argument in the ``Migration`` class of the squashed migration (this is how Django tells that it is a squashed migration)"
msgstr ""

#: ../../topics/migrations.txt:514
# 344bf5dc57634fb9b0cd93851475e917
msgid "Once you've squashed a migration, you should not then re-squash that squashed migration until you have fully transitioned it to a normal migration."
msgstr ""

#: ../../topics/migrations.txt:521
# ed1e607d785b438cb55e6e7f8bc23b42
msgid "Serializing values"
msgstr ""

#: ../../topics/migrations.txt:523
# 6a48332337ed445ba744f2b3b0c362c4
msgid "Migrations are just Python files containing the old definitions of your models - thus, to write them, Django must take the current state of your models and serialize them out into a file."
msgstr ""

#: ../../topics/migrations.txt:527
# d485c8e2526b46abac13a82190458cd7
msgid "While Django can serialize most things, there are some things that we just can't serialize out into a valid Python representation - there's no Python standard for how a value can be turned back into code (``repr()`` only works for basic values, and doesn't specify import paths)."
msgstr ""

#: ../../topics/migrations.txt:532
# 9cbafee9a70c488e940fb4cc514bfa51
msgid "Django can serialize the following:"
msgstr ""

#: ../../topics/migrations.txt:534
# 9279f32d45c5495e8b20934e5ae9735a
msgid "``int``, ``long``, ``float``, ``bool``, ``str``, ``unicode``, ``bytes``, ``None``"
msgstr ""

#: ../../topics/migrations.txt:535
# 5d8c4c64c4a6441282154541e957dbf7
msgid "``list``, ``set``, ``tuple``, ``dict``"
msgstr ""

#: ../../topics/migrations.txt:536
# 45af8d7404e4479a836f590518db53ee
msgid "``datetime.date``, ``datetime.time``, and ``datetime.datetime`` instances"
msgstr ""

#: ../../topics/migrations.txt:537
# e6c736f9f2b147bfa1799db1f622b4fc
msgid "``decimal.Decimal`` instances"
msgstr ""

#: ../../topics/migrations.txt:538
# d143c37be46f43f19b231aa5e2f79a24
msgid "Any Django field"
msgstr ""

#: ../../topics/migrations.txt:539
# 5c38f55179d541de8ecc603c660d1490
msgid "Any function or method reference (e.g. ``datetime.datetime.today``)"
msgstr ""

#: ../../topics/migrations.txt:540
# 49b6fc4ad3e346b0b19c7059038dac37
msgid "Any class reference"
msgstr ""

#: ../../topics/migrations.txt:541
# 7e32780ab89a4bc08572fbda6806a5b2
msgid "Anything with a custom ``deconstruct()`` method (:ref:`see below <custom-deconstruct-method>`)"
msgstr ""

#: ../../topics/migrations.txt:543
# d13142ad4dcc4a02ade0bdc5b638bf09
msgid "Django can serialize the following on Python 3 only:"
msgstr ""

#: ../../topics/migrations.txt:545
# c3637900b92e4aa5abf1935872285b5e
msgid "Unbound methods used from within the class body (see below)"
msgstr ""

#: ../../topics/migrations.txt:547
# f9262ee696fb42788a42844fae4d9c43
msgid "Django cannot serialize:"
msgstr ""

#: ../../topics/migrations.txt:549
# 14984178797d4a7b956c0e8f7ce6d6a7
msgid "Arbitrary class instances (e.g. ``MyClass(4.3, 5.7)``)"
msgstr ""

#: ../../topics/migrations.txt:550
# 4e722c255a3b428c93b51b2d2f3b3cb1
msgid "Lambdas"
msgstr ""

#: ../../topics/migrations.txt:552
# 33ea132c48ca438b8da718a8e9d8fd87
msgid "Due to the fact ``__qualname__`` was only introduced in Python 3, Django can only serialize the following pattern (an unbound method used within the class body) on Python 3, and will fail to serialize a reference to it on Python 2::"
msgstr ""

#: ../../topics/migrations.txt:563
# 5b0cadf9074c40ab8d1ef6a7f6269882
msgid "If you are using Python 2, we recommend you move your methods for upload_to and similar arguments that accept callables (e.g. ``default``) to live in the main module body, rather than the class body."
msgstr ""

#: ../../topics/migrations.txt:570
# 8d01821f9f5f40098a7f6d75af71b18f
msgid "Adding a deconstruct() method"
msgstr ""

#: ../../topics/migrations.txt:572
# 14e6837524b5484fb9756b1b5b7d2891
msgid "You can let Django serialize your own custom class instances by giving the class a ``deconstruct()`` method. It takes no arguments, and should return a tuple of three things ``(path, args, kwargs)``:"
msgstr ""

#: ../../topics/migrations.txt:576
# 767872e704c649268d0ec917754fe9c8
msgid "``path`` should be the Python path to the class, with the class name included as the last part (for example, ``myapp.custom_things.MyClass``). If your class is not available at the top level of a module it is not serializable."
msgstr ""

#: ../../topics/migrations.txt:580
# 4a627342d32a408e951935dc61f6d6f5
msgid "``args`` should be a list of positional arguments to pass to your class' ``__init__`` method. Everything in this list should itself be serializable."
msgstr ""

#: ../../topics/migrations.txt:583
# a3caeaa367f746e58efbe7abe6c1402c
msgid "``kwargs`` should be a dict of keyword arguments to pass to your class' ``__init__`` method. Every value should itself be serializable."
msgstr ""

#: ../../topics/migrations.txt:587
# 5e828151909e4b79b06e598a02b6af99
msgid "This return value is different from the ``deconstruct()`` method :ref:`for custom fields <custom-field-deconstruct-method>` which returns a tuple of four items."
msgstr ""

#: ../../topics/migrations.txt:591
# fee172edb68242c1acc11f233bbe7ca2
msgid "Django will write out the value as an instantiation of your class with the given arguments, similar to the way it writes out references to Django fields."
msgstr ""

#: ../../topics/migrations.txt:594
# 8eaaeab96b6e445dbcdb3e71c7279feb
msgid "As long as all of the arguments to your class' constructor are themselves serializable, you can just use the ``@deconstructible`` class decorator from ``django.utils.deconstruct`` to add the method::"
msgstr ""

#: ../../topics/migrations.txt:606
# c2ba6efa64824585bb535c998fb18237
msgid "The decorator adds logic to capture and preserve the arguments on their way into your constructor, and then returns those arguments exactly when deconstruct() is called."
msgstr ""

#: ../../topics/migrations.txt:613
# 419c8be8d9204e118c07291fe5df37b0
msgid "Upgrading from South"
msgstr ""

#: ../../topics/migrations.txt:615
# 2751d924e17c4f9d951823adf0150ff2
msgid "If you already have pre-existing migrations created with `South <http://south.aeracode.org>`_, then the upgrade process to use ``django.db.migrations`` is quite simple:"
msgstr ""

#: ../../topics/migrations.txt:619
# 58cf2b815bfe456d87ba26da24b28c6f
msgid "Ensure all installs are fully up-to-date with their migrations"
msgstr ""

#: ../../topics/migrations.txt:620
# cf9129e4c88a4bf9b883c3c912816896
msgid "Delete all your (numbered) migration files, but not the directory or ``__init__.py`` - make sure you remove the ``.pyc`` files too."
msgstr ""

#: ../../topics/migrations.txt:622
# 560ec807f1a14ccba289e91b358eb8e7
msgid "Run ``python manage.py makemigrations``. Django should see the empty migration directories and make new initial migrations in the new format."
msgstr ""

#: ../../topics/migrations.txt:624
# 9dceb4ea13074c2cb9ed3b0019675782
msgid "Run ``python manage.py migrate``. Django will see that the tables for the initial migrations already exist and mark them as applied without running them."
msgstr ""

#: ../../topics/migrations.txt:628
# 34b0dda75ba945a49eeb06d479b2308b
msgid "That's it! The only complication is if you have a circular dependency loop of foreign keys; in this case, ``makemigrations`` might make more than one initial migration, and you'll need to mark them all as applied using::"
msgstr ""

#: ../../topics/migrations.txt:635
# 1ae04795e3bf4dfea76e2e81b1f2b889
msgid "Libraries/Third-party Apps"
msgstr ""

#: ../../topics/migrations.txt:637
# 5e2c737668d24ebc934b1d95e0bb4108
msgid "If you are a library or app maintainer, and wish to support both South migrations (for Django 1.6 and below) and Django migrations (for 1.7 and above) you should keep two parallel migration sets in your app, one in each format."
msgstr ""

#: ../../topics/migrations.txt:641
# 7a9d3f4cd3da43f686ce5834a88e9761
msgid "To aid in this, South 1.0 will automatically look for South-format migrations in a ``south_migrations`` directory first, before looking in ``migrations``, meaning that users' projects will transparently use the correct set as long as you put your South migrations in the ``south_migrations`` directory and your Django migrations in the ``migrations`` directory."
msgstr ""

#: ../../topics/migrations.txt:647
# e49da7375aa7402ba99e053774e85f30
msgid "More information is available in the `South 1.0 release notes <http://south.readthedocs.org/en/latest/releasenotes/1.0.html#library-migration-path>`_."
msgstr ""

