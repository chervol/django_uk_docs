# 
msgid ""
msgstr ""
"Project-Id-Version: Django 1.7\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2014-10-13 18:33+0300\n"
"Last-Translator: Volodymyr Cherepanyak <chervol@gmail.com>\n"
"Language-Team: Quintagroup <info@quintagroup.com>\n"
"Content-Transfer-Encoding: 8bit\n"" Content-Type: text/plain; charset=UTF-8\n"
"MIME-Version: 1.0\n"
"Language: uk\n"

# 1fda67b00d73445f9456d35247656561
#: ../../topics/testing/advanced.txt:3
msgid "Advanced testing topics"
msgstr ""

# 7c20523c94b6476d8356adc4b000d3d5
#: ../../topics/testing/advanced.txt:6
msgid "The request factory"
msgstr ""

# e4574bda54594fdc961557a52c751297
#: ../../topics/testing/advanced.txt:12
msgid ""
"The :class:`~django.test.RequestFactory` shares the same API as the test "
"client. However, instead of behaving like a browser, the RequestFactory "
"provides a way to generate a request instance that can be used as the first "
"argument to any view. This means you can test a view function the same way "
"as you would test any other function -- as a black box, with exactly known "
"inputs, testing for specific outputs."
msgstr ""

# b4ca260b9e7c4e739bd91c00a32a71f8
#: ../../topics/testing/advanced.txt:19
msgid ""
"The API for the :class:`~django.test.RequestFactory` is a slightly "
"restricted subset of the test client API:"
msgstr ""

# b05d7607e8d940bdb196e7519885635c
#: ../../topics/testing/advanced.txt:22
msgid ""
"It only has access to the HTTP methods :meth:`~Client.get()`, "
":meth:`~Client.post()`, :meth:`~Client.put()`, :meth:`~Client.delete()`, "
":meth:`~Client.head()` and :meth:`~Client.options()`."
msgstr ""

# fe96e52be3f6472faf30ae6424d97188
#: ../../topics/testing/advanced.txt:27
msgid ""
"These methods accept all the same arguments *except* for ``follows``. Since "
"this is just a factory for producing requests, it's up to you to handle the "
"response."
msgstr ""

# 6f4bd4bf907a4f2490949296ae6e4790
#: ../../topics/testing/advanced.txt:31
msgid ""
"It does not support middleware. Session and authentication attributes must "
"be supplied by the test itself if required for the view to function "
"properly."
msgstr ""

# e120cb2abe4e43d1aa3e2a2c4a2af926
#: ../../topics/testing/advanced.txt:36
msgid "Example"
msgstr ""

# 0328a9cc4fb748b4ade1e246a508975e
#: ../../topics/testing/advanced.txt:38
msgid "The following is a simple unit test using the request factory::"
msgstr ""

# 689ebd89ab90466aa1faeccac7a2d5a4
#: ../../topics/testing/advanced.txt:65
msgid "Tests and multiple databases"
msgstr ""

# ef12d7d34fa1494389fdc7575fc1c6f5
#: ../../topics/testing/advanced.txt:70
msgid "Testing master/slave configurations"
msgstr ""

# ff3f9f640baf4de0b92ad90f61b0bee6
#: ../../topics/testing/advanced.txt:72
msgid ""
"If you're testing a multiple database configuration with master/slave "
"replication, this strategy of creating test databases poses a problem. When "
"the test databases are created, there won't be any replication, and as a "
"result, data created on the master won't be seen on the slave."
msgstr ""

# aeecee40606b40be9fe46eaa7bab9e14
#: ../../topics/testing/advanced.txt:78
msgid ""
"To compensate for this, Django allows you to define that a database is a "
"*test mirror*. Consider the following (simplified) example database "
"configuration::"
msgstr ""

# 8d5725c10df34c319c6c3c41616c8528
#: ../../topics/testing/advanced.txt:98
msgid ""
"In this setup, we have two database servers: ``dbmaster``, described by the "
"database alias ``default``, and ``dbslave`` described by the alias "
"``slave``. As you might expect, ``dbslave`` has been configured by the "
"database administrator as a read slave of ``dbmaster``, so in normal "
"activity, any write to ``default`` will appear on ``slave``."
msgstr ""

# c58a254e47bc466aaf32dd549b710315
#: ../../topics/testing/advanced.txt:104
msgid ""
"If Django created two independent test databases, this would break any tests"
" that expected replication to occur. However, the ``slave`` database has "
"been configured as a test mirror (using the :setting:`TEST_MIRROR` setting),"
" indicating that under testing, ``slave`` should be treated as a mirror of "
"``default``."
msgstr ""

# bcf2eec68c7b4f3a8f283646503a7ba2
#: ../../topics/testing/advanced.txt:110
msgid ""
"When the test environment is configured, a test version of ``slave`` will "
"*not* be created. Instead the connection to ``slave`` will be redirected to "
"point at ``default``. As a result, writes to ``default`` will appear on "
"``slave`` -- but because they are actually the same database, not because "
"there is data replication between the two databases."
msgstr ""

# 4f12b9aa47a94e6683d681f8dd4648f2
#: ../../topics/testing/advanced.txt:120
msgid "Controlling creation order for test databases"
msgstr ""

# 94d48a9bac1b462ab24c3d368966a06a
#: ../../topics/testing/advanced.txt:122
msgid ""
"By default, Django will assume all databases depend on the ``default`` "
"database and therefore always create the ``default`` database first. "
"However, no guarantees are made on the creation order of any other databases"
" in your test setup."
msgstr ""

# f19c6cc1ddd84744bb31ba613b8781be
#: ../../topics/testing/advanced.txt:127
msgid ""
"If your database configuration requires a specific creation order, you can "
"specify the dependencies that exist using the :setting:`TEST_DEPENDENCIES` "
"setting. Consider the following (simplified) example database "
"configuration::"
msgstr ""

# a28c85bbdf6343329c91f9db6fe82d3b
#: ../../topics/testing/advanced.txt:155
msgid ""
"Under this configuration, the ``diamonds`` database will be created first, "
"as it is the only database alias without dependencies. The ``default`` and "
"``clubs`` alias will be created next (although the order of creation of this"
" pair is not guaranteed); then ``hearts``; and finally ``spades``."
msgstr ""

# a661c5cd8453484d9fa881512d3321bc
#: ../../topics/testing/advanced.txt:160
msgid ""
"If there are any circular dependencies in the :setting:`TEST_DEPENDENCIES` "
"definition, an ``ImproperlyConfigured`` exception will be raised."
msgstr ""

# 2a02acbe65bb40148c611eba73f7d85f
#: ../../topics/testing/advanced.txt:165
msgid "Advanced features of ``TransactionTestCase``"
msgstr ""

# 5471736d9cb04517b815b5af8aeefb51
#: ../../topics/testing/advanced.txt:173
msgid ""
"This attribute is a private API. It may be changed or removed without a "
"deprecation period in the future, for instance to accommodate changes in "
"application loading."
msgstr ""

# 6285a27b4fd44322968aa58f28111e95
#: ../../topics/testing/advanced.txt:177
msgid ""
"It's used to optimize Django's own test suite, which contains hundreds of "
"models but no relations between models in different applications."
msgstr ""

# 57fb09367f944d81a6a5cd3658ce14f3
#: ../../topics/testing/advanced.txt:180
msgid ""
"By default, ``available_apps`` is set to ``None``. After each test, Django "
"calls :djadmin:`flush` to reset the database state. This empties all tables "
"and emits the :data:`~django.db.models.signals.post_migrate` signal, which "
"re-creates one content type and three permissions for each model. This "
"operation gets expensive proportionally to the number of models."
msgstr ""

# 6e34b4180be3447a8b8dac90300ac07c
#: ../../topics/testing/advanced.txt:186
msgid ""
"Setting ``available_apps`` to a list of applications instructs Django to "
"behave as if only the models from these applications were available. The "
"behavior of ``TransactionTestCase`` changes as follows:"
msgstr ""

# cd73248a4d294b6ebc8721cba87c1bc8
#: ../../topics/testing/advanced.txt:190
msgid ""
":data:`~django.db.models.signals.post_migrate` is fired before each test to "
"create the content types and permissions for each model in available apps, "
"in case they're missing."
msgstr ""

# 7c12cb5e9ae1484ca46b8a032e6b7dfb
#: ../../topics/testing/advanced.txt:193
msgid ""
"After each test, Django empties only tables corresponding to models in "
"available apps. However, at the database level, truncation may cascade to "
"related models in unavailable apps. Furthermore "
":data:`~django.db.models.signals.post_migrate` isn't fired; it will be fired"
" by the next ``TransactionTestCase``, after the correct set of applications "
"is selected."
msgstr ""

# 46972c5281024833a442ff65c69fb8c6
#: ../../topics/testing/advanced.txt:200
msgid ""
"Since the database isn't fully flushed, if a test creates instances of "
"models not included in ``available_apps``, they will leak and they may cause"
" unrelated tests to fail. Be careful with tests that use sessions; the "
"default session engine stores them in the database."
msgstr ""

# fd64def162c64dd2b8d69d06cd299b43
#: ../../topics/testing/advanced.txt:205
msgid ""
"Since :data:`~django.db.models.signals.post_migrate` isn't emitted after "
"flushing the database, its state after a ``TransactionTestCase`` isn't the "
"same as after a ``TestCase``: it's missing the rows created by listeners to "
":data:`~django.db.models.signals.post_migrate`. Considering the :ref:`order "
"in which tests are executed <order-of-tests>`, this isn't an issue, provided"
" either all ``TransactionTestCase`` in a given test suite declare "
"``available_apps``, or none of them."
msgstr ""

# 32df002aa3234ab78dc25021d82df604
#: ../../topics/testing/advanced.txt:213
msgid "``available_apps`` is mandatory in Django's own test suite."
msgstr ""

# 7cf45843f541489a8d8bcdfd9739e36f
#: ../../topics/testing/advanced.txt:217
msgid ""
"Setting ``reset_sequences = True`` on a ``TransactionTestCase`` will make "
"sure sequences are always reset before the test run::"
msgstr ""

# e70aeaee42704c33a4e764c13203824a
#: ../../topics/testing/advanced.txt:228
msgid ""
"Unless you are explicitly testing primary keys sequence numbers, it is "
"recommended that you do not hard code primary key values in tests."
msgstr ""

# d01754492c174a0286a4691e83b6d173
#: ../../topics/testing/advanced.txt:231
msgid ""
"Using ``reset_sequences = True`` will slow down the test, since the primary "
"key reset is an relatively expensive database operation."
msgstr ""

# 4ee57a1caf8a4c06a33a8e590972951b
#: ../../topics/testing/advanced.txt:235
msgid "Running tests outside the test runner"
msgstr ""

# 5a0210fabf894631b5ea1c99d444273f
#: ../../topics/testing/advanced.txt:237
msgid ""
"If you want to run tests outside of ``./manage.py test`` -- for example, "
"from a shell prompt -- you will need to set up the test environment first. "
"Django provides a convenience method to do this::"
msgstr ""

# 68a63424c4254b569e33a39ac0bd0b20
#: ../../topics/testing/advanced.txt:244
msgid ""
":func:`~django.test.utils.setup_test_environment` puts several Django "
"features into modes that allow for repeatable testing, but does not create "
"the test databases; "
":func:`django.test.runner.DiscoverRunner.setup_databases` takes care of "
"that."
msgstr ""

# 73a02b970c7644d587ab7b93d032ac10
#: ../../topics/testing/advanced.txt:249
msgid ""
"The call to :func:`~django.test.utils.setup_test_environment` is made "
"automatically as part of the setup of ``./manage.py test``. You only need to"
" manually invoke this method if you're not using running your tests via "
"Django's test runner."
msgstr ""

# bed00d6a92534360a28ccbcd072c8480
#: ../../topics/testing/advanced.txt:256
msgid ""
"If you are not using a management command to invoke the tests, you will also"
" need to first setup Django itself using :func:`django.setup()`."
msgstr ""

# 35811dad7aab4a83b9f47dabd4fc149a
#: ../../topics/testing/advanced.txt:262
msgid "Using different testing frameworks"
msgstr ""

# 0c9ab74a1a074924a223f9b3119b7692
#: ../../topics/testing/advanced.txt:264
msgid ""
"Clearly, :mod:`unittest` is not the only Python testing framework. While "
"Django doesn't provide explicit support for alternative frameworks, it does "
"provide a way to invoke tests constructed for an alternative framework as if"
" they were normal Django tests."
msgstr ""

# e3d0756fa78b4bdb90ec0ddd7f89406a
#: ../../topics/testing/advanced.txt:269
msgid ""
"When you run ``./manage.py test``, Django looks at the "
":setting:`TEST_RUNNER` setting to determine what to do. By default, "
":setting:`TEST_RUNNER` points to ``'django.test.runner.DiscoverRunner'``. "
"This class defines the default Django testing behavior. This behavior "
"involves:"
msgstr ""

# ba0159bdfb034eb7bb49021a4bc2a430
#: ../../topics/testing/advanced.txt:274
msgid "Performing global pre-test setup."
msgstr ""

# 20c2f6eb1d134439b7647b4ae8dcaa76
#: ../../topics/testing/advanced.txt:276
msgid ""
"Looking for tests in any file below the current directory whose name matches"
" the pattern ``test*.py``."
msgstr ""

# fee99882d8d14d66afede277e4dab121
#: ../../topics/testing/advanced.txt:279
msgid "Creating the test databases."
msgstr ""

# 48d310e174454c98af1a1f8c87fc2eb1
#: ../../topics/testing/advanced.txt:281
msgid ""
"Running ``migrate`` to install models and initial data into the test "
"databases."
msgstr ""

# 0c9a1219b3e04bac83e457e9dc41f66c
#: ../../topics/testing/advanced.txt:284
msgid "Running the tests that were found."
msgstr ""

# 1aee9ccdad7246e3a8bce5f529debdac
#: ../../topics/testing/advanced.txt:286
msgid "Destroying the test databases."
msgstr ""

# a9769d6942464fb1bb434cb072c5afcb
#: ../../topics/testing/advanced.txt:288
msgid "Performing global post-test teardown."
msgstr ""

# 19d3abf772d344849b4852217991c95d
#: ../../topics/testing/advanced.txt:290
msgid ""
"If you define your own test runner class and point :setting:`TEST_RUNNER` at"
" that class, Django will execute your test runner whenever you run "
"``./manage.py test``. In this way, it is possible to use any test framework "
"that can be executed from Python code, or to modify the Django test "
"execution process to satisfy whatever testing requirements you may have."
msgstr ""

# a871bead3a0a4a20bb64b0f90e297a72
#: ../../topics/testing/advanced.txt:299
msgid "Defining a test runner"
msgstr ""

# 77e319fa4fda416d89663076aa2962ba
#: ../../topics/testing/advanced.txt:305
msgid ""
"A test runner is a class defining a ``run_tests()`` method. Django ships "
"with a ``DiscoverRunner`` class that defines the default Django testing "
"behavior. This class defines the ``run_tests()`` entry point, plus a "
"selection of other methods that are used to by ``run_tests()`` to set up, "
"execute and tear down the test suite."
msgstr ""

# 16e3bcfeca4a412cae015e9521f8bd8a
#: ../../topics/testing/advanced.txt:313
msgid ""
"``DiscoverRunner`` will search for tests in any file matching ``pattern``."
msgstr ""

# e194edb96edf4b908839db1a36075eae
#: ../../topics/testing/advanced.txt:315
msgid ""
"``top_level`` can be used to specify the directory containing your top-level"
" Python modules. Usually Django can figure this out automatically, so it's "
"not necessary to specify this option. If specified, it should generally be "
"the directory containing your ``manage.py`` file."
msgstr ""

# fbb8fd0573ee4db7a902a5d27c8676f8
#: ../../topics/testing/advanced.txt:320
msgid ""
"``verbosity`` determines the amount of notification and debug information "
"that will be printed to the console; ``0`` is no output, ``1`` is normal "
"output, and ``2`` is verbose output."
msgstr ""

# 5ce4093d921645a389041e0d5b8c5adf
#: ../../topics/testing/advanced.txt:324
msgid ""
"If ``interactive`` is ``True``, the test suite has permission to ask the "
"user for instructions when the test suite is executed. An example of this "
"behavior would be asking for permission to delete an existing test database."
" If ``interactive`` is ``False``, the test suite must be able to run without"
" any manual intervention."
msgstr ""

# 08ba59b7b88142a7b8d7ee4e2bd18af5
#: ../../topics/testing/advanced.txt:330
msgid ""
"If ``failfast`` is ``True``, the test suite will stop running after the "
"first test failure is detected."
msgstr ""

# b2217ee6534c4457897ce215484c8a88
#: ../../topics/testing/advanced.txt:333
msgid ""
"Django may, from time to time, extend the capabilities of the test runner by"
" adding new arguments. The ``**kwargs`` declaration allows for this "
"expansion. If you subclass ``DiscoverRunner`` or write your own test runner,"
" ensure it accepts ``**kwargs``."
msgstr ""

# 513aeb154a2d4a3c9834b410d9bfd9df
#: ../../topics/testing/advanced.txt:338
msgid ""
"Your test runner may also define additional command-line options. If you add"
" an ``option_list`` attribute to a subclassed test runner, those options "
"will be added to the list of command-line options that the :djadmin:`test` "
"command can use."
msgstr ""

# ff8bc17858df40f8b97e4ee561c2b385
#: ../../topics/testing/advanced.txt:344
msgid "Attributes"
msgstr ""

# 4d22168900d54058bb2c5fc11af04bbd
#: ../../topics/testing/advanced.txt:350
msgid ""
"The class used to build the test suite. By default it is set to "
"``unittest.TestSuite``. This can be overridden if you wish to implement "
"different logic for collecting tests."
msgstr ""

# d52216862fe6401b993647107e52a649
#: ../../topics/testing/advanced.txt:358
msgid ""
"This is the class of the low-level test runner which is used to execute the "
"individual tests and format the results. By default it is set to "
"``unittest.TextTestRunner``. Despite the unfortunate similarity in naming "
"conventions, this is not the same type of class as ``DiscoverRunner``, which"
" covers a broader set of responsibilities. You can override this attribute "
"to modify the way tests are run and reported."
msgstr ""

# 84f2267bf555415bb2ea03f54e858895
#: ../../topics/testing/advanced.txt:367
msgid ""
"This is the class that loads tests, whether from TestCases or modules or "
"otherwise and bundles them into test suites for the runner to execute. By "
"default it is set to ``unittest.defaultTestLoader``. You can override this "
"attribute if your tests are going to be loaded in unusual ways."
msgstr ""

# 7c0a2e2a241d4519a3da1192dc4f9fa6
#: ../../topics/testing/advanced.txt:374
msgid ""
"This is the tuple of ``optparse`` options which will be fed into the "
"management command's ``OptionParser`` for parsing arguments. See the "
"documentation for Python's ``optparse`` module for more details."
msgstr ""

# 1f404ecb2b334c1291460eadc09b2e8d
#: ../../topics/testing/advanced.txt:379
msgid "Methods"
msgstr ""

# 558f12f9bb594662a3c695ea85fc0ae0
#: ../../topics/testing/advanced.txt:383
msgid "Run the test suite."
msgstr ""

# cb92fa1aed00474f8a4ef60702d4c339
#: ../../topics/testing/advanced.txt:385
msgid ""
"``test_labels`` allows you to specify which tests to run and supports "
"several formats (see :meth:`DiscoverRunner.build_suite` for a list of "
"supported formats)."
msgstr ""

# ba585bae54cc4c4f90cc7f2308be2b62
# a3d795cbb6864e9fa9daf44c6a026c8f
#: ../../topics/testing/advanced.txt:389 ../../topics/testing/advanced.txt:421
msgid ""
"``extra_tests`` is a list of extra ``TestCase`` instances to add to the "
"suite that is executed by the test runner. These extra tests are run in "
"addition to those discovered in the modules listed in ``test_labels``."
msgstr ""

# d7bdde9d772e4ecfbea3fce5efa194ac
#: ../../topics/testing/advanced.txt:393
msgid "This method should return the number of tests that failed."
msgstr ""

# 47a5caf5dc4e4681ace351f9680245dd
#: ../../topics/testing/advanced.txt:397
msgid ""
"Sets up the test environment by calling "
":func:`~django.test.utils.setup_test_environment` and setting "
":setting:`DEBUG` to ``False``."
msgstr ""

# 8df894e3bb3444ccb3852b73e11debe4
#: ../../topics/testing/advanced.txt:403
msgid "Constructs a test suite that matches the test labels provided."
msgstr ""

# 3ce6eba7c2b64ce7a3b2094768745bde
#: ../../topics/testing/advanced.txt:405
msgid ""
"``test_labels`` is a list of strings describing the tests to be run. A test "
"label can take one of four forms:"
msgstr ""

# 52c03b9e22e645d895ffe273d67010fc
#: ../../topics/testing/advanced.txt:408
msgid ""
"``path.to.test_module.TestCase.test_method`` -- Run a single test method in "
"a test case."
msgstr ""

# c871c4ddc9654e21af611926dd6473b4
#: ../../topics/testing/advanced.txt:410
msgid ""
"``path.to.test_module.TestCase`` -- Run all the test methods in a test case."
msgstr ""

# cc9f3533e3134871b130663d30ba4088
#: ../../topics/testing/advanced.txt:412
msgid ""
"``path.to.module`` -- Search for and run all tests in the named Python "
"package or module."
msgstr ""

# 5ee9d7247c1649158481265487751e09
#: ../../topics/testing/advanced.txt:414
msgid ""
"``path/to/directory`` -- Search for and run all tests below the named "
"directory."
msgstr ""

# a3dc230c00ec47d7834942b4a9eab843
#: ../../topics/testing/advanced.txt:417
msgid ""
"If ``test_labels`` has a value of ``None``, the test runner will search for "
"tests in all files below the current directory whose names match its "
"``pattern`` (see above)."
msgstr ""

# 428ab03a1ffd4200a259a07b21d7d579
#: ../../topics/testing/advanced.txt:425
msgid "Returns a ``TestSuite`` instance ready to be run."
msgstr ""

# a91824fb45254c06a54a1378a9ac9b1d
#: ../../topics/testing/advanced.txt:429
msgid "Creates the test databases."
msgstr ""

# 5328e88d501e4563915877d2868fb50e
#: ../../topics/testing/advanced.txt:431
msgid ""
"Returns a data structure that provides enough detail to undo the changes "
"that have been made. This data will be provided to the "
"``teardown_databases()`` function at the conclusion of testing."
msgstr ""

# bc2a1ea7bdd947ebadea05e0dbba723e
#: ../../topics/testing/advanced.txt:437
msgid "Runs the test suite."
msgstr ""

# 06198a2879c34db893042c41a8927b37
#: ../../topics/testing/advanced.txt:439
msgid "Returns the result produced by the running the test suite."
msgstr ""

# c1b3458018594926a82ca7836cec8889
#: ../../topics/testing/advanced.txt:443
msgid "Destroys the test databases, restoring pre-test conditions."
msgstr ""

# d600761fb80c43038ef9ac3b61d247b6
#: ../../topics/testing/advanced.txt:445
msgid ""
"``old_config`` is a data structure defining the changes in the database "
"configuration that need to be reversed. It is the return value of the "
"``setup_databases()`` method."
msgstr ""

# b4af80b10d3646258a72991a54caad63
#: ../../topics/testing/advanced.txt:451
msgid "Restores the pre-test environment."
msgstr ""

# 3fb07efe5aca4777bd4d8386c09c2294
#: ../../topics/testing/advanced.txt:455
msgid ""
"Computes and returns a return code based on a test suite, and the result "
"from that test suite."
msgstr ""

# 43308be8fc8f4a9ba99b06e9719674c4
#: ../../topics/testing/advanced.txt:460
msgid "Testing utilities"
msgstr ""

# 409ef7e0211d443d8f46c8fef53baff1
#: ../../topics/testing/advanced.txt:463
msgid "django.test.utils"
msgstr ""

# fed39c7658474a8db46960fb87abff81
#: ../../topics/testing/advanced.txt:468
msgid ""
"To assist in the creation of your own test runner, Django provides a number "
"of utility methods in the ``django.test.utils`` module."
msgstr ""

# 5f4f160a4ce44407b82ade8f0a6bfbbf
#: ../../topics/testing/advanced.txt:473
msgid ""
"Performs any global pre-test setup, such as the installing the "
"instrumentation of the template rendering system and setting up the dummy "
"email outbox."
msgstr ""

# a24172ef8705496698f58cfd5d79bb59
#: ../../topics/testing/advanced.txt:479
msgid ""
"Performs any global post-test teardown, such as removing the black magic "
"hooks into the template system and restoring normal email services."
msgstr ""

# fe7e5209aedc49339307528c5fb71df0
#: ../../topics/testing/advanced.txt:484
msgid "django.db.connection.creation"
msgstr ""

# cca86f10d72c4a0788f2b1a72ae95c20
#: ../../topics/testing/advanced.txt:488
msgid ""
"The creation module of the database backend also provides some utilities "
"that can be useful during testing."
msgstr ""

# de544a971a3748389169e2c93b75ece0
#: ../../topics/testing/advanced.txt:493
msgid "Creates a new test database and runs ``migrate`` against it."
msgstr ""

# 7eeec8153d464a48bac2c3d91f7b1c29
#: ../../topics/testing/advanced.txt:495
msgid "``verbosity`` has the same behavior as in ``run_tests()``."
msgstr ""

# 5346b55d898a4b90912683e33883ca2f
#: ../../topics/testing/advanced.txt:497
msgid ""
"``autoclobber`` describes the behavior that will occur if a database with "
"the same name as the test database is discovered:"
msgstr ""

# fa88329f12684436acec0eeaf0fd38c5
#: ../../topics/testing/advanced.txt:500
msgid ""
"If ``autoclobber`` is ``False``, the user will be asked to approve "
"destroying the existing database. ``sys.exit`` is called if the user does "
"not approve."
msgstr ""

# c2ed5ac4b1614cbb80957ff98a21cb8b
#: ../../topics/testing/advanced.txt:504
msgid ""
"If autoclobber is ``True``, the database will be destroyed without "
"consulting the user."
msgstr ""

# f73b6e7063da4553bd5ab5c94433376c
#: ../../topics/testing/advanced.txt:507
msgid ""
"``serialize`` determines if Django serializes the database into an in-memory"
" JSON string before running tests (used to restore the database state "
"between tests if you don't have transactions). You can set this to False to "
"significantly speed up creation time if you know you don't need data "
"persistence outside of test fixtures."
msgstr ""

# b274ae438b5d4628a75f4913c79fe5d4
#: ../../topics/testing/advanced.txt:513
msgid "Returns the name of the test database that it created."
msgstr ""

# e3c3092e733c43af8b1fb7482917b3ff
#: ../../topics/testing/advanced.txt:515
msgid ""
"``create_test_db()`` has the side effect of modifying the value of "
":setting:`NAME` in :setting:`DATABASES` to match the name of the test "
"database."
msgstr ""

# 2ca511b063f54d5eac8531dfea26b3ee
#: ../../topics/testing/advanced.txt:521
msgid "The ``serialize`` argument was added."
msgstr ""

# aab81756555741678511c0ebf485f031
#: ../../topics/testing/advanced.txt:525
msgid ""
"Destroys the database whose name is the value of :setting:`NAME` in "
":setting:`DATABASES`, and sets :setting:`NAME` to the value of "
"``old_database_name``."
msgstr ""

# dea7c1ef2e8e4d3a839fb078d779499d
#: ../../topics/testing/advanced.txt:529
msgid ""
"The ``verbosity`` argument has the same behavior as for "
":class:`~django.test.runner.DiscoverRunner`."
msgstr ""

# 2fed20bb6e064bb0847a0b5cbad11cc4
#: ../../topics/testing/advanced.txt:535
msgid "Integration with coverage.py"
msgstr ""

# 61bd3431ff5940c6a2dadda0b6a386c5
#: ../../topics/testing/advanced.txt:537
msgid ""
"Code coverage describes how much source code has been tested. It shows which"
" parts of your code are being exercised by tests and which are not. It's an "
"important part of testing applications, so it's strongly recommended to "
"check the coverage of your tests."
msgstr ""

# 2f17f662ea504142bbeec4213561cea5
#: ../../topics/testing/advanced.txt:542
msgid ""
"Django can be easily integrated with `coverage.py`_, a tool for measuring "
"code coverage of Python programs. First, `install coverage.py`_. Next, run "
"the following from your project folder containing ``manage.py``::"
msgstr ""

# 8d9812fb374b4672840568fc56fdaba6
#: ../../topics/testing/advanced.txt:548
msgid ""
"This runs your tests and collects coverage data of the executed files in "
"your project. You can see a report of this data by typing following "
"command::"
msgstr ""

# e425ac445381456aa102853b9ea16019
#: ../../topics/testing/advanced.txt:553
msgid ""
"Note that some Django code was executed while running tests, but it is not "
"listed here because of the ``source`` flag passed to the previous command."
msgstr ""

# a109bfddf2a44935be7cc4c11cb0cd27
#: ../../topics/testing/advanced.txt:556
msgid ""
"For more options like annotated HTML listings detailing missed lines, see "
"the `coverage.py`_ docs."
msgstr ""
