# SOME DESCRIPTIVE TITLE.
# Copyright (C) Django Software Foundation and contributors
# This file is distributed under the same license as the Django package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: Django 1.7\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2014-10-13 18:33+0300\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#: ../../topics/testing/index.txt:3
# 2aa4e42643204be799ca67eefe809cf5
msgid "Testing in Django"
msgstr ""

#: ../../topics/testing/index.txt:5
# 83909ee7c55b4930a55f67b08f386738
msgid "Automated testing is an extremely useful bug-killing tool for the modern Web developer. You can use a collection of tests -- a **test suite** -- to solve, or avoid, a number of problems:"
msgstr ""

#: ../../topics/testing/index.txt:9
# 225d95233ff2420d9c90702be7fd8cf3
msgid "When you're writing new code, you can use tests to validate your code works as expected."
msgstr ""

#: ../../topics/testing/index.txt:12
# f59e4df9e3de4211b6dfaa3f7c1befe9
msgid "When you're refactoring or modifying old code, you can use tests to ensure your changes haven't affected your application's behavior unexpectedly."
msgstr ""

#: ../../topics/testing/index.txt:16
# 3abdb2db9f764161af7915d5408e77c2
msgid "Testing a Web application is a complex task, because a Web application is made of several layers of logic -- from HTTP-level request handling, to form validation and processing, to template rendering. With Django's test-execution framework and assorted utilities, you can simulate requests, insert test data, inspect your application's output and generally verify your code is doing what it should be doing."
msgstr ""

#: ../../topics/testing/index.txt:23
# f86a5ac5b5bc484782a9a7341d5ee530
msgid "The best part is, it's really easy."
msgstr ""

#: ../../topics/testing/index.txt:25
# e56dd0973f1c468893ed50a040435d36
msgid "The preferred way to write tests in Django is using the :mod:`unittest` module built in to the Python standard library. This is covered in detail in the :doc:`overview` document."
msgstr ""

#: ../../topics/testing/index.txt:29
# eec3fc0797ea48b482adbf5bb2995a30
msgid "You can also use any *other* Python test framework; Django provides an API and tools for that kind of integration. They are described in the :ref:`other-testing-frameworks` section of :doc:`advanced`."
msgstr ""

