# SOME DESCRIPTIVE TITLE.
# Copyright (C) Django Software Foundation and contributors
# This file is distributed under the same license as the Django package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: Django 1.7\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2014-10-13 18:33+0300\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#: ../../releases/1.4.9.txt:3
# a95c50ed419f4ca59773f1ce78ebb9e5
msgid "Django 1.4.9 release notes"
msgstr ""

#: ../../releases/1.4.9.txt:5
# dec5995be2824f938a41cb758ed32ad1
msgid "*October 23, 2013*"
msgstr ""

#: ../../releases/1.4.9.txt:7
# d1f70034e9f54f5ab5aaf1902f9dff2b
msgid "Django 1.4.9 fixes a security-related bug in the 1.4 series and one other data corruption bug."
msgstr ""

#: ../../releases/1.4.9.txt:11
# b89ac5a060d54843924617138468870e
msgid "Readdressed denial-of-service via password hashers"
msgstr ""

#: ../../releases/1.4.9.txt:13
# dfc52022f0db4ad9b6022cd55f8f8ff2
msgid "Django 1.4.8 imposes a 4096-byte limit on passwords in order to mitigate a denial-of-service attack through submission of bogus but extremely large passwords. In Django 1.4.9, we've reverted this change and instead improved the speed of our PBKDF2 algorithm by not rehashing the key on every iteration."
msgstr ""

#: ../../releases/1.4.9.txt:19
# 62b1c4042e7c4e00aa3e213bce05eb38
msgid "Bugfixes"
msgstr ""

#: ../../releases/1.4.9.txt:21
# a5449515fbbd4f4180d9873845c8e369
msgid "Fixed a data corruption bug with ``datetime_safe.datetime.combine`` (#21256)."
msgstr ""

