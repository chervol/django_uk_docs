# SOME DESCRIPTIVE TITLE.
# Copyright (C) Django Software Foundation and contributors
# This file is distributed under the same license as the Django package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: Django 1.7\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2014-10-13 18:33+0300\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#: ../../releases/1.4.13.txt:3
# 92d277bb419344989d5e5641e2a50150
msgid "Django 1.4.13 release notes"
msgstr ""

#: ../../releases/1.4.13.txt:5
# f067d0d5529441f2a8efa26dddd5751e
msgid "*May 14, 2014*"
msgstr ""

#: ../../releases/1.4.13.txt:7
# 8ea71d01893a455093983da4e96eb319
msgid "Django 1.4.13 fixes two security issues in 1.4.12."
msgstr ""

#: ../../releases/1.4.13.txt:10
# 46eb46db54b34ac2ab64fb7fb5a5fac1
msgid "Caches may incorrectly be allowed to store and serve private data"
msgstr ""

#: ../../releases/1.4.13.txt:12
# 87e69000b4984e15917614b9e328509a
msgid "In certain situations, Django may allow caches to store private data related to a particular session and then serve that data to requests with a different session, or no session at all. This can lead to information disclosure and can be a vector for cache poisoning."
msgstr ""

#: ../../releases/1.4.13.txt:17
# 6c605f9242024fd88e6539461895b74a
msgid "When using Django sessions, Django will set a ``Vary: Cookie`` header to ensure caches do not serve cached data to requests from other sessions. However, older versions of Internet Explorer (most likely only Internet Explorer 6, and Internet Explorer 7 if run on Windows XP or Windows Server 2003) are unable to handle the ``Vary`` header in combination with many content types. Therefore, Django would remove the header if the request was made by Internet Explorer."
msgstr ""

#: ../../releases/1.4.13.txt:25
# 92616c82c94f4f929eb146b634c3e441
msgid "To remedy this, the special behavior for these older Internet Explorer versions has been removed, and the ``Vary`` header is no longer stripped from the response. In addition, modifications to the ``Cache-Control`` header for all Internet Explorer requests with a ``Content-Disposition`` header have also been removed as they were found to have similar issues."
msgstr ""

#: ../../releases/1.4.13.txt:32
# 868b5a23cf354bfc8e9dc1b0a2f00c18
msgid "Malformed redirect URLs from user input not correctly validated"
msgstr ""

#: ../../releases/1.4.13.txt:34
# 01fcb1734f934f548b77ff9e4b9c96d2
msgid "The validation for redirects did not correctly validate some malformed URLs, which are accepted by some browsers. This allows a user to be redirected to an unsafe URL unexpectedly."
msgstr ""

#: ../../releases/1.4.13.txt:38
# 7c9b2bebdb664fe78a75142f311f86d0
msgid "Django relies on user input in some cases (e.g. :func:`django.contrib.auth.views.login`, ``django.contrib.comments``, and :doc:`i18n </topics/i18n/index>`) to redirect the user to an \"on success\" URL. The security checks for these redirects (namely ``django.util.http.is_safe_url()``) did not correctly validate some malformed URLs, such as `http:\\\\\\\\\\\\djangoproject.com`, which are accepted by some browsers with more liberal URL parsing."
msgstr ""

#: ../../releases/1.4.13.txt:46
# 6a83486274bf4304bbe1f9463bee21c2
msgid "To remedy this, the validation in ``is_safe_url()`` has been tightened to be able to handle and correctly validate these malformed URLs."
msgstr ""

