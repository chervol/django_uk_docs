# 
msgid ""
msgstr ""
"Project-Id-Version: Django 1.7\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2014-10-13 18:33+0300\n"
"Last-Translator: Volodymyr Cherepanyak <chervol@gmail.com>\n"
"Language-Team: Quintagroup <info@quintagroup.com>\n"
"Content-Transfer-Encoding: 8bit\n"" Content-Type: text/plain; charset=UTF-8\n"
"MIME-Version: 1.0\n"
"Language: uk\n"

# 9e99bc1d95314a049f1ab9a1be626dd3
#: ../../releases/1.4.15.txt:3
msgid "Django 1.4.15 release notes"
msgstr ""

# 3cf364a8cc584589af7a3a8e844fa3e1
#: ../../releases/1.4.15.txt:5
msgid "*Under development*"
msgstr ""

# b1dfa649b1c240c9b7a876d7d3e933de
#: ../../releases/1.4.15.txt:7
msgid "Django 1.4.15 fixes a regression in the 1.4.14 security release."
msgstr ""

# 8be51a09efba4609b9042f87e9d57230
#: ../../releases/1.4.15.txt:10
msgid "Bugfixes"
msgstr ""

# dc094c9eabab48e38868269a417dc9f2
#: ../../releases/1.4.15.txt:12
msgid ""
"Allowed inherited and m2m fields to be referenced in the admin (`#22486 "
"<http://code.djangoproject.com/ticket/23329>`_)"
msgstr ""
