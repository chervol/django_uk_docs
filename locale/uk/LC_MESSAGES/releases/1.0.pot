# SOME DESCRIPTIVE TITLE.
# Copyright (C) Django Software Foundation and contributors
# This file is distributed under the same license as the Django package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: Django 1.7\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2014-10-13 18:33+0300\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#: ../../releases/1.0.txt:3
# 203e3bf6b2e4457c9f3d217324652075
msgid "Django 1.0 release notes"
msgstr ""

#: ../../releases/1.0.txt:5
# 9d08ef6b1c5d4e0393caa767b6165437
msgid "Welcome to Django 1.0!"
msgstr ""

#: ../../releases/1.0.txt:7
# 77c47852a32e4c45b0f0efa55d77b70f
msgid "We've been looking forward to this moment for over three years, and it's finally here. Django 1.0 represents a the largest milestone in Django's development to date: a Web framework that a group of perfectionists can truly be proud of."
msgstr ""

#: ../../releases/1.0.txt:11
# 559acecc64dd4999813b5997aa965f9b
msgid "Django 1.0 represents over three years of community development as an Open Source project. Django's received contributions from hundreds of developers, been translated into fifty languages, and today is used by developers on every continent and in every kind of job."
msgstr ""

#: ../../releases/1.0.txt:16
# b909e961d4414c7bb28258dbee7bf4ef
msgid "An interesting historical note: when Django was first released in July 2005, the initial released version of Django came from an internal repository at revision number 8825. Django 1.0 represents revision 8961 of our public repository. It seems fitting that our 1.0 release comes at the moment where community contributions overtake those made privately."
msgstr ""

#: ../../releases/1.0.txt:23
# 8bfa7d911cc34d9091bee94c07f2dfc2
msgid "Stability and forwards-compatibility"
msgstr ""

#: ../../releases/1.0.txt:25
# cd22e730635648b9914b1e51ec086f1a
msgid ":doc:`The release of Django 1.0 </releases/1.0>` comes with a promise of API stability and forwards-compatibility. In a nutshell, this means that code you develop against Django 1.0 will continue to work against 1.1 unchanged, and you should need to make only minor changes for any 1.X release."
msgstr ""

#: ../../releases/1.0.txt:30
# ee131a8fe8c1476a8ff52e97bf9d025c
msgid "See the :doc:`API stability guide </misc/api-stability>` for full details."
msgstr ""

#: ../../releases/1.0.txt:33
# 0613b8d491744dcda361e955f77fca4d
msgid "Backwards-incompatible changes"
msgstr ""

#: ../../releases/1.0.txt:35
# 6770f844445040b0aa5bb306c8ad7a64
msgid "Django 1.0 has a number of backwards-incompatible changes from Django 0.96. If you have apps written against Django 0.96 that you need to port, see our detailed porting guide:"
msgstr ""

#: ../../releases/1.0.txt:44
# 3e6395d5719844d6a66e07a3d6e62d28
msgid "A complete list of backwards-incompatible changes can be found at https://code.djangoproject.com/wiki/BackwardsIncompatibleChanges."
msgstr ""

#: ../../releases/1.0.txt:48
# 60dbea4b231544a1ba2eeb47fde6c61e
msgid "What's new in Django 1.0"
msgstr ""

#: ../../releases/1.0.txt:50
# 000e14e612404f4aa4bda992f4d6e267
msgid "A *lot*!"
msgstr ""

#: ../../releases/1.0.txt:52
# db5ec20d950e439bb52bf9891cc1d638
msgid "Since Django 0.96, we've made over 4,000 code commits, fixed more than 2,000 bugs, and edited, added, or removed around 350,000 lines of code. We've also added 40,000 lines of new documentation, and greatly improved what was already there."
msgstr ""

#: ../../releases/1.0.txt:57
# bc283fc28cc74eb984fa5eff7ef3b58d
msgid "In fact, new documentation is one of our favorite features of Django 1.0, so we might as well start there. First, there's a new documentation site:"
msgstr ""

#: ../../releases/1.0.txt:60
# 0caf84cdbb944b87b1503c8eba04d7f2
msgid "https://docs.djangoproject.com/"
msgstr ""

#: ../../releases/1.0.txt:62
# b4026bb67e9f4da68842f04240b335be
msgid "The documentation has been greatly improved, cleaned up, and generally made awesome. There's now dedicated search, indexes, and more."
msgstr ""

#: ../../releases/1.0.txt:65
# db9875e0e6824706b063c4163087cdc0
msgid "We can't possibly document everything that's new in 1.0, but the documentation will be your definitive guide. Anywhere you see something like:"
msgstr ""

#: ../../releases/1.0.txt:70
# 92ee032f5c2545a6882926c61bec643a
msgid "This feature is new in Django 1.0"
msgstr ""

#: ../../releases/1.0.txt:72
# f28542312c354f9a898b3206e558b818
msgid "You'll know that you're looking at something new or changed."
msgstr ""

#: ../../releases/1.0.txt:74
# 4b76bba5ac6c46f48fa9a0a562e6f4ef
msgid "The other major highlights of Django 1.0 are:"
msgstr ""

#: ../../releases/1.0.txt:77
# 4bd004a659e5432ea05eb5b62b88be0e
msgid "Re-factored admin application"
msgstr ""

#: ../../releases/1.0.txt:79
# 02a0f02597a84e8b9f3f6ce2db888037
msgid "The Django administrative interface (``django.contrib.admin``) has been completely refactored; admin definitions are now completely decoupled from model definitions (no more ``class Admin`` declaration in models!), rewritten to use Django's new form-handling library (introduced in the 0.96 release as ``django.newforms``, and now available as simply ``django.forms``) and redesigned with extensibility and customization in mind. Full documentation for the admin application is available online in the official Django documentation:"
msgstr ""

#: ../../releases/1.0.txt:87
# c23991b34f7d4c6aaf4794aaf79a253a
msgid "See the :doc:`admin reference </ref/contrib/admin/index>` for details"
msgstr ""

#: ../../releases/1.0.txt:90
# d39875d828494346abe9fd542ff7fb73
msgid "Improved Unicode handling"
msgstr ""

#: ../../releases/1.0.txt:92
# 4bba72d3cf5d4677898ef60d893aa704
msgid "Django's internals have been refactored to use Unicode throughout; this drastically simplifies the task of dealing with non-Western-European content and data in Django. Additionally, utility functions have been provided to ease interoperability with third-party libraries and systems which may or may not handle Unicode gracefully. Details are available in Django's Unicode-handling documentation."
msgstr ""

#: ../../releases/1.0.txt:99
# 3e1e3a5fa1a54da89e2c93b1ee0eaf53
msgid "See :doc:`/ref/unicode`."
msgstr ""

#: ../../releases/1.0.txt:102
# e1b59c6a72104704bda873b21e0bb4e0
msgid "An improved ORM"
msgstr ""

#: ../../releases/1.0.txt:104
# e9723aabd4374eb897faadbe65011046
msgid "Django's object-relational mapper -- the component which provides the mapping between Django model classes and your database, and which mediates your database queries -- has been dramatically improved by a massive refactoring. For most users of Django this is backwards-compatible; the public-facing API for database querying underwent a few minor changes, but most of the updates took place in the ORM's internals. A guide to the changes, including backwards-incompatible modifications and mentions of new features opened up by this refactoring, is `available on the Django wiki`__."
msgstr ""

#: ../../releases/1.0.txt:116
# aaeb666d7a4d4bcb988de7ccd501f096
msgid "Automatic escaping of template variables"
msgstr ""

#: ../../releases/1.0.txt:118
# cab9056cd78345569caacd95897239f0
msgid "To provide improved security against cross-site scripting (XSS) vulnerabilities, Django's template system now automatically escapes the output of variables. This behavior is configurable, and allows both variables and larger template constructs to be marked as safe (requiring no escaping) or unsafe (requiring escaping). A full guide to this feature is in the documentation for the :ttag:`autoescape` tag."
msgstr ""

#: ../../releases/1.0.txt:126
# 45cb9dfe2b4641769839aeade8423890
msgid "``django.contrib.gis`` (GeoDjango)"
msgstr ""

#: ../../releases/1.0.txt:128
# d50ae944a4d04210966595eafc33e58d
msgid "A project over a year in the making, this adds world-class GIS (`Geographic Information Systems`_) support to Django, in the form of a ``contrib`` application. Its documentation is currently being maintained externally, and will be merged into the main Django documentation shortly. Huge thanks go to Justin Bronn, Jeremy Dunck, Brett Hoerner and Travis Pinney for their efforts in creating and completing this feature."
msgstr ""

#: ../../releases/1.0.txt:135
# 4ac68accff014d9597671308e00fee9f
msgid "See http://geodjango.org/ for details."
msgstr ""

#: ../../releases/1.0.txt:140
# 46fc0ed1298943f5926608fdcb06ac7a
msgid "Pluggable file storage"
msgstr ""

#: ../../releases/1.0.txt:142
# b890ed18985e4044990296a3df217876
msgid "Django's built-in ``FileField`` and ``ImageField`` now can take advantage of pluggable file-storage backends, allowing extensive customization of where and how uploaded files get stored by Django. For details, see :doc:`the files documentation </topics/files>`; big thanks go to Marty Alchin for putting in the hard work to get this completed."
msgstr ""

#: ../../releases/1.0.txt:149
# b32a7659c0ff460ba31a8bf1a07f5ce1
msgid "Jython compatibility"
msgstr ""

#: ../../releases/1.0.txt:151
# dd66afe128fe4573b45653bb74d4b9c8
msgid "Thanks to a lot of work from Leo Soto during a Google Summer of Code project, Django's codebase has been refactored to remove incompatibilities with `Jython`_, an implementation of Python written in Java, which runs Python code on the Java Virtual Machine. Django is now compatible with the forthcoming Jython 2.5 release."
msgstr ""

#: ../../releases/1.0.txt:157
# 3038eabad2354097a8800fd77e050e3b
msgid "See :doc:`/howto/jython`."
msgstr ""

#: ../../releases/1.0.txt:162
# f24a66e7766d4bd683e3acd09e212442
msgid "Generic relations in forms and admin"
msgstr ""

#: ../../releases/1.0.txt:164
# 614b53e6a6cf4f3bbea32a82d7a6f31d
msgid "Classes are now included in ``django.contrib.contenttypes`` which can be used to support generic relations in both the admin interface and in end-user forms. See :ref:`the documentation for generic relations <generic-relations>` for details."
msgstr ""

#: ../../releases/1.0.txt:169
# 46be48b33dc0479199557927b930caf2
msgid "``INSERT``/``UPDATE`` distinction"
msgstr ""

#: ../../releases/1.0.txt:171
# c7446fac199d45e181c6b779937f390c
msgid "Although Django's default behavior of having a model's ``save()`` method automatically determine whether to perform an ``INSERT`` or an ``UPDATE`` at the SQL level is suitable for the majority of cases, there are occasional situations where forcing one or the other is useful. As a result, models can now support an additional parameter to ``save()`` which can force a specific operation."
msgstr ""

#: ../../releases/1.0.txt:177
# 39d021e3c6e94a23bfee497e24dd6c8e
msgid "See :ref:`ref-models-force-insert` for details."
msgstr ""

#: ../../releases/1.0.txt:180
# 1b6bb0dd018f480a9995eab7d429b110
msgid "Split ``CacheMiddleware``"
msgstr ""

#: ../../releases/1.0.txt:182
# aa6057ea5e724184b86d8ae3a136d949
msgid "Django's ``CacheMiddleware`` has been split into three classes: ``CacheMiddleware`` itself still exists and retains all of its previous functionality, but it is now built from two separate middleware classes which handle the two parts of caching (inserting into and reading from the cache) separately, offering additional flexibility for situations where combining these functions into a single middleware posed problems."
msgstr ""

#: ../../releases/1.0.txt:189
# 1851e99ea57f4ff2939e54482a4a288b
msgid "Full details, including updated notes on appropriate use, are in :doc:`the caching documentation </topics/cache>`."
msgstr ""

#: ../../releases/1.0.txt:193
# 6d58fc9b62224b7da85a371a05a8dd3a
msgid "Refactored ``django.contrib.comments``"
msgstr ""

#: ../../releases/1.0.txt:195
# 529f56ae2b6844479c3751fd2c395361
msgid "As part of a Google Summer of Code project, Thejaswi Puthraya carried out a major rewrite and refactoring of Django's bundled comment system, greatly increasing its flexibility and customizability. :doc:`Full documentation </ref/contrib/comments/index>` is available, as well as an upgrade guide if you were using the previous incarnation of the comments application."
msgstr ""

#: ../../releases/1.0.txt:203
# dbf4039d6fab4397a350cc860d0a2287
msgid "Removal of deprecated features"
msgstr ""

#: ../../releases/1.0.txt:205
# 87e6c54e775542bfa7d0cc096ab47307
msgid "A number of features and methods which had previously been marked as deprecated, and which were scheduled for removal prior to the 1.0 release, are no longer present in Django. These include imports of the form library from ``django.newforms`` (now located simply at ``django.forms``), the ``form_for_model`` and ``form_for_instance`` helper functions (which have been replaced by ``ModelForm``) and a number of deprecated features which were replaced by the dispatcher, file-uploading and file-storage refactorings introduced in the Django 1.0 alpha releases."
msgstr ""

#: ../../releases/1.0.txt:215
# 043d33fd6db7414b81905e906f8f3303
msgid "Known issues"
msgstr ""

#: ../../releases/1.0.txt:217
# 73217f9828304443ac7b4ef41c98f6d3
msgid "We've done our best to make Django 1.0 as solid as possible, but unfortunately there are a couple of issues that we know about in the release."
msgstr ""

#: ../../releases/1.0.txt:221
# 859318ee63194dc796febbab93e79d0a
msgid "Multi-table model inheritance with ``to_field``"
msgstr ""

#: ../../releases/1.0.txt:223
# 5d11eede5af648399c4282cd473aff98
msgid "If you're using :ref:`multiple table model inheritance <multi-table-inheritance>`, be aware of this caveat: child models using a custom ``parent_link`` and ``to_field`` will cause database integrity errors. A set of models like the following are **not valid**::"
msgstr ""

#: ../../releases/1.0.txt:236
# ecda60c6027b40d0bcb46eee6f284fe9
msgid "This bug will be fixed in the next release of Django."
msgstr ""

#: ../../releases/1.0.txt:239
# 42822fc0a79a49a38b214f88c1e255a2
msgid "Caveats with support of certain databases"
msgstr ""

#: ../../releases/1.0.txt:241
# 8cab1025571b4c80acbbba255b0b30eb
msgid "Django attempts to support as many features as possible on all database backends. However, not all database backends are alike, and in particular many of the supported database differ greatly from version to version. It's a good idea to checkout our :doc:`notes on supported database </ref/databases>`:"
msgstr ""

#: ../../releases/1.0.txt:244
# 4d404f286c5b446ab73dc1fa3e90c35c
msgid ":ref:`mysql-notes`"
msgstr ""

#: ../../releases/1.0.txt:245
# 5b04e8909d8c4860be8182e2d35e2d2b
msgid ":ref:`sqlite-notes`"
msgstr ""

#: ../../releases/1.0.txt:246
# 9a8fd70de1fb40468bc777f2d72706d8
msgid ":ref:`oracle-notes`"
msgstr ""

