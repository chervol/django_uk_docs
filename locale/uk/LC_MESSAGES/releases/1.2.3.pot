# SOME DESCRIPTIVE TITLE.
# Copyright (C) Django Software Foundation and contributors
# This file is distributed under the same license as the Django package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: Django 1.7\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2014-10-13 18:33+0300\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#: ../../releases/1.2.3.txt:3
# f716d261c073429b9c18854b9984306f
msgid "Django 1.2.3 release notes"
msgstr ""

#: ../../releases/1.2.3.txt:5
# 0b434560ae11487fa68f15f7b1691b92
msgid "Django 1.2.3 fixed a couple of release problems in the 1.2.2 release and was released two days after 1.2.2."
msgstr ""

#: ../../releases/1.2.3.txt:8
# c095121ea05c44f794ccb9b7615653a2
msgid "This release corrects the following problems:"
msgstr ""

#: ../../releases/1.2.3.txt:10
# 29e25822102a43c7b6bac7c3f7c99b9a
msgid "The patch_ applied for the security issue covered in Django 1.2.2 caused issues with non-ASCII responses using CSRF tokens."
msgstr ""

#: ../../releases/1.2.3.txt:13
# 901a029dd5c74939ad1d275cf4e43fde
msgid "The patch also caused issues with some forms, most notably the user-editing forms in the Django administrative interface."
msgstr ""

#: ../../releases/1.2.3.txt:16
# 68b33197ffc0407c99f74b53bede140d
msgid "The packaging manifest did not contain the full list of required files."
msgstr ""

