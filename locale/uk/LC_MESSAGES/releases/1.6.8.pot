# SOME DESCRIPTIVE TITLE.
# Copyright (C) Django Software Foundation and contributors
# This file is distributed under the same license as the Django package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
# 
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: Django 1.8\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2014-10-08 21:09+0300\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: Quintagroup <info@quintagroup.com>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#: ../../releases/1.6.8.txt:3
msgid "Django 1.6.8 release notes"
msgstr ""

#: ../../releases/1.6.8.txt:5
msgid "*Under development*"
msgstr ""

#: ../../releases/1.6.8.txt:7
msgid "Django 1.6.8 fixes a couple regressions in the 1.6.6 security release."
msgstr ""

#: ../../releases/1.6.8.txt:10
msgid "Bugfixes"
msgstr ""

#: ../../releases/1.6.8.txt:12
msgid ""
"Allowed related many-to-many fields to be referenced in the admin "
"(:ticket:`23604`)."
msgstr ""

#: ../../releases/1.6.8.txt:15
msgid ""
"Allowed inline and hidden references to admin fields (:ticket:`23431`)."
msgstr ""
