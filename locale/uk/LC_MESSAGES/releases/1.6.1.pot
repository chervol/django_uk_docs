# SOME DESCRIPTIVE TITLE.
# Copyright (C) Django Software Foundation and contributors
# This file is distributed under the same license as the Django package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: Django 1.7\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2014-10-13 18:33+0300\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#: ../../releases/1.6.1.txt:3
# 8b2a388cbc054a1993bece743429a2d3
msgid "Django 1.6.1 release notes"
msgstr ""

#: ../../releases/1.6.1.txt:5
# 4b72ef5b38a9444eb82ed31ec5226350
msgid "*December 12, 2013*"
msgstr ""

#: ../../releases/1.6.1.txt:7
# e5bf106dc3824e93a1fed67cef944ba9
msgid "This is Django 1.6.1, a bugfix release for Django 1.6. In addition to the bug fixes listed below, translations submitted since the 1.6 release are also included."
msgstr ""

#: ../../releases/1.6.1.txt:12
# 7922acc371d44a52a50d08c9eb9438a8
msgid "Bug fixes"
msgstr ""

#: ../../releases/1.6.1.txt:14
# c9f7e2a383334b669828de196e6c5a8e
msgid "Fixed ``BCryptSHA256PasswordHasher`` with py-bcrypt and Python 3 (#21398)."
msgstr ""

#: ../../releases/1.6.1.txt:15
# 3d05b26d8e504f7ca1e227467936969d
msgid "Fixed a regression that prevented a ``ForeignKey`` with a hidden reverse manager (``related_name`` ending with '+') from being used as a lookup for ``prefetch_related`` (#21410)."
msgstr ""

#: ../../releases/1.6.1.txt:18
# bc29179ad2b7460c9a158ceb639a60a7
msgid "Fixed :meth:`Queryset.datetimes<django.db.models.query.QuerySet.datetimes>` raising ``AttributeError`` in some situations (#21432)."
msgstr ""

#: ../../releases/1.6.1.txt:20
# fd9592816528483baebd4581e36329d1
msgid "Fixed :class:`~django.contrib.auth.backends.ModelBackend` raising ``UnboundLocalError`` if :func:`~django.contrib.auth.get_user_model` raised an error (#21439)."
msgstr ""

#: ../../releases/1.6.1.txt:23
# 25c86e4e9608492e868de415e0e59aa8
msgid "Fixed a regression that prevented editable ``GenericRelation`` subclasses from working in ``ModelForms`` (#21428)."
msgstr ""

#: ../../releases/1.6.1.txt:25
# 168e5df755534b92b401c35d94aa3394
msgid "Added missing ``to_python`` method for ``ModelMultipleChoiceField`` which is required in Django 1.6 to properly detect changes from initial values (#21568)."
msgstr ""

#: ../../releases/1.6.1.txt:28
# b8623cc2e21b4879987fe785ee13626b
msgid "Fixed ``django.contrib.humanize`` translations where the unicode sequence for the non-breaking space was returned verbatim (#21415)."
msgstr ""

#: ../../releases/1.6.1.txt:30
# 65d92771094d4070b520aa8b2551f700
msgid "Fixed :djadmin:`loaddata` error when fixture file name contained any dots not related to file extensions (#21457) or when fixture path was relative but located in a subdirectory (#21551)."
msgstr ""

#: ../../releases/1.6.1.txt:33
# 41bd51ef93b54823bb8142dc805574dd
msgid "Fixed display of inline instances in formsets when parent has 0 for primary key (#21472)."
msgstr ""

#: ../../releases/1.6.1.txt:35
# 7875c040bc4544a1b2ae80f4a40583a5
msgid "Fixed a regression where custom querysets for foreign keys were overwritten if ``ModelAdmin`` had ordering set (#21405)."
msgstr ""

#: ../../releases/1.6.1.txt:37
# 43a969c7eec1419b9f07a16587193df5
msgid "Removed mention of a feature in the ``--locale``/``-l`` option of the ``makemessages`` and ``compilemessages`` commands that never worked as promised: Support of multiple locale names separated by commas. It's still possible to specify multiple locales in one run by using the option multiple times (#21488, #17181)."
msgstr ""

#: ../../releases/1.6.1.txt:42
# 7ae3d99911664eedbb23d33b05659fcc
msgid "Fixed a regression that unnecessarily triggered settings configuration when importing ``get_wsgi_application`` (#21486)."
msgstr ""

#: ../../releases/1.6.1.txt:44
# 5fff90327cf249628cb8c9f189e79b30
msgid "Fixed test client ``logout()`` method when using the cookie-based session backend (#21448)."
msgstr ""

#: ../../releases/1.6.1.txt:46
# 03688014c45a4467bd844fc49e6336be
msgid "Fixed a crash when a ``GeometryField`` uses a non-geometric widget (#21496)."
msgstr ""

#: ../../releases/1.6.1.txt:47
# f28c9f3df8f5409e8d20f617f7fbac4d
msgid "Fixed password hash upgrade when changing the iteration count (#21535)."
msgstr ""

#: ../../releases/1.6.1.txt:48
# 0fba492e02f144a8987e82ae7e93ab87
msgid "Fixed a bug in the debug view when the URLconf only contains one element (#21530)."
msgstr ""

#: ../../releases/1.6.1.txt:50
# 2d794f77a3754a50af62d03066eeff2a
msgid "Re-added missing search result count and reset link in changelist admin view (#21510)."
msgstr ""

#: ../../releases/1.6.1.txt:52
# 68c3fcaba939467286b457c92145ff24
msgid "The current language is no longer saved to the session by ``LocaleMiddleware`` on every response, but rather only after a logout (#21473)."
msgstr ""

#: ../../releases/1.6.1.txt:54
# 4d1e48dc33bd4ba0a834bdc048adefa8
msgid "Fixed a crash when executing ``runserver`` on non-English systems and when the formatted date in its output contained non-ASCII characters (#21358)."
msgstr ""

#: ../../releases/1.6.1.txt:56
# c28b0ed543a0455db4c90342ae3132aa
msgid "Fixed a crash in the debug view after an exception occurred on Python ≥ 3.3 (#21443)."
msgstr ""

#: ../../releases/1.6.1.txt:58
# bbb0ef4ace2a485a9f334059acdcf761
msgid "Fixed a crash in :class:`~django.db.models.ImageField` on some platforms (Homebrew and RHEL6 reported) (#21355)."
msgstr ""

#: ../../releases/1.6.1.txt:60
# b7aa93d547824aa18eae4354827bf339
msgid "Fixed a regression when using generic relations in ``ModelAdmin.list_filter`` (#21431)."
msgstr ""

