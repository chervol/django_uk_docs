# SOME DESCRIPTIVE TITLE.
# Copyright (C) Django Software Foundation and contributors
# This file is distributed under the same license as the Django package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: Django 1.7\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2014-10-13 18:33+0300\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#: ../../releases/0.95.txt:3
# 1a7c0f05b7d54a5e8639c38e5e8a5d2a
msgid "Django version 0.95 release notes"
msgstr ""

#: ../../releases/0.95.txt:5
# 9fed0873058d4ef48e016f4feacba8b9
msgid "Welcome to the Django 0.95 release."
msgstr ""

#: ../../releases/0.95.txt:7
# 46f6b189a84f4c1d82f7f51fd5a33f21
msgid "This represents a significant advance in Django development since the 0.91 release in January 2006. The details of every change in this release would be too extensive to list in full, but a summary is presented below."
msgstr ""

#: ../../releases/0.95.txt:12
# b14ceb4d7a1647289d0fbbfe728a9152
msgid "Suitability and API stability"
msgstr ""

#: ../../releases/0.95.txt:14
# 49583fbc901e4584a2b1d661811cfc78
msgid "This release is intended to provide a stable reference point for developers wanting to work on production-level applications that use Django."
msgstr ""

#: ../../releases/0.95.txt:17
# cd0c9799d8454b30a568969f0fb0095b
msgid "However, it's not the 1.0 release, and we'll be introducing further changes before 1.0. For a clear look at which areas of the framework will change (and which ones will *not* change) before 1.0, see the ``api-stability.txt`` file, which lives in the docs/ directory of the distribution."
msgstr ""

#: ../../releases/0.95.txt:22
# 39e9e6a63f2046e8a7a7bb08b77c4eef
msgid "You may have a need to use some of the features that are marked as \"subject to API change\" in that document, but that's OK with us as long as it's OK with you, and as long as you understand APIs may change in the future."
msgstr ""

#: ../../releases/0.95.txt:26
# cb79de1b23c24ddeb894838977946541
msgid "Fortunately, most of Django's core APIs won't be changing before version 1.0. There likely won't be as big of a change between 0.95 and 1.0 versions as there was between 0.91 and 0.95."
msgstr ""

#: ../../releases/0.95.txt:31
# 0bc2a3d87c3e48ce9716e77acb32a981
msgid "Changes and new features"
msgstr ""

#: ../../releases/0.95.txt:33
# 9087f3daca7f4974b5fc68db5ac48eeb
msgid "The major changes in this release (for developers currently using the 0.91 release) are a result of merging the 'magic-removal' branch of development. This branch removed a number of constraints in the way Django code had to be written that were a consequence of decisions made in the early days of Django, prior to its open-source release. It's now possible to write more natural, Pythonic code that works as expected, and there's less \"black magic\" happening behind the scenes."
msgstr ""

#: ../../releases/0.95.txt:41
# 7baca32620934e44aeb3bd67888efa58
msgid "Aside from that, another main theme of this release is a dramatic increase in usability. We've made countless improvements in error messages, documentation, etc., to improve developers' quality of life."
msgstr ""

#: ../../releases/0.95.txt:45
# 516cab309a2b4b198acac7dcb80bc28a
msgid "The new features and changes introduced in 0.95 include:"
msgstr ""

#: ../../releases/0.95.txt:47
# 23e2b852bd3e4a3ab194abc2113b5086
msgid "Django now uses a more consistent and natural filtering interface for retrieving objects from the database."
msgstr ""

#: ../../releases/0.95.txt:50
# ea7b37d1baaa4cfb9f3c9dffb58a7329
msgid "User-defined models, functions and constants now appear in the module namespace they were defined in. (Previously everything was magically transferred to the django.models.* namespace.)"
msgstr ""

#: ../../releases/0.95.txt:54
# 9cc77616bff841578cc713602e4b8a07
msgid "Some optional applications, such as the FlatPage, Sites and Redirects apps, have been decoupled and moved into django.contrib. If you don't want to use these applications, you no longer have to install their database tables."
msgstr ""

#: ../../releases/0.95.txt:59
# 7b47dc6fd1dc437f9068bdfe4c7d7768
msgid "Django now has support for managing database transactions."
msgstr ""

#: ../../releases/0.95.txt:61
# 69ddb9c4438844af9bc74e2ebc4188b9
msgid "We've added the ability to write custom authentication and authorization backends for authenticating users against alternate systems, such as LDAP."
msgstr ""

#: ../../releases/0.95.txt:65
# 928cbb7bd79344d2b47abf62cfd0f233
msgid "We've made it easier to add custom table-level functions to models, through a new \"Manager\" API."
msgstr ""

#: ../../releases/0.95.txt:68
# 132cb6b5f8b1408abf3276e7abaeaa0d
msgid "It's now possible to use Django without a database. This simply means that the framework no longer requires you to have a working database set up just to serve dynamic pages. In other words, you can just use URLconfs/views on their own. Previously, the framework required that a database be configured, regardless of whether you actually used it."
msgstr ""

#: ../../releases/0.95.txt:74
# a842508bec5f4c82a4eac2bc0f52da6f
msgid "It's now more explicit and natural to override save() and delete() methods on models, rather than needing to hook into the pre_save() and post_save() method hooks."
msgstr ""

#: ../../releases/0.95.txt:78
# 7067af95c2e04e01ba1f9f6e193bb9fd
msgid "Individual pieces of the framework now can be configured without requiring the setting of an environment variable. This permits use of, for example, the Django templating system inside other applications."
msgstr ""

#: ../../releases/0.95.txt:82
# a5638f3e3800455da38494500434bb1a
msgid "More and more parts of the framework have been internationalized, as we've expanded internationalization (i18n) support. The Django codebase, including code and templates, has now been translated, at least in part, into 31 languages. From Arabic to Chinese to Hungarian to Welsh, it is now possible to use Django's admin site in your native language."
msgstr ""

#: ../../releases/0.95.txt:88
# e42e17c85da24a0ab85c6263ebde8953
msgid "The number of changes required to port from 0.91-compatible code to the 0.95 code base are significant in some cases. However, they are, for the most part, reasonably routine and only need to be done once. A list of the necessary changes is described in the `Removing The Magic`_ wiki page. There is also an easy checklist_ for reference when undertaking the porting operation."
msgstr ""

#: ../../releases/0.95.txt:98
# e5c0b0b4cc1a4ea7a68c4b7705eda448
msgid "Problem reports and getting help"
msgstr ""

#: ../../releases/0.95.txt:100
# f88b4ecf32f643ac96941503455d409e
msgid "Need help resolving a problem with Django? The documentation in the distribution is also available :doc:`online </index>` at the `Django Web site`_. The :doc:`FAQ </faq/index>` document is especially recommended, as it contains a number of issues that come up time and again."
msgstr ""

#: ../../releases/0.95.txt:105
# 570d78aee5d84605843b398fa7abb899
msgid "For more personalized help, the `django-users`_ mailing list is a very active list, with more than 2,000 subscribers who can help you solve any sort of Django problem. We recommend you search the archives first, though, because many common questions appear with some regularity, and any particular problem may already have been answered."
msgstr ""

#: ../../releases/0.95.txt:111
# bb1f55803c3d42b29713349bbdd051c3
msgid "Finally, for those who prefer the more immediate feedback offered by IRC, there's a #django channel on irc.freenode.net that is regularly populated by Django users and developers from around the world. Friendly people are usually available at any hour of the day -- to help, or just to chat."
msgstr ""

#: ../../releases/0.95.txt:119
# b255774b233b4b0b8769ca63931f30b6
msgid "Thanks for using Django!"
msgstr ""

#: ../../releases/0.95.txt:121
# b5d85cfe0e9e4a55b5ec4ca156f36cbf
msgid "The Django Team July 2006"
msgstr ""

