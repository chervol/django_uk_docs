# SOME DESCRIPTIVE TITLE.
# Copyright (C) Django Software Foundation and contributors
# This file is distributed under the same license as the Django package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: Django 1.7\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2014-10-13 18:33+0300\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#: ../../releases/1.4.2.txt:3
# 3b1981605eb34b1cbc48b12ebccb78b4
msgid "Django 1.4.2 release notes"
msgstr ""

#: ../../releases/1.4.2.txt:5
# 56ad1fb134804b0f930644164b9d8de2
msgid "*October 17, 2012*"
msgstr ""

#: ../../releases/1.4.2.txt:7
# d2041ad3ea204a2ea701393c0c422920
msgid "This is the second security release in the Django 1.4 series."
msgstr ""

#: ../../releases/1.4.2.txt:10
# 5d061fdf8e7d44448bc2f4eebb0cdade
msgid "Host header poisoning"
msgstr ""

#: ../../releases/1.4.2.txt:12
# 5ac9a129a342482bbf9bc7039cb4237a
msgid "Some parts of Django -- independent of end-user-written applications -- make use of full URLs, including domain name, which are generated from the HTTP Host header. Some attacks against this are beyond Django's ability to control, and require the web server to be properly configured; Django's documentation has for some time contained notes advising users on such configuration."
msgstr ""

#: ../../releases/1.4.2.txt:18
# b67aa91f256d492188970746db3c2d85
msgid "Django's own built-in parsing of the Host header is, however, still vulnerable, as was reported to us recently. The Host header parsing in Django 1.3.3 and Django 1.4.1 -- specifically, ``django.http.HttpRequest.get_host()`` -- was incorrectly handling username/password information in the header. Thus, for example, the following Host header would be accepted by Django when running on \"validsite.com\"::"
msgstr ""

#: ../../releases/1.4.2.txt:27
# 3e1cd8b66a5847c4b4a0c70a13642448
msgid "Using this, an attacker can cause parts of Django -- particularly the password-reset mechanism -- to generate and display arbitrary URLs to users."
msgstr ""

#: ../../releases/1.4.2.txt:30
# ef299d303fd347548585c64036fcc39d
msgid "To remedy this, the parsing in ``HttpRequest.get_host()`` is being modified; Host headers which contain potentially dangerous content (such as username/password pairs) now raise the exception :exc:`django.core.exceptions.SuspiciousOperation`."
msgstr ""

#: ../../releases/1.4.2.txt:35
# d8af70d7c0c84d1faab6bc06f488e4d6
msgid "Details of this issue were initially posted online as a `security advisory`_."
msgstr ""

#: ../../releases/1.4.2.txt:40
# 1964b3d769e54ea298f347fad347cdcf
msgid "Backwards incompatible changes"
msgstr ""

#: ../../releases/1.4.2.txt:42
# d334c57d5f824917bf5b5bb0ce1a3e6b
msgid "The newly introduced :class:`~django.db.models.GenericIPAddressField` constructor arguments have been adapted to match those of all other model fields. The first two keyword arguments are now verbose_name and name."
msgstr ""

#: ../../releases/1.4.2.txt:47
# cdfde532bd254bda8ee7a4fde923786e
msgid "Other bugfixes and changes"
msgstr ""

#: ../../releases/1.4.2.txt:49
# 675e1555fd5f4839a847751ae555d659
msgid "Subclass HTMLParser only for appropriate Python versions (#18239)."
msgstr ""

#: ../../releases/1.4.2.txt:50
# 5cfc18410b8e4070a27c73dab7fbd218
msgid "Added batch_size argument to qs.bulk_create() (#17788)."
msgstr ""

#: ../../releases/1.4.2.txt:51
# e3e7668415024f51967e2d2b705edcb8
msgid "Fixed a small regression in the admin filters where wrongly formatted dates passed as url parameters caused an unhandled ValidationError (#18530)."
msgstr ""

#: ../../releases/1.4.2.txt:52
# 5918af9ad75b4130b0b3324b6e0904e3
msgid "Fixed an endless loop bug when accessing permissions in templates (#18979)"
msgstr ""

#: ../../releases/1.4.2.txt:53
# 32ab63cc2e1d4e52a7d877fe4be782b7
msgid "Fixed some Python 2.5 compatibility issues"
msgstr ""

#: ../../releases/1.4.2.txt:54
# a3ffa57bfdc74025b68ba7ba1ccd4de8
msgid "Fixed an issue with quoted filenames in Content-Disposition header (#19006)"
msgstr ""

#: ../../releases/1.4.2.txt:55
# 2f9ab064bef141cfa902fb25771530dc
msgid "Made the context option in ``trans`` and ``blocktrans`` tags accept literals wrapped in single quotes (#18881)."
msgstr ""

#: ../../releases/1.4.2.txt:56
# 0b6bdb0c74734312ba0a7b58719e87ec
msgid "Numerous documentation improvements and fixes."
msgstr ""

