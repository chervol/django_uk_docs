# SOME DESCRIPTIVE TITLE.
# Copyright (C) Django Software Foundation and contributors
# This file is distributed under the same license as the Django package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: Django 1.7\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2014-10-13 18:33+0300\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#: ../../releases/1.3.1.txt:3
# 95da1126a4fd4b6195b6ac5bb5c6c28f
msgid "Django 1.3.1 release notes"
msgstr ""

#: ../../releases/1.3.1.txt:5
# b4b9e6378fe04add9ed765bb0ddae7c6
msgid "*September 9, 2011*"
msgstr ""

#: ../../releases/1.3.1.txt:7
# 536e0be48db1431fa0f4e0e2f8abcac1
msgid "Welcome to Django 1.3.1!"
msgstr ""

#: ../../releases/1.3.1.txt:9
# ca06dddfb0fa4c9ead87dcbfb25553d3
msgid "This is the first security release in the Django 1.3 series, fixing several security issues in Django 1.3.  Django 1.3.1 is a recommended upgrade for all users of Django 1.3."
msgstr ""

#: ../../releases/1.3.1.txt:13
# ceca7a335715454d8b5541a916bb9c56
msgid "For a full list of issues addressed in this release, see the `security advisory`_."
msgstr ""

