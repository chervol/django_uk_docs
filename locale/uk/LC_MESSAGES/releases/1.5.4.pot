# SOME DESCRIPTIVE TITLE.
# Copyright (C) Django Software Foundation and contributors
# This file is distributed under the same license as the Django package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: Django 1.7\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2014-10-13 18:33+0300\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#: ../../releases/1.5.4.txt:3
# c57b5ca0eb914fb997c4e90843dbd795
msgid "Django 1.5.4 release notes"
msgstr ""

#: ../../releases/1.5.4.txt:5
# 9d0b67085f3c4a71a7ae625fb10d2e18
msgid "*September 14, 2013*"
msgstr ""

#: ../../releases/1.5.4.txt:7
# 3ea9510624fe408dab55be81f78c3faf
msgid "This is Django 1.5.4, the fourth release in the Django 1.5 series. It addresses two security issues and one bug."
msgstr ""

#: ../../releases/1.5.4.txt:11
# c83cb567c2b1459c8b6ee38fd4aea54a
msgid "Denial-of-service via password hashers"
msgstr ""

#: ../../releases/1.5.4.txt:13
# 681c7a293a8245eda3ab4642d11e5a4a
msgid "In previous versions of Django, no limit was imposed on the plaintext length of a password. This allowed a denial-of-service attack through submission of bogus but extremely large passwords, tying up server resources performing the (expensive, and increasingly expensive with the length of the password) calculation of the corresponding hash."
msgstr ""

#: ../../releases/1.5.4.txt:19
# 1b6cea871fbe47da9d55829e2ad1527f
msgid "As of 1.5.4, Django's authentication framework imposes a 4096-byte limit on passwords, and will fail authentication with any submitted password of greater length."
msgstr ""

#: ../../releases/1.5.4.txt:24
# 98ab7ddf61cd46778dee0710cce914c8
msgid "Corrected usage of :func:`~django.views.decorators.debug.sensitive_post_parameters` in :mod:`django.contrib.auth`’s admin"
msgstr ""

#: ../../releases/1.5.4.txt:26
# 03f4d1cfa6ad43138e86b22834240914
msgid "The decoration of the ``add_view`` and ``user_change_password`` user admin views with :func:`~django.views.decorators.debug.sensitive_post_parameters` did not include :func:`~django.utils.decorators.method_decorator` (required since the views are methods) resulting in the decorator not being properly applied. This usage has been fixed and :func:`~django.views.decorators.debug.sensitive_post_parameters` will now throw an exception if it's improperly used."
msgstr ""

#: ../../releases/1.5.4.txt:35
# 6df26f29a93047b2ab781bbc994243ea
msgid "Bugfixes"
msgstr ""

#: ../../releases/1.5.4.txt:37
# 3e3de2787cb74ec59e9127da43a05de1
msgid "Fixed a bug that prevented a ``QuerySet`` that uses :meth:`~django.db.models.query.QuerySet.prefetch_related` from being pickled and unpickled more than once (the second pickling attempt raised an exception) (#21102)."
msgstr ""

