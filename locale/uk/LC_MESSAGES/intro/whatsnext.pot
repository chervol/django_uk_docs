# SOME DESCRIPTIVE TITLE.
# Copyright (C) Django Software Foundation and contributors
# This file is distributed under the same license as the Django package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: Django 1.7\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2014-10-13 18:33+0300\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#: ../../intro/whatsnext.txt:3
# e81adaa627354a0b92853a696a28a998
msgid "What to read next"
msgstr ""

#: ../../intro/whatsnext.txt:5
# 4a031e5f0c33471a8990a54641f2818b
msgid "So you've read all the :doc:`introductory material </intro/index>` and have decided you'd like to keep using Django. We've only just scratched the surface with this intro (in fact, if you've read every single word, you've read about 5% of the overall documentation)."
msgstr ""

#: ../../intro/whatsnext.txt:10
# cfd79c1eb7d4418cb26040a0d244c112
msgid "So what's next?"
msgstr ""

#: ../../intro/whatsnext.txt:12
# fe963629ad4841738b35a8fef31b253c
msgid "Well, we've always been big fans of learning by doing. At this point you should know enough to start a project of your own and start fooling around. As you need to learn new tricks, come back to the documentation."
msgstr ""

#: ../../intro/whatsnext.txt:16
# f44a65651b1942db82499d50e47ac298
msgid "We've put a lot of effort into making Django's documentation useful, easy to read and as complete as possible. The rest of this document explains more about how the documentation works so that you can get the most out of it."
msgstr ""

#: ../../intro/whatsnext.txt:20
# 52f2fcee2dc44589a0f6a063d6cde076
msgid "(Yes, this is documentation about documentation. Rest assured we have no plans to write a document about how to read the document about documentation.)"
msgstr ""

#: ../../intro/whatsnext.txt:24
# 1336d1063ed14e51bb3f003084ca564c
msgid "Finding documentation"
msgstr ""

#: ../../intro/whatsnext.txt:26
# 016b2b2046c6473da9fb00cefea43b08
msgid "Django's got a *lot* of documentation -- almost 450,000 words and counting -- so finding what you need can sometimes be tricky. A few good places to start are the :ref:`search` and the :ref:`genindex`."
msgstr ""

#: ../../intro/whatsnext.txt:30
# 80288b31a8b84ca9a84319a282285df8
msgid "Or you can just browse around!"
msgstr ""

#: ../../intro/whatsnext.txt:33
# e482bce716114365aaa3d6bf2cb642e0
msgid "How the documentation is organized"
msgstr ""

#: ../../intro/whatsnext.txt:35
# 4a70f0a3ff644c8e84d9d3ee7caac31d
msgid "Django's main documentation is broken up into \"chunks\" designed to fill different needs:"
msgstr ""

#: ../../intro/whatsnext.txt:38
# 316ee59acb274336aaac21d7a7a24764
msgid "The :doc:`introductory material </intro/index>` is designed for people new to Django -- or to Web development in general. It doesn't cover anything in depth, but instead gives a high-level overview of how developing in Django \"feels\"."
msgstr ""

#: ../../intro/whatsnext.txt:43
# ae7ad383ab344610908a30c9bbd9e5cd
msgid "The :doc:`topic guides </topics/index>`, on the other hand, dive deep into individual parts of Django. There are complete guides to Django's :doc:`model system </topics/db/index>`, :doc:`template engine </topics/templates>`, :doc:`forms framework </topics/forms/index>`, and much more."
msgstr ""

#: ../../intro/whatsnext.txt:49
# 3b79ab66e1684286841ae623704f9dea
msgid "This is probably where you'll want to spend most of your time; if you work your way through these guides you should come out knowing pretty much everything there is to know about Django."
msgstr ""

#: ../../intro/whatsnext.txt:53
# a230f75e307c4e1fbc16fb22984fb193
msgid "Web development is often broad, not deep -- problems span many domains. We've written a set of :doc:`how-to guides </howto/index>` that answer common \"How do I ...?\" questions. Here you'll find information about :doc:`generating PDFs with Django </howto/outputting-pdf>`, :doc:`writing custom template tags </howto/custom-template-tags>`, and more."
msgstr ""

#: ../../intro/whatsnext.txt:59
# 822422edb42b4206b2b6929ab08d78e1
msgid "Answers to really common questions can also be found in the :doc:`FAQ </faq/index>`."
msgstr ""

#: ../../intro/whatsnext.txt:62
# 9fdc05d7646149e288db9fa7f601a745
msgid "The guides and how-to's don't cover every single class, function, and method available in Django -- that would be overwhelming when you're trying to learn. Instead, details about individual classes, functions, methods, and modules are kept in the :doc:`reference </ref/index>`. This is where you'll turn to find the details of a particular function or whatever you need."
msgstr ""

#: ../../intro/whatsnext.txt:69
# f11eecd820f34e1a8db99d605984a82d
msgid "If you are interested in deploying a project for public use, our docs have :doc:`several guides</howto/deployment/index>` for various deployment setups as well as a :doc:`deployment checklist</howto/deployment/checklist>` for some things you'll need to think about."
msgstr ""

#: ../../intro/whatsnext.txt:74
# 718a87a5c85246269bfb4c72b75ffa7e
msgid "Finally, there's some \"specialized\" documentation not usually relevant to most developers. This includes the :doc:`release notes </releases/index>` and :doc:`internals documentation </internals/index>` for those who want to add code to Django itself, and a :doc:`few other things that simply don't fit elsewhere </misc/index>`."
msgstr ""

#: ../../intro/whatsnext.txt:82
# 7c439851b5b44e7a8be0d539b715171c
msgid "How documentation is updated"
msgstr ""

#: ../../intro/whatsnext.txt:84
# 3c014782e11e4321bdcea181154dcf0b
msgid "Just as the Django code base is developed and improved on a daily basis, our documentation is consistently improving. We improve documentation for several reasons:"
msgstr ""

#: ../../intro/whatsnext.txt:88
# cdbf5b1e0b8440ad942c3408a3e99363
msgid "To make content fixes, such as grammar/typo corrections."
msgstr ""

#: ../../intro/whatsnext.txt:90
# 77564319eadf490cafcfdbab0049bad8
msgid "To add information and/or examples to existing sections that need to be expanded."
msgstr ""

#: ../../intro/whatsnext.txt:93
# 8cdf3e052ad446ca87cc1efc141412b8
msgid "To document Django features that aren't yet documented. (The list of such features is shrinking but exists nonetheless.)"
msgstr ""

#: ../../intro/whatsnext.txt:96
# 640de8f3563947ae993ca59e0bae9d29
msgid "To add documentation for new features as new features get added, or as Django APIs or behaviors change."
msgstr ""

#: ../../intro/whatsnext.txt:99
# 883eddb0bb3d47e6a73dca603b77ea1c
msgid "Django's documentation is kept in the same source control system as its code. It lives in the `docs`_ directory of our Git repository. Each document online is a separate text file in the repository."
msgstr ""

#: ../../intro/whatsnext.txt:106
# 24bd27c537f041c89ea126e95e3007cb
msgid "Where to get it"
msgstr ""

#: ../../intro/whatsnext.txt:108
# 6f6904cce7ae406cbf5f480794037e47
msgid "You can read Django documentation in several ways. They are, in order of preference:"
msgstr ""

#: ../../intro/whatsnext.txt:112
# 9be75cfb29964a43842c81816eab5e90
msgid "On the Web"
msgstr ""

#: ../../intro/whatsnext.txt:114
# ebf42251f90448c5ba3b5589bb0fb035
msgid "The most recent version of the Django documentation lives at https://docs.djangoproject.com/en/dev/. These HTML pages are generated automatically from the text files in source control. That means they reflect the \"latest and greatest\" in Django -- they include the very latest corrections and additions, and they discuss the latest Django features, which may only be available to users of the Django development version. (See \"Differences between versions\" below.)"
msgstr ""

#: ../../intro/whatsnext.txt:122
# 536a5a10c99a42cdae90ccc5f5f89aa1
msgid "We encourage you to help improve the docs by submitting changes, corrections and suggestions in the `ticket system`_. The Django developers actively monitor the ticket system and use your feedback to improve the documentation for everybody."
msgstr ""

#: ../../intro/whatsnext.txt:126
# 9fae0620fbb64224bcf620fe7aedb421
msgid "Note, however, that tickets should explicitly relate to the documentation, rather than asking broad tech-support questions. If you need help with your particular Django setup, try the |django-users| mailing list or the `#django IRC channel`_ instead."
msgstr ""

#: ../../intro/whatsnext.txt:135
# 360ad120f3e24f62a474139ae2d144f4
msgid "In plain text"
msgstr ""

#: ../../intro/whatsnext.txt:137
# 9433ddc2a056474ba972d30ac0414a17
msgid "For offline reading, or just for convenience, you can read the Django documentation in plain text."
msgstr ""

#: ../../intro/whatsnext.txt:140
# cae5886d2f194c74838b51b74dafac1b
msgid "If you're using an official release of Django, note that the zipped package (tarball) of the code includes a ``docs/`` directory, which contains all the documentation for that release."
msgstr ""

#: ../../intro/whatsnext.txt:144
# a296b3f8408c4068980eb480c8fab250
msgid "If you're using the development version of Django (aka \"trunk\"), note that the ``docs/`` directory contains all of the documentation. You can update your Git checkout to get the latest changes."
msgstr ""

#: ../../intro/whatsnext.txt:148
# 7569ad5a9c41450e962bbc148e37fe0d
msgid "One low-tech way of taking advantage of the text documentation is by using the Unix ``grep`` utility to search for a phrase in all of the documentation. For example, this will show you each mention of the phrase \"max_length\" in any Django document:"
msgstr ""

#: ../../intro/whatsnext.txt:158
# a2da5fed51be4688bd0872437b8147bc
msgid "As HTML, locally"
msgstr ""

#: ../../intro/whatsnext.txt:160
# 3f068aa3212144f39123f85184fb053b
msgid "You can get a local copy of the HTML documentation following a few easy steps:"
msgstr ""

#: ../../intro/whatsnext.txt:162
# cb5f4909c0fb4c2ebc26c1aa05229499
msgid "Django's documentation uses a system called Sphinx__ to convert from plain text to HTML. You'll need to install Sphinx by either downloading and installing the package from the Sphinx Web site, or with ``pip``:"
msgstr ""

#: ../../intro/whatsnext.txt:170
# 9b8777e41aaf449db9044350d008988a
msgid "Then, just use the included ``Makefile`` to turn the documentation into HTML:"
msgstr ""

#: ../../intro/whatsnext.txt:178
# 35fa8d31746e4b2a9984851b3c21c72c
msgid "You'll need `GNU Make`__ installed for this."
msgstr ""

#: ../../intro/whatsnext.txt:180
# 882fd6b0d1754c2c9cc7c1c6f87f6380
msgid "If you're on Windows you can alternatively use the included batch file:"
msgstr ""

#: ../../intro/whatsnext.txt:187
# 411dd9d299f7481a88324a4c88eb3e83
msgid "The HTML documentation will be placed in ``docs/_build/html``."
msgstr ""

#: ../../intro/whatsnext.txt:191
# dc5cd7bfe54a40e9b68732f0640b154b
msgid "Generation of the Django documentation will work with Sphinx version 0.6 or newer, but we recommend going straight to Sphinx 1.0.2 or newer."
msgstr ""

#: ../../intro/whatsnext.txt:200
# 92fdd641c00b408bac1d8bfdbdc9810c
msgid "Differences between versions"
msgstr ""

#: ../../intro/whatsnext.txt:202
# 68e3928459b74c39862a12d54c6d109b
msgid "As previously mentioned, the text documentation in our Git repository contains the \"latest and greatest\" changes and additions. These changes often include documentation of new features added in the Django development version -- the Git (\"trunk\") version of Django. For that reason, it's worth pointing out our policy on keeping straight the documentation for various versions of the framework."
msgstr ""

#: ../../intro/whatsnext.txt:209
# 29efca0054534aca9b261565efefa75c
msgid "We follow this policy:"
msgstr ""

#: ../../intro/whatsnext.txt:211
# f502dcba41a246bdaa344cb17b2bd21d
msgid "The primary documentation on djangoproject.com is an HTML version of the latest docs in Git. These docs always correspond to the latest official Django release, plus whatever features we've added/changed in the framework *since* the latest release."
msgstr ""

#: ../../intro/whatsnext.txt:216
# dfc964e3f1174614954070a27ecb2eba
msgid "As we add features to Django's development version, we try to update the documentation in the same Git commit transaction."
msgstr ""

#: ../../intro/whatsnext.txt:219
# 4ec247a103d14b2b938b2d4e5bd2b19a
msgid "To distinguish feature changes/additions in the docs, we use the phrase: \"New in version X.Y\", being X.Y the next release version (hence, the one being developed)."
msgstr ""

#: ../../intro/whatsnext.txt:223
# 8da8c0620452457da04e9a063598023d
msgid "Documentation fixes and improvements may be backported to the last release branch, at the discretion of the committer, however, once a version of Django is :ref:`no longer supported<backwards-compatibility-policy>`, that version of the docs won't get any further updates."
msgstr ""

#: ../../intro/whatsnext.txt:228
# b43e7952e10643c4ba7b3286d2d4a356
msgid "The `main documentation Web page`_ includes links to documentation for all previous versions. Be sure you are using the version of the docs corresponding to the version of Django you are using!"
msgstr ""

